<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <link rel="shortcut icon" type="image/x-icon" href="images/favicon.svg">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
  <meta name="google-site-verification" content="QCN0_plIog6zDhFqWQ5gDahETGCf9bduJniYjblCYk0"/>
  <meta name="title" content="<?php echo $title;  ?>" >
  <meta name="description" content="<?php echo $description; ?>">
  <title><?php echo $title; ?></title>
  <link rel="stylesheet" href="css/style.css">
  <link rel="stylesheet" href="css/swiper.min.css">
  <link rel="canonical" href="<?=$path?>">
</head>
<body>
