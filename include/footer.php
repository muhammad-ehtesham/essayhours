<style media="screen">

a{
  text-decoration: none;
}
.bottomFooter .wrapper{
  max-width: 1170px;
  width: 100%;
  margin: 0 auto;
  display: flex;
  justify-content: space-between;
}
.bottomFooter p{
  color:#fff;
}
.bottomFooter ul{
  list-style: none;
  display: flex;
}
.bottomFooter ul li{
  margin-right: 18px;
}
.bottomFooter ul li a{
  color: #fff;
  font-size: 20px;
  line-height: 23px;
}
section.bottomFooter {
  background: #35414B;
  box-shadow: inset 0px 1px 1px rgb(0 0 0 / 25%);
  color: #fff;
  padding: 20px 0px;
}
.socials {
  display: flex;
  justify-content: center;
  align-items: center;
}
.socialIcon {
  background-image: url('images/socials-sprite.png');
  margin-right: 20px;
}
.facebook {
  width: 30px;
  height: 31px;
  background-position: -10px -10px;
}
.instagram{
  width: 30px; height: 31px;
  background-position: -60px -10px;
}
.twitter{
  width: 30px; height: 31px;
  background-position: -110px -10px
}
.youtube{
  width: 30px; height: 31px;
  background-position: -10px -61px;
}

@media (max-width:1200px){
  .bottomFooter .wrapper{
    max-width: 980px;
  }
}
@media (max-width:992px){
  .bottomFooter .wrapper{
    max-width: 805px;
  }
  .bottomFooter .container{
    max-width: 700px;
  }
  .bottomFooter ul li a{
    font-size: 14px;
    line-height: 20px;
  }

}
@media (max-width:767px){
  .bottomFooter ul{
    list-style: none;
    display: flex;
    flex-direction: column;
    align-items: center;
  }
  .bottomFooter .wrapper{
    flex-direction:column;
  }
  .bottomFooter p{
    text-align:center;
  }
  .socials{
    padding-top:10px;
  }
}
</style>
</head>
<body>
  <section class="bottomFooter">
    <div class="wrapper">
      <div>
        <p>© 2021 - All rights reserved</p>
      </div>
      <div>
        <!-- <ul>
        <li><a href="javascript:;">Contact Us</a></li>
        <li><a href="javascript:;">Privacy Policy</a></li>
        <li><a href="javascript:;">Terms & Conditions</a></li>
      </ul> -->
    </div>
    <div class="socials">
      <a href="https://www.facebook.com/profile.php?id=100071279212269"><div class="socialIcon facebook"></div></a>
      <a href="https://www.instagram.com/essayhours/"><div class="socialIcon instagram"></div></a>
      <a href="https://mobile.twitter.com/EssayHours"><div class="socialIcon twitter"></div></a>
      <!-- <a href="javascript:;"><div class="socialIcon youtube"> </div></a> -->
    </div>
  </div>
</section>


<!-- jQuery -->
<script src="JS/jquery.min.js"></script>
<!-- Swiper JS -->
<script src="JS/swiper.min.js"></script>
<!-- Custom JS file -->
<script src="JS/main.js"></script>
<?php if($actual_link=='') {  ?>
  <script type="application/ld+json">
  {
    "@context": "https://schema.org",
    "@type": "FAQPage",
    "mainEntity": [{
      "@type": "Question",
      "name": "How much should I pay someone to write my paper?",
      "acceptedAnswer": {
        "@type": "Answer",
        "text": "<p>At EssayHours.com, you have to pay $15 to $30 for a high school essay. However, the following factors can affect our pricing for an academic paper.</p>
        <ul>
        <li>Number of pages</li>
        <li>Academic level</li>
        <li>Deadline</li>
        </ul>"
      }
    },{
      "@type": "Question",
      "name": "Is it legal to pay someone to write a paper for you?",
      "acceptedAnswer": {
        "@type": "Answer",
        "text": "<p>Yes, it is absolutely legal to pay a professional writer at EssayHours.com to write a paper for you. We have a team of experienced and qualified experts who work with the aim to produce high-quality and plagiarism-free papers.</p>"
      }
    },{
      "@type": "Question",
      "name": "Which website can help me with my essay?",
      "acceptedAnswer": {
        "@type": "Answer",
        "text": "<p>EssayHours.com is the website that can help you write your essays or other academic assignments. If you are looking to get your essay done, simply place your order. We will compose a flawless essay for you by following your requirements.</p>"
      }
    },{
      "@type": "Question",
      "name": "Can you write essays for free?",
      "acceptedAnswer": {
        "@type": "Answer",
        "text": "<p>No, we do not write essays for free. We know how tight on-budget students are. That's why we have made sure to keep our prices low. With this, you can get the best essay writing help, without having a huge price tag for it!</p>"
      }
    },{
      "@type": "Question",
      "name": "Should I use an online ‘write my essay’ service?",
      "acceptedAnswer": {
        "@type": "Answer",
        "text": "<p>Yes, you should use an online &lsquo;write my essay&rsquo; service if you are unable to write your papers. Professional services help students struggling with their academic assignments. So working with such a service can be of great writing help for you.</p>"
      }
    },{
      "@type": "Question",
      "name": "Can you trust essay writing sites?",
      "acceptedAnswer": {
        "@type": "Answer",
        "text": "<p>Yes, you can trust reliable and legitimate essay writing services like EssayHours.com. Our main goal is to provide the best help to everyone coming to us with their &lsquo;write my essay&rsquo; requests.</p>"
      }
    },{
      "@type": "Question",
      "name": "How fast can you write my college essay?",
      "acceptedAnswer": {
        "@type": "Answer",
        "text": "<p>Professional writers at EssayHours.com can complete your assignments within a 6-12 hour deadline. However, short deadlines are not a problem for us though we recommend starting your college essays on time.&nbsp;</p>"
      }
    },{
      "@type": "Question",
      "name": "Can I pay someone to write my paper?",
      "acceptedAnswer": {
        "@type": "Answer",
        "text": "<p>Yes, you can pay a professional essay writer to write an essay for you. Similarly, you can always buy an essay online when you are unable to complete it yourself due to any reason.&nbsp;</p>
        <p>If you are feeling unmotivated or unable to finish your essay, make an order at EssayHours.com. We are a legit writing service that will write your paper from scratch.&nbsp;</p>
        <p>We also offer a wide range of services that cater to all different needs - even if the deadline is tight. Our team of highly qualified, experienced, and fastest essay writers can write your essay within 6 hours.&nbsp;</p>"
      }
    },{
      "@type": "Question",
      "name": "Who can I pay to write my paper?",
      "acceptedAnswer": {
        "@type": "Answer",
        "text": "<p>At EssayHours.com, you can pay a professional essay writer to write your paper. They are committed to helping students overcome their writing struggles.&nbsp;</p>
        <p>If you need help on your paper and have no idea where to start, contact us today for custom paper writing services at affordable rates. With us, you don't have to concern yourself with deadlines or formatting any longer. We got your back!</p>
        <p>Whether it&rsquo;s a high school essay, college assignment, or a university thesis, just trust our essay writers.</p>"
      }
    },{
      "@type": "Question",
      "name": "Is it reliable to get help from a cheap essay writer?",
      "acceptedAnswer": {
        "@type": "Answer",
        "text": "<p>No, it is not reliable to work with a free or cheap essay writer. We completely understand the most students are tight on the budget. But getting help from a &lsquo;write my paper for cheap&rsquo; source is not an option. Keep in mind that top-quality and plagiarism-free papers will never come for cheap.</p>
        <p>Instead, it is better to get reliable help from an affordable academic writing service. Legit writing services like EssayHours.com provide affordable paper writing services from experienced writers.&nbsp;</p>
        <p>We DO NOT compromise on the quality and make sure everything comes to us get the best help at budget-friendly rates.</p>"
      }
    },{
      "@type": "Question",
      "name": "How long will it take for you to write my essay?",
      "acceptedAnswer": {
        "@type": "Answer",
        "text": "<p>We focus on the given deadline. Thus, we write your paper as soon as possible to make sure that you get it back in time for submission.&nbsp;</p>
        <p>The minimum time required to write a high school essay is 6 hours. But some papers like lab reports or case studies might take more work depending upon their topic and complexity.</p>
        <p>Rest assured, EssayHours.com only hires qualified and capable writers who know how to produce original papers before the deadline. However, it is always better to place your order in advance so that we have enough time to polish your paper.&nbsp;</p>"
      }
    }]
  }
  </script>

<?php } else if($actual_link == "essay-writer") { ?>
  <script type="application/ld+json">
  {
    "@context": "https://schema.org/",
    "@type": "BreadcrumbList",
    "itemListElement": [{
      "@type": "ListItem",
      "position": 1,
      "name": "Home",
      "item": "http://essayhours.com/"
    },{
      "@type": "ListItem",
      "position": 2,
      "name": "Essay Writer",
      "item": "http://essayhours.com/essay-writer"
    }]
  }
  </script>
  <script type="application/ld+json">
  {
    "@context": "https://schema.org",
    "@type": "FAQPage",
    "mainEntity": [{
      "@type": "Question",
      "name": "Can you write my essay for me?",
      "acceptedAnswer": {
        "@type": "Answer",
        "text": "Yes, EssayHours.com is a professional writing service that can write essays for you. The writers of our team are professional, and they write according to academic standards."
      }
    },{
      "@type": "Question",
      "name": "Can I pay someone to write an essay?",
      "acceptedAnswer": {
        "@type": "Answer",
        "text": "Yes, you can easily pay a legitimate writing service like EssayHours.com to write an essay for you. All our prices are affordable, and every student can easily afford them."
      }
    },{
      "@type": "Question",
      "name": "Can I hire someone to write my college essay?",
      "acceptedAnswer": {
        "@type": "Answer",
        "text": "Yes, you can easily hire an expert essay writer to write a college essay for you. However, make sure they have good writing skills and complete your essay on time."
      }
    },{
      "@type": "Question",
      "name": "Should I use an essay writing service?",
      "acceptedAnswer": {
        "@type": "Answer",
        "text": "Yes, you use an essay writing service if you are busy, have weak writing skills, or need professional writing help. However, choose carefully and save from the fraud ones."
      }
    },{
      "@type": "Question",
      "name": "Is buying essays illegal?",
      "acceptedAnswer": {
        "@type": "Answer",
        "text": "Buying essays online is legal and safe. You can either purchase an essay that has already been written or hire someone who will write one for you."
      }
    },{
      "@type": "Question",
      "name": "Are essay-writing websites legit?",
      "acceptedAnswer": {
        "@type": "Answer",
        "text": "Yes, some essay writing websites like EssayHours.com are legit. The team of our experts will guide you with their years of experience in this field. We offer the best online writing services across the globe."
      }
    }]
  }
</script>

<?php } else if($actual_link == "write-my-essay") { ?>
  <script type="application/ld+json">
  {
    "@context": "https://schema.org/",
    "@type": "BreadcrumbList",
    "itemListElement": [{
      "@type": "ListItem",
      "position": 1,
      "name": "Home",
      "item": "http://essayhours.com/"
    },{
      "@type": "ListItem",
      "position": 2,
      "name": "Write My Essay",
      "item": "http://essayhours.com/write-my-essay"
    }]
  }
  </script>
  <script type="application/ld+json">
  {
    "@context": "https://schema.org",
    "@type": "FAQPage",
    "mainEntity": [{
      "@type": "Question",
      "name": "Is ‘write my essay for me’ legit?",
      "acceptedAnswer": {
        "@type": "Answer",
        "text": "Yes, ‘write my essay for me’ services like EssayHours.com are reliable and legit. We have been around for decades. All our clients love us because we provide high-quality services every time without fail."
      }
    },{
      "@type": "Question",
      "name": "What should I write my college essay about?",
      "acceptedAnswer": {
        "@type": "Answer",
        "text": "Below is the list of topics that you can write your college essay about.
        Discuss your career goals.
        Explain an interesting solution to a problem.
        Any fear you have overcome.
        Describe your unique abilities."
      }
    },{
      "@type": "Question",
      "name": "Where can I have someone to write my essay?",
      "acceptedAnswer": {
        "@type": "Answer",
        "text": "You can hire a professional essay writer at EssayHours.com to write an essay for you. They are proficient in writing all types of essays tailored according to your instructions."
      }
    },{
      "@type": "Question",
      "name": "What other websites can help me write my essay?",
      "acceptedAnswer": {
        "@type": "Answer",
        "text": "Besides EssayHours.com, there are also several other websites that can help you write an amazing essay. These include:
        FreeEssayWriter.net
        5StarEssays.com
        MyPerfectWords.com"
      }
    }]
  }
  </script>
<?php } else if($actual_link == "write-my-paper") { ?>
  <script type="application/ld+json">
  {
    "@context": "https://schema.org/",
    "@type": "BreadcrumbList",
    "itemListElement": [{
      "@type": "ListItem",
      "position": 1,
      "name": "Home",
      "item": "http://essayhours.com/"
    },{
      "@type": "ListItem",
      "position": 2,
      "name": "Write My Paper",
      "item": "http://essayhours.com/write-my-paper"
    }]
  }
  </script>
  <script type="application/ld+json">
  {
    "@context": "https://schema.org",
    "@type": "FAQPage",
    "mainEntity": [{
      "@type": "Question",
      "name": "Is ‘write my papers’ legit?",
      "acceptedAnswer": {
        "@type": "Answer",
        "text": "Yes, ‘write my paper’ is legit if you choose to work with a reliable service like EssayHours.com. By working with us, you don’t need to worry about plagiarized work. Our team only produces original and top-quality papers for you."
      }
    },{
      "@type": "Question",
      "name": "Will you write my paper for free?",
      "acceptedAnswer": {
        "@type": "Answer",
        "text": "No, EssayHours.com does not offer free paper writing services to its clients. There are many online companies that claim to offer high-quality and free papers. They are nothing but frauds and scams that mislead students and provide copied content. Thus, it is safe to work with an affordably priced service that can process your ‘write my paper’ request on time."
      }
    },{
      "@type": "Question",
      "name": "How would I know if a ‘write my paper’ service is reliable?",
      "acceptedAnswer": {
        "@type": "Answer",
        "text": "When you are looking for a reliable paper writing service, it's important to read what other customers have said about them. This gives us an idea of how the company operates and provides its services while avoiding any biases.
        Not only that, but work samples also give us insight into the quality they provide. This is how we can see exactly who will be working on our project before starting!"
      }
    }]
  }
  </script>

<?php } else if($actual_link == "essay-writing-service") { ?>
  <script type="application/ld+json">
  {
    "@context": "https://schema.org/",
    "@type": "BreadcrumbList",
    "itemListElement": [{
      "@type": "ListItem",
      "position": 1,
      "name": "Home",
      "item": "http://essayhours.com/"
    },{
      "@type": "ListItem",
      "position": 2,
      "name": "Essay Writing Service",
      "item": "http://essayhours.com/essay-writing-service"
    }]
  }
  </script>
  <script type="application/ld+json">
  {
    "@context": "https://schema.org",
    "@type": "FAQPage",
    "mainEntity": [{
      "@type": "Question",
      "name": "Is essay writing service legal?",
      "acceptedAnswer": {
        "@type": "Answer",
        "text": "Yes, a reliable and legitimate essay writing service is legal to use. Online writing companies are the legal and the best option for students struggling with their academic papers. They provide assistance all around the world where people need help with their assignments. So just place your order and let us complete your assignments before time runs out!"
      }
    },{
      "@type": "Question",
      "name": "What is a good essay writing service?",
      "acceptedAnswer": {
        "@type": "Answer",
        "text": "EssayHours.com is a good essay writing service available online. We have an experienced team of writers that are ready to take care of all your academic needs. Moreover, with our efficient turnaround rate, we can deliver papers even within a short deadline."
      }
    },{
      "@type": "Question",
      "name": "Is paying someone to write an essay illegal?",
      "acceptedAnswer": {
        "@type": "Answer",
        "text": "No, it is not illegal to pay someone to write an essay as long as you are getting help from a reliable service. In fact, it is considered safe and legal to seek professional help in all your academic assignments."
      }
    },{
      "@type": "Question",
      "name": "Will I get caught using an essay writing service?",
      "acceptedAnswer": {
        "@type": "Answer",
        "text": "No, you will not get caught if you are using a legitimate and trustworthy essay writing service. A good approach before using a service is to read the customer reviews. It should be done to ensure that it is the safest platform to buy an essay."
      }
    }]
  }
  </script>

<?php } else if($actual_link == "paper-writing-service") { ?>
  <script type="application/ld+json">
  {
    "@context": "https://schema.org/",
    "@type": "BreadcrumbList",
    "itemListElement": [{
      "@type": "ListItem",
      "position": 1,
      "name": "Home",
      "item": "http://essayhours.com/"
    },{
      "@type": "ListItem",
      "position": 2,
      "name": "Paper Writing Service",
      "item": "http://essayhours.com/paper-writing-service"
    }]
  }
  </script>
  <script type="application/ld+json">
  {
    "@context": "https://schema.org",
    "@type": "FAQPage",
    "mainEntity": [{
      "@type": "Question",
      "name": "Are there any legit paper writing services?",
      "acceptedAnswer": {
        "@type": "Answer",
        "text": "Yes, EssayHours.com is one of the most legitimate paper writing services that provide top-quality content at affordable rates. Simply fill out the order form to place your order and leave the rest on our experts."
      }
    },{
      "@type": "Question",
      "name": "Are paper writing services worth it?",
      "acceptedAnswer": {
        "@type": "Answer",
        "text": "Yes, working with a genuine and reliable paper writing service like EssayHours.com is absolutely worth it. They have subject experts with the right knowledge and expertise to help you submit customized essays on time."
      }
    },{
      "@type": "Question",
      "name": "Can you write essays for free?",
      "acceptedAnswer": {
        "@type": "Answer",
        "text": "No, we do not write your essays for free because no legit writing service will do that. Remember, quality content does not come for free or cheap. Therefore, it is better to choose someone offering professional services at affordable rates."
      }
    },{
      "@type": "Question",
      "name": "What is the best research paper writing service?",
      "acceptedAnswer": {
        "@type": "Answer",
        "text": "EssayHours.com is the best research paper writing service as it can write impressive papers for you at low rates. However, we will never compromise on the quality of the work. We only aim to provide high-quality and original essays within the set deadline."
      }
    },{
      "@type": "Question",
      "name": "Can someone help me do my homework?",
      "acceptedAnswer": {
        "@type": "Answer",
        "text": "Yes, we at EssayHours.com can help you do your homework. Simply contact us with your ‘do my homework’ request, and we will get it done for you on time."
      }
    }]
  }
  </script>

<?php } ?>


<script type="text/javascript">
var erroChecker = false;
var formId = '';
$(".orderWraper .sh-form").submit(function(e){
  formId = $(this).attr("id");
  console.log(formId);
 e.preventDefault();
  formValidation();
  if (!erroChecker) {
    $(".orderWraper:not(.provideDetails)").addClass("af-none");
    $(".orderWraper.provideDetails").removeClass("af-none");
  }

  if (formId == 'step2' && !erroChecker) {
    window.location.href = '<?=$path?>make-payment';
  }

});
function formValidation(){
  var fields = $('#'+ formId + ' .form1');
  var values = {};
  erroChecker = false;
  fields.each(function(){
    values[this.id] = $(this).val();
    if (values[this.id] == '') {
      $('#'+this.id).addClass("error");
      $('#'+this.id).removeClass("valid");
      erroChecker = true;
      return;
    }
    else if (values[this.id] != '') {
      $('#'+this.id).removeClass("error");
      $('#'+this.id).addClass("valid");
    }
  });
}
$(document).ready(function(){
  $(".form1").change(function(){
    if ($(this).val != '' && $(this).hasClass('error')) {
      $(this).removeClass("error");
      $(this).addClass("valid");
    }
  });
});
</script>

</body>
</html>
