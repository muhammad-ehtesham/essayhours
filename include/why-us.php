<style media="screen">
*,body{
  box-sizing: border-box;
}
.container{
  max-width: 1170px;
  width: 100%;
  margin: 0 auto;
  padding: 0 15px;
}
section.pricingPage {
  padding: 50px 0;
}
.pricingWraper {
  width: 100%;
  display: flex;
  justify-content: space-between;
}
.pricingContent {
  max-width: 750px;
  width: 100%;
  float: left;
}
.pricingContent h1 {
  line-height: 46px;
  color: #35414B;
  font-size: 40px;
  font-family: 'GT-Walsheim-Regular';
  padding-bottom: 30px;
}
.pricingContent p{
  font-size: 16px;
  line-height: 18px;
  color: #35414B;
  padding-bottom: 40px;
  font-family: 'GT-Walsheim-Regular';
}
.pricingContent  a {
  color: #249dec;
  text-decoration: none;
}
.pricingContent h3 {
  font-size: 24px;
  line-height: 27px;
  color: #249DEC;
  padding-top: 10px;
  padding-bottom: 30px;
  font-family: 'GT-Walsheim-Regular';
}
.pricingContent .blue-text {
  font-size: 18px;
  line-height: 21px;
  color: #249DEC;
  padding-bottom: 30px;
  font-family: 'GT-Walsheim-Regular';
}
.promisesInner {
  width: 100%;
  display: flex;
  flex-wrap: wrap;
  justify-content: space-between;
}
.promises {
  width: 100%;
  margin-bottom: 20px;
  display: flex;
}
.assurance-left, .assurance-right {
  width: 50%;
}
.promises label {
  padding-left: 30px;
  position: relative;
  font-size: 14px;
  line-height: 18px;
  color: #35414B;
  font-family: 'GT-Walsheim-Regular';
}
.promises label::before {
  position: absolute;
  content: '';
  width: 20px;
  height: 20px;
  background-image: url('images/promise-check.svg');
  top: 0;
  left: 0px;
  background-repeat: no-repeat;
}
ul.questions {
  width: 100%;
  padding: 0;
  list-style: none;
}
.questions li strong {
  padding-bottom: 20px;
  display: inline-block;
  font-size: 18px;
  line-height: 21px;
  font-family: 'GT-Walsheim-Regular';
  color: #35414B;
}
/* SideBar CSS */
.sideBar {
  max-width: 230px;
  width: 100%;
  margin-left: auto;
}
.benefitsWrap{
  filter: drop-shadow(0px 2px 14px rgba(216, 217, 255, 0.668003));
  margin-bottom: 50px;
}
.benefitsInner{
  background: #Fff;
  color: #222;
  clip-path: polygon(50% 0%, 100% 21%, 100% 100%, 0 100%, 0 21%);
  width: 100%;
  min-height: 400px;
  padding-top: 50px;
}
.benefitsInner h3 {
  font-size: 33px;
  line-height: 38px;
  color: #35414B;
  font-family: 'GT-Walsheim-Regular';
  text-align: center;
}
.benefitsInner ul {
  padding-top: 10px;
  list-style: none;
  padding-left: 15px;
}
.benefitsInner ul li{
  padding-bottom: 7px;
  font-size: 15px;
  line-height: 22px;
  text-align: justify;
  font-family: 'GT-Walsheim-Regular';
}
.benefitsInner ul li span{
  color: #249DEC;
}
.valueBox {
  width: 100%;
  min-height: 230px;
  background: #FFFFFF;
  box-shadow: 0px 2px 14px rgba(216, 217, 255, 0.668003);
  border-radius: 8px;
  margin-bottom: 50px;
  border: 3px solid #249DEC;
}
.valueInner {
  padding: 55px 0 0 0;
  text-align: center;
  position: relative;
}
.valueInner::before {
  position: absolute;
  content: '';
  width: 90px;
  height: 70px;
  background: #EBF5FF;
  border-radius: 0 0 150px 0;
  top: 0;
  left: 0;
}
.valueInner small {
  font-size: 19px;
  line-height: 22px;
  text-align: center;
  letter-spacing: 0.01em;
  color: #35414B;
  font-family: 'GT-Walsheim-Regular';
  position: relative;
}
.valueInner  .words {
  font-size: 34px;
  line-height: 39px;
  text-align: center;
  color: #35414B;
  font-family: 'GT-Walsheim-Regular';
  font-weight: 600;
}
.valueInner .discount {
  font-size: 22px;
  line-height: 27px;
  text-align: center;
  letter-spacing: 0.01em;
  color: #35414B;
  font-family: 'GT-Walsheim-Regular';
  padding-top: 10px;
  border-top: 2px dashed #D5D2DC;
  margin-top: 30px;
  position: relative;
}
.papersWrap{
  filter: drop-shadow(0px 2px 14px rgba(216, 217, 255, 0.668003));
  margin-bottom: 50px;
}
.papersInner{
  background: #Fff;
  color: #222;
  clip-path: polygon(100% 0, 100% 75%, 50% 100%, 0 75%, 0 0);
  width: 100%;
  min-height: 400px;
  padding-top: 70px;
  text-align: center;
}
p.paperMatters {
  font-size: 33px;
  line-height: 38px;
  text-align: center;
  color: #35414B;
  font-family: 'GT-Walsheim-Regular';
}
p.goodWords {
  font-size: 33px;
  line-height: 38px;
  text-align: center;
  color: #35414B;
  font-family: 'GT-Walsheim-Regular';
  font-weight: 600;
}
p.panelOrder {
  margin-top: 20px;
}
.papersInner a {
  background: #249DEC;
  border: 3px solid #249DEC;
  box-sizing: border-box;
  border-radius: 30px;
  color: #fff;
  text-decoration: none;
  padding: 10px;
  display: block;
  max-width: 160px;
  margin: 0 auto;
  width: 100%;
}
.roundShape::before {
  position: absolute;
  content: '';
  width: 32px;
  height: 32px;
  background: #fff;
  border-radius: 100%;
  top: -20px;
  left: -20px;
  border: 3px solid #249DEC;
}
.roundShape::after {
  content: "";
  position: absolute;
  content: '';
  width: 18px;
  height: 38px;
  background: #fff;
  top: -20px;
  left: -21px;
}
.roundShape.right::before {
  top: -20px;
  right: -20px;
  left: auto;
}
.roundShape.right::after {
  top: -20px;
  right: -21px;
  left: auto;
}
.prepayBox {
  background: #FFFFFF;
  box-shadow: 0px 2px 14px rgba(216, 217, 255, 0.668003);
  border-radius: 8px;
  padding: 45px 0px 0 20px;
  width: 100%;
  min-height: 230px;
  margin-bottom: 50px;
  position: relative;
}
.prepayBox::before {
  position: absolute;
  content: '';
  width: 100%;
  height: 30px;
  background-color: #EBF5FF;
  left: 0;
  top: 0;
}
.prepayBox::after {
  position: absolute;
  background-image: url('images/paper-clip.svg');
  width: 49px;
  height: 87px;
  top: -16px;
  right: 15px;
  content: '';
  z-index: 0;
}
.prepayBox p {
  font-size: 34px;
  line-height: 39px;
  color: #35414B;
  font-family: 'GT-Walsheim-Regular';
  padding-top: 10px;
}
p.projectStarted {
  font-size: 22px;
  line-height: 27px;
  color: #35414B;
  font-family: 'GT-Walsheim-Regular';
}
.projectStarted span{
  color: #249DEC;
}

@media (max-width:1200px) {
  .container{
    max-width: 920px;
  }
  .pricingContent {
    max-width: 680px;
    padding-right: 50px;
  }
  .promises {
    width: 100%;
  }
  .benefitsInner ul li {
    font-size: 13px;
  }
  .sideBar {
    max-width: 210px;
  }
  p.goodWords {
    font-size: 30px;
  }
  p.paperMatters {
    font-size: 25px;
  }
}
@media (max-width:991px) {
  .container{
    max-width: 820px;
  }
  .assurance-left, .assurance-right {
    width: 100%;
  }
  .pricingContent {
    padding-right: 50px;
  }
}
@media (max-width:767px) {
  .container{
    max-width: 720px;
  }
  .pricingContent {
    max-width: unset;
    width: 100%;
  }
  .sideBar {
    display: none;
  }
  .pricingContent {
    padding-right: 0;
  }
}
@media (max-width:575px) {
  .container{
    max-width: 540px;
  }
}
</style>
<body>
  <section class="pricingPage">
    <div class="container">
      <div class="pricingWraper">
        <div class="pricingContent">
          <h1>Why Us? Because your grades matter</h1>
          <h3>We do not buy prepackaged content!</h3>
          <p>There are over a 100,000 different websites that offer cut and paste information. But if you see that piece of content in one place you are bound to see it somewhere else, and that is deemed plagiarized. Plagiarism detection software has become highly sophisticated and is used by educators, universities and even the U.S. government these days. Accusations of plagiarism can cause you to be expelled from school, lose a job or fail the class. None of these are tenable options.</p>
          <p><a href="javascript:;">EssayHours.com</a> professional writers provide original writing tailored to your specific needs and request. Do not purchase pre-written work; you will be disappointed every time.</p>
          <p>Choose <a href="javascript:;">EssayHours.com</a> when you want the <a href="javascript:;">best essay writing service</a>! That way you can be sure you will retain full control over the writing process from beginning to end and always know the status of your order. </p>
          <p class="blue-text">Other assurances offered by EssayHours.com include:</p>
          <div class="promisesInner">
            <div class="assurance-left">
              <div class="promises">
                <label>Direct contact with your writer</label>
              </div>
              <div class="promises">
                <label>The writing style will fit your needs.</label>
              </div>
              <div class="promises">
                <label>Absolute quality assurance.</label>
              </div>
              <div class="promises">
                <label>Never a hidden charge or any add-ons.</label>
              </div>
              <div class="promises">
                <label>Revisions and edits upon request.</label>
              </div>
              <div class="promises">
                <label>Bibliography/Citation pages for free.</label>
              </div>
              <div class="promises">
                <label>No unexpected charges. What we quote is what you pay.</label>
              </div>
            </div>
            <div class="assurance-right">
              <div class="promises">
                <label>Work to begin immediately upon receipt of the 50% deposit; balance on delivery.</label>
              </div>
              <div class="promises">
                <label>You receive the best assignment help from subject area experts aligned to your writing needs that hold Ph.D., Masters, or Bachelor Degrees.</label>
              </div>
              <div class="promises">
                <label>Unparalleled quality content from experts with authentic and varied relevant experience.</label>
              </div>
              <div class="promises">
                <label>Our team is made up of writers who are native English speakers familiar with American English vernacular and colloquialism.</label>
              </div>
            </div>

          </div>
          <p class="blue-text">Ok. decision-making time.</p>
          <p>Do you want a dependable piece of work with superior quality or are you shopping for the cheapest paper you can find?</p>
          <p class="blue-text">Consider the following questions.</p>
          <ul class="questions">
            <li>
              <strong>1- Is it possible to get quality work delivered in a matter of 2 or 3 hours?</strong>
              <p>Sorry – not possible. Quality takes time and research. Be sure to build in the time you need to get the work you need.</p>
            </li>
            <li>
              <strong>2- Can I get a Ph.D. dissertation done for the same price as a high school essay?</strong>
              <p>Sorry – not possible. Ph.D. work is painstaking and requires sophisticated and advanced research. You should expect to pay for it, but <a href="javascript:;">EssayHours.com</a> can assure you of a completed dissertation that will add to the body of knowledge in your field.</p>
            </li>
            <li>
              <strong>3- Can I get real quality work for $10 per page?</strong>
              <p>Sorry – not possible. Exceptional writing tailored to assignment requirements that will meet and exceed academic expectations does not come cheap, and you shouldn't expect it too. If someone says they can do it through <a href="javascript:;">cheap essay writing services</a>, then expect that promise refers to quality as well as price.</p>
            </li>
          </ul>
        </div>
        <div class="sideBar">
          <div class="sidebarWraper">
            <div class="benefitsWrap">
              <div class="benefitsInner">
                <h3>Benefits</h3>
                <ul>
                  <li><span>Free</span> Proofreading</li>
                  <li><span>Free</span> Unlimited Revisions</li>
                  <li><span>Free</span> Formatting</li>
                  <li><span>Free</span> Title Page</li>
                  <li><span>Free</span> Bibliography</li>
                  <li><span>US Qualified</span> Writers</li>
                  <li><span>No Plagiarism</span> Guaranteed</li>
                  <li><span>Direct contact</span> with Writer</li>
                  <li><span>100%</span> Secure & Confidential</li>
                  <li><span>Deadline</span> Driven</li>
                </ul>
              </div>
            </div>
            <div class="prepayBox">
              <p>PREPAY</p>
              <p><strong>50% ONLY</strong> </p>
              <p class="projectStarted">TO GET YOUR <span>PROJECT</span> STARTED</p>
            </div>
            <div class="valueBox">
              <div class="valueInner">
                <small>Our average page is</small>
                <p class="words">300 WORDS</p>

                <p class="discount">so you get upto 20% more value
                  <span class="roundShape left"></span>
                  <span class="roundShape right"></span>
                </p>
              </div>
            </div>
            <div class="papersWrap">
              <div class="papersInner">
                <p class="paperMatters">If your paper matters,</p>
                <p class="goodWords">we own all the good words.</p>
                <p class="panelOrder"><a href="#javascript:;">Order Now</a> </p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
