
<link rel="stylesheet" href="css/banner.css">
<section class="banner">
  <div class="container">
    <div class="bannerContent">
      <div class="wrapper">
        <div class="bannerText">
          <?php
          if(isset($pageInfo[$actual_link]['is_page_heading']) && $pageInfo[$actual_link]['is_page_heading'] == true)  { ?>
            <h1 class=""><?= $pageInfo[$actual_link]['page_heading'] ?></h1>
          <?php  } else {  ?>
            <h1 class="">Essay Writing Service</h1>
          <?php } ?>
          <div class="services">
            <div class="singleService ">
              <div class="spriteIcons sprite1"></div>
              <p>100+ Writing Experts</p>
            </div>
            <div class="singleService">
              <div class="spriteIcons sprite2"></div>
              <p>Money Back Guarantee</p>
            </div>
            <div class="singleService">
              <div class="spriteIcons sprite3"></div>
              <p>100+ Plagiarism-Free Content</p>
            </div>
            <div class="singleService">
              <div class="spriteIcons sprite4"></div>
              <p>Unique and Premium Quality Papers</p>
            </div>
          </div>
          <div class="orderBtn">
            <a href="javascript:void(0)">Calculate Now</a>
          </div>
        </div>
        <div class="heroImage">
          <picture>
            <source type="image/webp" srcset="" data-src="">
              <img src="images/hero-image.svg" alt="Student">
            </picture>
          </div>
        </div>
      </div>
    </div>
  </section>
  <section class="howItWorks" id="howItWorks">
    <h2 class="h-two">How Our Essay Writing Service Works?</h2>
    <div class="wrapper">
      <div class="girlImage">
        <picture>
          <source type="image/webp" srcset="" data-src="">
            <img src="images/how-it-works-opt.svg" alt="Girl Image">
          </picture>
        </div>
        <div class="hierarchy">
          <div class="step">
            <div class="sprite-bg">
              <div class="spriteIcons sprite9"></div>
            </div>

            <p>Place your order by filling an order form</p>
          </div>
          <div class="step">
            <div class="sprite-bg">
              <div class="spriteIcons sprite10"></div>
            </div>
            <p>Our U.S. - based writer gets busy on your project</p>
          </div>
          <div class="step">
            <div class="sprite-bg">
              <div class="spriteIcons sprite11"></div>
            </div>
            <p>Your project beats the deadline and shows up IN YOUR INBOX</p>
          </div>
        </div>
      </div>
    </div>
  </section>
<?php
if(isset($pageInfo[$actual_link]['includeFiles']) && $pageInfo[$actual_link]['includeFiles'] == true)
{
  include($pageInfo[$actual_link]['includeFilePaths']);
}
