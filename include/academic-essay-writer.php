<style media="screen">
*,body{
  box-sizing: border-box;
}
.container{
  max-width: 1170px;
  width: 100%;
  margin: 0 auto;
  padding: 0 15px;
}
section.pricingPage {
  padding: 50px 0;
}
.pricingWraper {
  width: 100%;
  display: flex;
  justify-content: space-between;
}
.pricingContent {
  max-width: 750px;
  width: 100%;
  float: left;
}
.pricingContent h1 {
  line-height: 46px;
  color: #35414B;
  font-size: 40px;
  font-family: 'GT-Walsheim-Regular';
  padding-bottom: 30px;
}
.pricingContent p{
  font-size: 16px;
  line-height: 18px;
  color: #35414B;
  padding-bottom: 40px;
  font-family: 'GT-Walsheim-Regular';
}
.pricingContent  a {
  color: #249dec;
  text-decoration: none;
}
.pricingContent h2 {
  font-size: 22px;
  line-height: 25px;
  color: #249DEC;
  padding-bottom: 30px;
  font-family: 'GT-Walsheim-Regular';
}
.writersWraper {
  width: 100%;
  float: left;
  display: flex;
  flex-wrap: wrap;
  justify-content: space-between;
}
.writersInner {
  max-width: 210px;
  width:100%;
  padding: 23px 16px;
  border: 2px solid #35414B;
  box-shadow: 0px 2px 5px rgba(124, 124, 124, 0.5);
  border-radius: 12px;
  text-align: center;
  margin-bottom: 30px;
}
.writerImage {
  max-width: 70px;
  width: 100%;
  height: 70px;
  border: 2px solid #35414B;
  border-radius: 50%;
  display: flex;
  align-items: center;
  justify-content: center;
  margin: 0 auto;
}
.writersInner h4 {
  font-size: 18px;
  line-height: 21px;
  color: #35414B;
  font-family: 'GT-Walsheim-Regular';
  padding-top: 10px;
}
.writersInner .writerQualification {
  font-size: 14px;
  line-height: 16px;
  text-align: center;
  color: #35414B;
  font-family: 'GT-Walsheim-Regular';
  border-bottom: 2px solid #35414B;
  padding-bottom: 10px;
  margin-bottom: 20px;
}
.writersInner .writerDesc {
  font-size: 16px;
  line-height: 18px;
  text-align: center;
  color: #35414B;
  font-family: 'GT-Walsheim-Regular';
  padding-bottom: 0;
}
.orderNow a {
  background: #249DEC;
  border: 3px solid #249DEC;
  box-sizing: border-box;
  border-radius: 30px;
  color: #fff!important;
  text-decoration: none;
  padding: 10px;
  display: block;
  margin: 0 auto;
  width: 100%;
}
p.orderNow {
  float: left;
  padding: 0;
  max-width: 160px;
  width: 100%;
  text-align: center;
}
/* SideBar CSS */
.sideBar {
  max-width: 230px;
  width: 100%;
  margin-left: auto;
}
.benefitsWrap{
  filter: drop-shadow(0px 2px 14px rgba(216, 217, 255, 0.668003));
  margin-bottom: 50px;
}
.benefitsInner{
  background: #Fff;
  color: #222;
  clip-path: polygon(50% 0%, 100% 21%, 100% 100%, 0 100%, 0 21%);
  width: 100%;
  min-height: 400px;
  padding-top: 50px;
}
.benefitsInner h3 {
  font-size: 33px;
  line-height: 38px;
  color: #35414B;
  font-family: 'GT-Walsheim-Regular';
  text-align: center;
}
.benefitsInner ul {
  padding-top: 10px;
  list-style: none;
  padding-left: 15px;
}
.benefitsInner ul li{
  padding-bottom: 7px;
  font-size: 15px;
  line-height: 22px;
  text-align: justify;
  font-family: 'GT-Walsheim-Regular';
}
.benefitsInner ul li span{
  color: #249DEC;
}
.valueBox {
  width: 100%;
  min-height: 230px;
  background: #FFFFFF;
  box-shadow: 0px 2px 14px rgba(216, 217, 255, 0.668003);
  border-radius: 8px;
  margin-bottom: 50px;
  border: 3px solid #249DEC;
}
.valueInner {
  padding: 55px 0 0 0;
  text-align: center;
  position: relative;
}
.valueInner::before {
  position: absolute;
  content: '';
  width: 90px;
  height: 70px;
  background: #EBF5FF;
  border-radius: 0 0 150px 0;
  top: 0;
  left: 0;
}
small {
  font-size: 19px;
  line-height: 22px;
  text-align: center;
  letter-spacing: 0.01em;
  color: #35414B;
  font-family: 'GT-Walsheim-Regular';
  position: relative;
}
.valueInner  .words {
  font-size: 34px;
  line-height: 39px;
  text-align: center;
  color: #35414B;
  font-family: 'GT-Walsheim-Regular';
  font-weight: 600;
}
.valueInner .discount {
  font-size: 22px;
  line-height: 27px;
  text-align: center;
  letter-spacing: 0.01em;
  color: #35414B;
  font-family: 'GT-Walsheim-Regular';
  padding-top: 10px;
  border-top: 2px dashed #D5D2DC;
  margin-top: 30px;
  position: relative;
}
.papersWrap{
  filter: drop-shadow(0px 2px 14px rgba(216, 217, 255, 0.668003));
  margin-bottom: 50px;
}
.papersInner{
  background: #Fff;
  color: #222;
  clip-path: polygon(100% 0, 100% 75%, 50% 100%, 0 75%, 0 0);
  width: 100%;
  min-height: 400px;
  padding-top: 70px;
  text-align: center;
}
p.paperMatters {
  font-size: 33px;
  line-height: 38px;
  text-align: center;
  color: #35414B;
  font-family: 'GT-Walsheim-Regular';
}
p.goodWords {
  font-size: 33px;
  line-height: 38px;
  text-align: center;
  color: #35414B;
  font-family: 'GT-Walsheim-Regular';
  font-weight: 600;
}
p.panelOrder {
  margin-top: 20px;
}
.papersInner a {
  background: #249DEC;
  border: 3px solid #249DEC;
  box-sizing: border-box;
  border-radius: 30px;
  color: #fff;
  text-decoration: none;
  padding: 10px;
  display: block;
  max-width: 160px;
  margin: 0 auto;
  width: 100%;
}
.roundShape::before {
  position: absolute;
  content: '';
  width: 32px;
  min-height: 32px;
  background: #fff;
  border-radius: 100%;
  top: -20px;
  left: -20px;
  border: 3px solid #249DEC;
}
.roundShape::after {
  content: "";
  position: absolute;
  content: '';
  width: 18px;
  height: 38px;
  background: #fff;
  top: -20px;
  left: -21px;
}
.roundShape.right::before {
  top: -20px;
  right: -20px;
  left: auto;
}
.roundShape.right::after {
  top: -20px;
  right: -21px;
  left: auto;
}
/* End of SideBar CSS */
@media (max-width:1200px) {
  .container{
    max-width: 920px;
  }
  .pricingContent {
    max-width: 680px;
    padding-right: 50px;
  }
  .writersInner {
    max-width: unset;
    width: 45%;
  }
  .benefitsInner ul li {
    font-size: 13px;
  }
  .sideBar {
    max-width: 210px;
  }
  p.goodWords {
    font-size: 30px;
  }
  p.paperMatters {
    font-size: 25px;
  }
}
@media (max-width:991px) {
  .container{
    max-width: 820px;
  }
  .pricingContent {
    padding-right: 50px;
  }
}
@media (max-width:767px) {
  .container{
    max-width: 720px;
  }
  .pricingContent {
    max-width: unset;
    width: 100%;
  }
  .sideBar {
    display: none;
  }
  .pricingContent {
    padding-right: 0;
  }
}
@media (max-width:575px) {
  .container{
    max-width: 540px;
  }
  .writersWraper {
    justify-content: space-between;
  }
  .pricingContent h1 {
    line-height: 25px;
    font-size: 25px;
  }
}
@media (max-width:480px) {
  .writersWraper {
    justify-content: center;
  }
  .writersInner {
    max-width:unset;
    width: 100%;
  }
}

</style>
</head>
<body>
  <section class="pricingPage">
    <div class="container">
      <div class="pricingWraper">
        <div class="pricingContent">
          <h1>Academic essay writers - High grade and on time!</h1>
          <p>When you hire a custom academic essay writer, you can’t afford getting the “second best.”</p>
          <p>You want the best possible grade on your paper. You don’t have time to work with amateurs or with “writers” who are off-shore in Indonesia, India, Singapore, Australia…or anywhere but in the U.S. You don’t want a paper filled with rehashed content from some “paper mill” that sells the same papers to thousands of students.</p>
          <p>You need original writing from U.S.-educated, native-born English speaking professionals who know your coursework or application requirements.</p>
          <p>We are a team of professional academic writers who have published numerous papers in academic journals throughout the world.</p>
          <p>Whatever your subject area is, we know it well…</p>
          <p>At EssayHours, you’ll be working with a team of native English academic professionals ranked #1 among our competitors by The National Board of Professional Scholastic Writers.</p>
          <p>Our professional writers specialize in ESL Student projects from undergrad through Doctoral levels. They know what your professors are looking for.</p>
          <p>Here’s a quick look at some writers we have standing by ready to help you achieve your academic success:</p>
          <div class="writersWraper">
            <div class="writersInner">
              <div class="writerImage">
                <img src="images/writer-image.svg" alt="">
              </div>
              <h4>MICHEAL M.</h4>
              <p class="writerQualification">Ph.D, B.A Philosophy</p>
              <p class="writerDesc">University English professor, Comparative Religion Faculty, expertise in Classic Literature and World Religions. Fluent in Greek, French, German.</p>
            </div>
            <div class="writersInner">
              <div class="writerImage">
                <img src="images/writer-image.svg" alt="">
              </div>
              <h4>MICHEAL M.</h4>
              <p class="writerQualification">Ph.D, B.A Philosophy</p>
              <p class="writerDesc">University English professor, Comparative Religion Faculty, expertise in Classic Literature and World Religions. Fluent in Greek, French, German.</p>
            </div>
            <div class="writersInner">
              <div class="writerImage">
                <img src="images/writer-image.svg" alt="">
              </div>
              <h4>MICHEAL M.</h4>
              <p class="writerQualification">Ph.D, B.A Philosophy</p>
              <p class="writerDesc">University English professor, Comparative Religion Faculty, expertise in Classic Literature and World Religions. Fluent in Greek, French, German.</p>
            </div>
            <div class="writersInner">
              <div class="writerImage">
                <img src="images/writer-image.svg" alt="">
              </div>
              <h4>MICHEAL M.</h4>
              <p class="writerQualification">Ph.D, B.A Philosophy</p>
              <p class="writerDesc">University English professor, Comparative Religion Faculty, expertise in Classic Literature and World Religions. Fluent in Greek, French, German.</p>
            </div>
            <div class="writersInner">
              <div class="writerImage">
                <img src="images/writer-image.svg" alt="">
              </div>
              <h4>MICHEAL M.</h4>
              <p class="writerQualification">Ph.D, B.A Philosophy</p>
              <p class="writerDesc">University English professor, Comparative Religion Faculty, expertise in Classic Literature and World Religions. Fluent in Greek, French, German.</p>
            </div>
            <div class="writersInner">
              <div class="writerImage">
                <img src="images/writer-image.svg" alt="">
              </div>
              <h4>MICHEAL M.</h4>
              <p class="writerQualification">Ph.D, B.A Philosophy</p>
              <p class="writerDesc">University English professor, Comparative Religion Faculty, expertise in Classic Literature and World Religions. Fluent in Greek, French, German.</p>
            </div>
          </div>
          <p>Our team knows how to deliver original, quality academic paper, and has the maturity and experience to deliver to your specifications, and to deliver it on time, every time. When you get essay help from MyPerfectWords.com, you're working with real professionals who have been carefully screened and pre-qualified by our rigorous testing and monitoring standards.</p>
          <p>100% original, high-quality custom academic writing, delivered on time, every time—is our guarantee!</p>
          <p class="orderNow"><a href="#javascript:;">Order Now</a> </p>
        </div>
        <div class="sideBar">
          <div class="sidebarWraper">
            <div class="benefitsWrap">
              <div class="benefitsInner">
                <h3>Benefits</h3>
                <ul>
                  <li><span>Free</span> Proofreading</li>
                  <li><span>Free</span> Unlimited Revisions</li>
                  <li><span>Free</span> Formatting</li>
                  <li><span>Free</span> Title Page</li>
                  <li><span>Free</span> Bibliography</li>
                  <li><span>US Qualified</span> Writers</li>
                  <li><span>No Plagiarism</span> Guaranteed</li>
                  <li><span>Direct contact</span> with Writer</li>
                  <li><span>100%</span> Secure & Confidential</li>
                  <li><span>Deadline</span> Driven</li>
                </ul>
              </div>
            </div>
            <div class="valueBox">
              <div class="valueInner">
                <small>Our average page is</small>
                <p class="words">300 WORDS</p>

                <p class="discount">so you get upto 20% more value
                  <span class="roundShape left"></span>
                  <span class="roundShape right"></span>
                </p>
              </div>
            </div>
            <div class="papersWrap">
              <div class="papersInner">
                <p class="paperMatters">If your paper matters,</p>
                <p class="goodWords">we own all the good words.</p>
                <p class="panelOrder"><a href="#javascript:;">Order Now</a> </p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
