<style media="screen">
*,body{
  box-sizing: border-box;
}
.container{
  max-width: 1170px;
  width: 100%;
  margin: 0 auto;
  padding: 0 15px;
}
section.pricingPage {
  padding: 50px 0;
}
.pricingWraper {
  width: 100%;
  display: flex;
  justify-content: space-between;
}
.pricingContent {
  max-width: 750px;
  width: 100%;
  float: left;
}
.completedWork h1 {
  line-height: 46px;
  color: #35414B;
  font-size: 40px;
  font-family: 'GT-Walsheim-Regular';
  padding-bottom: 50px;
}
.pricingContent p{
  font-size: 16px;
  line-height: 18px;
  color: #35414B;
  padding-bottom: 40px;
  font-family: 'GT-Walsheim-Regular';
}
.pricingContent  a {
  color: #249dec;
  text-decoration: none;
}
.pricingContent h2 {
  font-size: 22px;
  line-height: 25px;
  color: #249DEC;
  padding-bottom: 30px;
  font-family: 'GT-Walsheim-Regular';
}
.pdfsWraper {
  background: #FFFFFF;
  box-shadow: 0px 2px 14px rgb(124 124 124 / 50%);
  border-radius: 8px;
  width: 100%;
  min-height: 72px;
  margin-bottom: 25px;
  position:relative;
}
.pdfsWraper::before {
  position: absolute;
  content: '';
  width: 22px;
  height: 36px;
  background-image: url(images/download-arrow.svg);
  right: 25px;
  top: 15px;
  pointer-events: none;
}
.pdfsWraper a {
  width: 100%;
  display: flex;
  align-items: center;
  padding: 12px 0 12px 30px;
}
.fileIcon {
  max-width: 28px;
  width: 100%;
  height: 48px;
  background-image: url('images/pdf-icon.png');
  background-position: -10px -10px;
}
.pdfsWraper p {
  font-size: 18px;
  line-height: 21px;
  color: #35414B;
  font-family: 'GT-Walsheim-Regular';
  padding-left: 30px;
  padding-bottom: 0;
}
/* SideBar CSS */
.sideBar {
  max-width: 230px;
  width: 100%;
  margin-left: auto;
}
.benefitsWrap{
  filter: drop-shadow(0px 2px 14px rgba(216, 217, 255, 0.668003));
  margin-bottom: 50px;
}
.benefitsInner{
  background: #Fff;
  color: #222;
  clip-path: polygon(50% 0%, 100% 21%, 100% 100%, 0 100%, 0 21%);
  width: 100%;
  min-height: 400px;
  padding-top: 50px;
}
.benefitsInner h3 {
  font-size: 33px;
  line-height: 38px;
  color: #35414B;
  font-family: 'GT-Walsheim-Regular';
  text-align: center;
}
.benefitsInner ul {
  padding-top: 10px;
  list-style: none;
  padding-left: 15px;
}
.benefitsInner ul li{
  padding-bottom: 7px;
  font-size: 15px;
  line-height: 22px;
  text-align: justify;
  font-family: 'GT-Walsheim-Regular';
}
.benefitsInner ul li span{
  color: #249DEC;
}
.valueBox {
  width: 100%;
  min-height: 230px;
  background: #FFFFFF;
  box-shadow: 0px 2px 14px rgba(216, 217, 255, 0.668003);
  border-radius: 8px;
  margin-bottom: 50px;
  position: relative;
  background-image:url('images/great-grades.svg');
  background-repeat: no-repeat;
}
.greatGrades {
  position: relative;
  padding: 100px 0px 0 10px;
}
.greatGrades h4 {
  font-size: 19px;
  line-height: 22px;
  color: #35414B;
  font-family: 'GT-Walsheim-Regular';
}
.greatGrades p {
  font-weight: 300;
  font-size: 16px;
  line-height: 18px;
  color: #35414B;
  font-family: 'GT-Walsheim-Regular';
}
.greatGrades .panelOrder {
  margin-top: 35px;
}
.greatGrades a {
  background: #249DEC;
  border: 3px solid #249DEC;
  box-sizing: border-box;
  border-radius: 30px;
  color: #fff;
  text-decoration: none;
  padding: 9px 20px;
  font-family: 'GT-Walsheim-Regular'
}
/* End of SideBar CSS */
@media (max-width:1200px) {
  .container{
    max-width: 920px;
  }
  .pricingContent {
    max-width: 680px;
    padding-right: 50px;
  }
  .benefitsInner ul li {
    font-size: 13px;
  }
  .sideBar {
    max-width: 210px;
  }
}
@media (max-width:991px) {
  .container{
    max-width: 820px;
  }
  .pdfsWraper p {
    font-size: 15px;
    padding-left: 20px;
  }
  .pricingContent {
    padding-right: 50px;
  }
}
@media (max-width:767px) {
  .container{
    max-width: 720px;
  }
  .pricingContent {
    max-width: unset;
    width: 100%;
  }
  .sideBar {
    display: none;
  }
  .pricingContent {
    padding-right: 0;
  }
}
@media (max-width:575px) {
  .container{
    max-width: 540px;
  }
  .pricingContent h1 {
    line-height: 25px;
    font-size: 25px;
  }
  .pdfsWraper::before {
    right: 10px;
    top: 15px;
  }
  .pdfsWraper a {
    padding: 12px 0 12px 15px;
  }
  .pdfsWraper p {
    font-size: 15px;
    padding: 0 20px;
  }
}
</style>
</head>
<body>
  <section class="pricingPage completedWork">
    <div class="container">
      <div class="pricingWraper">
        <div class="pricingContent">
          <h1>Samples of completed work</h1>
          <p><a href="javascript:;">Disclaimer</a>: The works below have been completed for actual clients. We have secured personal permission from the customers to post these examples of our custom academic writing services. We will never post your assignment without your explicit written permission.</p>
          <div class="completedFiles">
            <div class="pdfsWraper">
              <a href="javascript:;">
                <div class="fileIcon"></div>
                <p>Immigrant Students</p>
              </a>
            </div>
            <div class="pdfsWraper">
              <a href="javascript:;">
                <div class="fileIcon"></div>
                <p>Nursing-Schizo Conceptual Framework</p>
              </a>
            </div>
            <div class="pdfsWraper">
              <a href="javascript:;">
                <div class="fileIcon"></div>
                <p>Saudi Arabia's Policy and Perspectives on MOOC Education</p>
              </a>
            </div>
            <div class="pdfsWraper">
              <a href="javascript:;">
                <div class="fileIcon"></div>
                <p>Philosophy Research</p>
              </a>
            </div>
            <div class="pdfsWraper">
              <a href="javascript:;">
                <div class="fileIcon"></div>
                <p>Biofeedback Therapy</p>
              </a>
            </div>
            <div class="pdfsWraper">
              <a href="javascript:;">
                <div class="fileIcon"></div>
                <p>Effects of cultural diversity</p>
              </a>
            </div>
          </div>
        </div>
        <div class="sideBar">
          <div class="sidebarWraper">
            <div class="benefitsWrap">
              <div class="benefitsInner">
                <h3>Benefits</h3>
                <ul>
                  <li><span>Free</span> Proofreading</li>
                  <li><span>Free</span> Unlimited Revisions</li>
                  <li><span>Free</span> Formatting</li>
                  <li><span>Free</span> Title Page</li>
                  <li><span>Free</span> Bibliography</li>
                  <li><span>US Qualified</span> Writers</li>
                  <li><span>No Plagiarism</span> Guaranteed</li>
                  <li><span>Direct contact</span> with Writer</li>
                  <li><span>100%</span> Secure & Confidential</li>
                  <li><span>Deadline</span> Driven</li>
                </ul>
              </div>
            </div>
            <div class="valueBox">
              <div class="greatGrades">
                <h4>Great Grades</h4>
                <p>Without the stress</p>
                <p class="panelOrder"><a href="#javascript:;">Order Now</a> </p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
