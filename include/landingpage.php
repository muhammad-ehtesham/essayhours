<style>
.container {
  max-width: 1555px;
  width: 100%;
  margin: 0 auto
}
a {
  text-decoration: none
}
section.whyTrust .swiper-container {
  display: none
}
.banner {
  background-image: url(images/grainy-background.svg);
  padding-top: 100px;
}

.heroImage {
  max-width: 740px;
  margin: 0 auto;
  margin-top: -55px
}

.sprite-formIcons {
  background-image: url(images/sprite-formIcons.png)
}

.calendarIcon {
  width: 26px;
  height: 30px;
  background-position: -2px 2px;
  position: absolute;
  z-index: 1
}

.docIcon {
  width: 25px;
  height: 28px;
  background-position: -2px -34px;
  position: absolute
}

.dsktp-none {
  display: none
}

.levelIcon {
  width: 25px;
  height: 28px;
  background-position: -39px 2px;
  position: absolute
}

.spriteIcons {
  background-image: url(images/homePage-icons-sprite.png)
}

.sprite1 {
  max-width: 80px;
  height: 93px;
  background-position: -1470px -100px;
  width: 100%
}

.sprite2 {
  max-width: 80px;
  height: 93px;
  background-position: -960px -900px;
  width: 100%
}

.sprite3 {
  max-width: 80px;
  height: 93px;
  background-position: -680px -900px;
  width: 100%
}

.sprite4 {
  max-width: 79px;
  height: 93px;
  background-position: -1470px -393px;
  width: 100%
}

.spriteOrder {
  background-image: url(images/sprite_order_essay.png)
}

.sprite5 {
  max-width: 90px;
  height: 115px;
  background-position: -10px -145px;
  width: 100%
}

.sprite6 {
  width: 182px;
  height: 115px;
  background-position: -10px -10px
}

.sprite7 {
  width: 170px;
  height: 115px;
  background-position: -120px -10px
}

.sprite8 {
  width: 200px;
  height: 115px;
  background-position: -230px -10px
}

.sprite-bg {
  background: #249dec;
  width: 96px;
  height: 98px;
  position: absolute;
  border-radius: 50%
}

.sprite9 {
  max-width: 96px;
  height: 98px;
  background-position: -73px -1197px;
  width: 100%;
  border-radius: 50%;
  margin-right: 20px
}

.sprite10 {
  max-width: 96px;
  height: 98px;
  background-position: -1442px -668px;
  width: 100%;
  border-radius: 50%;
  margin-right: 20px
}

.sprite11 {
  max-width: 96px;
  height: 98px;
  background-position: -1449px -936px;
  width: 100%;
  border-radius: 50%;
  margin-right: 20px
}

.sprite12 {
  max-width: 155px;
  height: 193px;
  background-position: -101px -500px;
  width: 100%
}

.sprite13 {
  max-width: 155px;
  height: 193px;
  background-position: -823px -101px;
  width: 100%
}

.sprite16 {
  max-width: 155px;
  height: 193px;
  background-position: -463px -101px;
  width: 100%
}

.sprite14 {
  max-width: 155px;
  height: 193px;
  background-position: -821px -500px;
  width: 100%
}

.sprite15 {
  max-width: 155px;
  height: 193px;
  background-position: -462px -500px;
  width: 100%
}

.sprite17 {
  max-width: 155px;
  height: 193px;
  background-position: -101px -99px;
  width: 100%
}

.bannerContent .wrapper {
  display: flex;
  justify-content: space-between
}

.star {
  max-width: 170px;
  display: flex;
  align-items: center;
  width: 100%;
  justify-content: space-between
}

.star p {
  padding-bottom: 0
}

.star img {
  width: 120px !important;
  height: 18px !important
}

.paper {
  padding: 102px 0
}

.paper .wrapper {
  display: flex;
  justify-content: center;
  max-width: 1045px;
  background: #fff;
  margin: 0 auto;
  padding: 30px;
  box-shadow: 0 2px 12px 0 #e2e2e2ad;
  border-radius: 12px
}

.paperText h2 {
  font-size: 50px;
  line-height: 55px;
  padding-bottom: 20px;
  text-align: left
}

.paperText p strong {
  color: #249dec !important;
  font-size: 28px;
  line-height: 40px;
  font-family: GT-Walsheim-Medium;
  font-weight: 500
}

.paperText .orderBtn {
  margin-top: 45px
}

.girlSideImg {
  width: 365px;
  height: 374px;
  margin-top: -37px;
  position: relative
}

h1 {
  font-family: GT-Walsheim-Medium;
  font-size: 70px;
  font-weight: 500;
  max-width: 813px;
  width: 100%;
  padding-bottom: 30px
}

.services {
  display: grid;
  grid-row: auto auto;
  grid-column-gap: 44px;
  grid-row-gap: 60px;
  grid-template-columns: 1fr 1fr;
}

.singleService {
  display: flex;
  align-items: center;
  font-size: 22px;
  font-weight: 500;
  padding: 15px;
  max-height: 110px
}

.singleService p {
  font-family: GT-Walsheim-Medium;
  font-weight: 500;
  font-size: 18px
}

.orderEssay .singleService p {
  font-family: GT-Walsheim-Regular;
  font-weight: 300
}

.orderBtn {
  margin-top: 101px
}

.orderBtn a {
  color: #35414b;
  font-size: 42px;
  background: #ffce4b;
  padding: 22px 35px;
  border-radius: 60px;
  font-family: GT-Walsheim-Medium;
  border: 3px solid #ffce4b;
  transition: .3s
}

.orderBtn a:hover {
  background: 0 0;
  border: 3px solid #ffce4b
}

section.orderEssay {
  padding-top: 70px;
  min-height: 755px
}

.orderEssayNow {
  max-width: 1170px;
  display: flex;
  justify-content: space-between;
  padding-top: 70px;
  width: 100%;
  margin: 0 auto
}

.h-two {
  font-size: 48px;
  font-family: GT-Walsheim-Medium;
  font-weight: 500;
  text-align: center;
  margin-bottom: 0;
  margin-top: 0
}

.h-three {
  font-size: 29px;
  line-height: 44px
}

.orderEssayNow .services {
  grid-template-columns: 1fr 1fr;
  margin-left: 50px;
  margin-top: 75px
}

.orderEssayNow .services .singleService {
  box-shadow: 1px 1px 5px 1px rgb(0 0 0 / 25%);
}

.orderForm {
  max-width: 262px;
  width: 100%;
  box-shadow: 0 1px 7px 2px rgb(0 0 0 / 25%);
  padding: 45px;
  position: relative
}

.small-text {
  margin-top: 10px;
  font-size: 16px
}

.medium-text {
  font-family: GT-Walsheim-Medium;
  font-size: 25px;
  margin: 0
}

.orderForm p.medium-text {
  margin-bottom: 40px
}

.howItWorks,
.paper,
.reviews,
.securePayment {
  background: #f8f8f8;
  padding-top: 70px
}
.howItWorks{
  padding-top: 100px;
}
.securePayment {
  padding-bottom: 30px
}
.faqs ul{
  padding-left: 40px;
}
.reviews {
  padding-bottom: 90px;
  position: relative;
  background-image: url(images/bg-paperPattern.svg);
  background-repeat: no-repeat;
  background-size: inherit
}

.reviews .inner {
  max-width: 1300px !important;
  width: 100%;
  margin: 0 auto
}

.swiper-button-next,
.swiper-button-prev {
  color: #35414b
}

.swiper-button-next:after,
.swiper-button-prev:after {
  font-size: 24px;
  font-weight: 800
}

.swiper-slide p strong {
  font-size: 20px;
  font-weight: 600;
  line-height: 1.8
}

.swiper-wrapper {
  align-items: center
}

.reviews .swiper-container {
  position: relative;
  padding: 100px 0
}

.swiper-container>.swiper-slide__content {
  position: absolute;
  top: 0
}

.swiper-container>.swiper-slide__content {
  position: absolute;
  top: 0
}

.swiper-slide .slide-text {
  background: #fff;
  transition: all .2s linear;
  border: 1px solid #249dec;
  box-shadow: 0 4px 10px 5px rgba(36, 157, 236, .1);
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  padding: 25px;
  max-width: 600px
}

.swiper-slide.swiper-slide-active .slide-text {
  transform: scale(1.5);
  position: relative;
  z-index: 99;
  border: none;
  box-shadow: 0 4px 20px 5px rgb(36 157 236 / 10%)
}

.swiper-slide.swiper-slide-prev .slide-text {
  margin-left: 50px
}

.swiper-slide.swiper-slide-next .slide-text {
  margin-right: 50px;
  cursor: default
}

.swiper-slide .slide-text h6 {
  font-size: 18px;
  margin-bottom: 10px
}

.swiper-slide .slide-text p {
  font-size: 16px;
  text-align: justify
}

.swiper-slide .slide-text .slideInfo {
  min-height: 127px
}

.author {
  margin-top: 15px;
  display: flex;
  justify-content: space-between;
  width: 100%;
  align-items:flex-start;
}

.author p {
  font-size: 12px !important
}

.swiper-slide__content {
  height: 300px
}

.swiper-slide img {
  display: block;
  width: 100%;
  height: 100%;
  object-fit: cover
}

.step {
  display: flex;
  padding: 26px 16px;
  box-shadow: 0 4px 5px rgb(0 0 0 / 25%);
  border-radius: 65px;
  background: #fff;
  position: relative;
  align-items: center
}

.step::after {
  content: '';
  background-image: url(images/down-arrow.svg);
  position: absolute;
  width: 25px;
  height: 73px;
  z-index: 1;
  top: 156px;
  left: 250px;
  background-repeat: no-repeat
}

.step p {
  font-size: 29px;
  line-height: 38px;
  margin-left: 115px
}

.howItWorks .wrapper {
  max-width: 1010px;
  display: flex;
  justify-content: space-between;
  padding-top: 72px;
  width: 100%;
  margin: 0 auto
}

.hierarchy {
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  padding-bottom: 38px;
  max-width: 560px;
  width: 100%;
  min-height: 635px
}

.hierarchy .step:last-child::after {
  display: none
}
.hierarchy .step p{
  padding-bottom: 0;
}

select#pages {
  -webkit-appearance: none;
  max-width: 192px;
  width: 100%;
  margin-left: 30px;
  border-radius: 0
}

.minus,
.plus {
  width: 18px;
  height: 20px;
  background: #ffce4b;
  border-radius: 4px;
  padding: 5px 5px 6px 5px;
  border: 1px solid #35414b;
  display: inline-block;
  vertical-align: middle;
  text-align: center;
  font-size: large;
  cursor: pointer
}

.plus {
  position: absolute;
  right: 57px;
  bottom: 106px;
  font-weight: bolder;
  border-radius: 0 8px 8px 0;
  padding: 3px 5px 5px 5px
}

.minus {
  position: absolute;
  left: 46px;
  bottom: 106px;
  font-weight: 400;
  border-radius: 8px 0 0 8px;
  padding: 3px 5px 5px 5px
}

input,
select {
  max-width: 250px;
  width: 100%;
  min-height: 30px;
  border: 1px solid #35414b;
  box-sizing: border-box;
  border-radius: 8px;
  margin-bottom: 26px;
  margin-top: 5px;
  outline: 0;
  padding-left: 30px;
  appearance: none;
  -webkit-appearance: none;
  -moz-appearance: none;
  appearance: none;
  background-color: #fff;
}

.downIcon {
  position: relative
}

.downIcon::before {
  content: '';
  width: 0;
  height: 0;
  border-left: 5px solid transparent;
  border-right: 5px solid transparent;
  border-top: 6px solid #35414b;
  position: absolute;
  top: 6px;
  right: 9px;
  pointer-events: none
}

input[type=date]::-webkit-calendar-picker-indicator,
input[type=date]::-webkit-inner-spin-button {
  -webkit-appearance: none
}

input[type=date] {
  position: relative
}

input[type=date]::-webkit-calendar-picker-indicator {
  background: 0 0;
  bottom: 0;
  color: transparent;
  cursor: pointer;
  height: auto;
  left: 0;
  position: absolute;
  right: 0;
  top: 0;
  width: auto
}

input[type=submit] {
  font-family: GT-Walsheim-Medium;
  margin-bottom: 0;
  background: #ffce4b;
  font-weight: 500;
  font-size: 16px;
  line-height: 18px;
  text-align: center;
  max-width: 170px;
  width: 100%;
  margin-left: 45px;
  padding-left: 0
}

section.whyTrust {
  padding-top: 70px;
  padding-bottom: 75px
}

.whyTrust .h-width {
  max-width: 721px;
  width: 100%;
  margin: 0 auto
}

.whyTrust .services {
  display: flex;
  flex-direction: column;
  grid-row-gap: 120px
}

.whyTrust .wrapper {
  display: flex;
  justify-content: center;
  align-items: center;
  padding-top: 70px
}

.whyTrust .serviceText {
  max-width: 550px;
  width: 100%;
  margin-left: 35px
}

.whyTrust .serviceText p.small-text {
  font-family: GT-Walsheim-Regular;
  font-weight: 300
}

.serviceText p strong {
  font-weight: 600;
  font-size: 19px
}

.subTitle {
  font-size: 29px;
  max-width: 997px;
  width: 100%;
  margin: 0 auto;
  line-height: 33.2px;
  text-align: justify;
  margin-top: 70px;
  position: relative;
  font-family: GT-Walsheim-Regular;
  font-weight: 300
}

tag {
  color: #249dec;
  font-weight: 500;
  font-family: GT-Walsheim-Medium;
  font-size: 22px;
  line-height: 25px;
  background: #fff;
  box-shadow: -4px -4px 4px rgb(0 0 0 / 8%), 4px 4px 4px rgb(0 0 0 / 8%);
  padding: 12px 41px;
  display: flex;
  justify-content: center;
  max-width: 460px;
  width: 100%;
  margin: 54px auto;
  position: relative
}

.reviews .subTitle::after {
  content: '';
  position: absolute;
  background-image: url(images/quote-left.svg);
  background-repeat: no-repeat;
  background-size: cover;
  width: 133px;
  height: 121px;
  top: 146px;
  right: -12px
}

.reviews .subTitle::before {
  content: '';
  position: absolute;
  background-image: url(images/bg-paperPattern.svg);
  background-repeat: no-repeat;
  background-size: cover
}

.tab {
  float: left;
  width: 30%;
  max-width: 260px;
  width: 100%;
  padding-top: 30px;
  z-index: 1;
  margin: 0 auto;
  position: relative
}

.tab::before {
  content: '';
  position: absolute;
  width: 15px;
  height: 95%;
  background: #249dec;
  border-radius: 344px;
  left: -65px
}

.tab button {
  display: block;
  color: #249dec;
  padding: 9px 30px;
  outline: 0;
  text-align: left;
  cursor: pointer;
  transition: .3s;
  font-size: 21px;
  font-family: GT-Walsheim-Medium;
  font-weight: 500;
  background-color: #fff;
  min-width: 350px;
  width: 100%;
  border: 2px solid #249dec;
  margin-bottom: 50px;
  box-sizing: border-box;
  border-radius: 344px;
  text-align: center
}

.tab button.active {
  background-color: #249dec;
  color: #fff;
  position: relative
}

button.tablinks.active::before {
  content: '';
  position: absolute;
  width: 54px;
  height: 10px;
  background-color: #249dec;
  top: 14px;
  left: -55px
}

.tabcontent {
  padding: 0 12px;
  max-width: 350px;
  border-left: none;
  width: 100%;
  float: left;display: none;
  z-index: -1
}
.tabcontent.active{display: block;}

.tabcontent p {
  font-weight: 300;
  font-size: 25px;
  line-height: 29px;
  text-align: justify;
  font-family: GT-Walsheim-Regular
}

.weOffer {
  padding-top: 70px;
  padding-bottom: 30px
}

.weOffer .wrapper {
  max-width: 1196px;
  width: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
  position: relative;
  margin: 0 auto
}

.tabcontent:before {
  content: '';
  position: absolute;
  background-image: url(images/light-bg.svg);
  inset: 0;
  background-repeat: no-repeat;
  background-position: right;
  background-size: contain
}

.tabCircle::after {
  content: '';
  position: absolute;
  max-width: 174px;
  width: 100%;
  min-height: 203px;
  border: 14px solid #9dd8ff;
  border-radius: 50%;
  top: -1px;
  left: -2px;
  padding: 0 16px;
  box-shadow: inset -4px 0 4px rgb(0 0 0 / 25%), inset 4px 0 4px rgb(0 0 0 / 25%), inset 0 -4px 4px rgb(0 0 0 / 25%), inset 0 4px 4px rgb(0 0 0 / 25%)
}

.tabCircle p {
  padding: 40px;
  font-size: 29px
}

.tabCircle::before {
  content: '';
  position: absolute;
  width: 98px;
  min-height: 15px;
  background-color: #249dec;
  top: 110px;
  left: 241px
}

.tabCircle {
  max-width: 204px;
  width: 100%;
  min-height: 85px;
  border: 14px solid #249dec;
  border-radius: 50%;
  padding: 25px 13px;
  text-align: center;
  position: relative
}

section.faqs {
  padding-top: 70px;
  padding-bottom: 60px
}

section.faqs .wrapper {
  display: flex;
  justify-content: center
}

.accordionWrapper {
  background: #fff;
  float: left;
  max-width: 915px;
  width: 100%;
  box-sizing: border-box;
  padding-top: 70px
}

.accordionItem {
  float: left;
  display: block;
  width: 100%;
  box-sizing: border-box;
  font-family: Open-sans, Arial, sans-serif
}

.accordionItemHeading {
  cursor: pointer;
  margin: 0 0 22px 0;
  padding: 25px 30px;
  background: #fff;
  color: #2b3543;
  width: 100%;
  border-radius: 8px;
  box-shadow: 0 2px 14px rgb(124 124 124 / 50%);
  box-sizing: border-box;
  position: relative;
  font-size: 20px
}

.close .accordionItemContent {
  height: 0;
  transition: transform .4s ease;
  -webkit-transform: scaleY(0);
  -o-transform: scaleY(0);
  -ms-transform: scaleY(0);
  transform: scaleY(0);
  float: left;
  display: block;
  -webkit-transition: -webkit-transform .4s ease-out
}

.open .accordionItemContent {
  padding: 20px;
  background-color: #fff;
  border: 1px solid #ddd;
  width: 100%;
  margin: 0 0 10px 0;
  display: block;
  -webkit-transform: scaleY(1);
  -o-transform: scaleY(1);
  -ms-transform: scaleY(1);
  transform: scaleY(1);
  -webkit-transform-origin: top;
  -o-transform-origin: top;
  -ms-transform-origin: top;
  transform-origin: top;
  -webkit-transition: -webkit-transform .4s ease-in;
  -o-transition: -o-transform .4s ease;
  -ms-transition: -ms-transform .4s ease;
  transition: transform .4s ease;
  box-sizing: border-box
}

.open .accordionItemHeading {
  margin: 0;
  border-bottom-right-radius: 0;
  border-bottom-left-radius: 0
}

.accordionItem.close {
  transition: .4s ease
}

.loadMore a {
  color: #249dec;
  font-weight: 900;
  font-size: 14px;
  line-height: 17px;
  text-transform: uppercase
}

.loadMore {
  width: 100%;
  float: left;
  margin: 0 auto;
  text-align: center;
  margin-top: 40px
}

h5.accordionItemHeading::before {
  content: '';
  position: absolute;
  background-image: url(images/plus.svg);
  top: 24px;
  right: 38px;
  background-repeat: no-repeat;
  pointer-events: none;
  width: 13px;
  height: 14px
}

.open .accordionItemHeading::before {
  background-image: url(images/minus.svg);
  top: 35px
}

.securePayment p {
  font-size: 40px;
  line-height: 46px;
  font-size: 40px;
  line-height: 46px;
  text-align: center;
  margin: 0;
  padding-top: 30px;
  padding-bottom: 40px
}

.badges {
  display: flex;
  justify-content: center;
  padding-top: 54px;
  padding-bottom: 40px
}

.badges .wrapper {
  max-width: 980px;
  width: 100%;
  display: flex;
  justify-content: space-between;
  margin: 0 auto
}

.badges .wrapper li {
  position: relative
}

.badges .wrapper li:first-child::before {
  content: '';
  position: absolute;
  background-image: url(images/2000-Students.svg);
  background-repeat: no-repeat;
  top: 58px;
  right: 0px;
  left: 44px;
  bottom: 0;
}

.badges .wrapper li:nth-child(2)::before {
  content: '';
  position: absolute;
  background-image: url(images/satisfactionRate.svg);
  background-repeat: no-repeat;
  top: 50px;
  right: 0px;
  left: 42px;
  bottom: 0;
}

.badges .wrapper li:nth-child(3)::before {
  content: '';
  position: absolute;
  background-image: url(images/30-days-moneyBack.svg);
  background-repeat: no-repeat;
  top: 48px;
  right: 0px;
  left: 42px;
  bottom: 0;
}

.badges .wrapper li:last-child::before {
  content: '';
  position: absolute;
  background-image: url(images/Success-Rate.svg);
  background-repeat: no-repeat;
  top: 54px;
  right: 0px;
  left: 62px;
  bottom: 0;
}

.paymentGateways {
  margin: 0 auto;
  width: 100%;
  display: flex;
  justify-content: center
}

.paymentMethod {
  background-image: url(images/paymentGateways.png);
  margin-right: 50px
}

.masterCard {
  width: 137px;
  height: 85px;
  background-position: -167px -115px
}

.visa {
  width: 137px;
  height: 85px;
  background-position: -10px -115px
}

.xpress {
  width: 137px;
  height: 85px;
  background-position: -167px -10px
}

.discover {
  width: 137px;
  height: 85px;
  background-position: -10px -10px
}

.girlSideImg img {
  width: inherit;
  position: absolute
}

.readdiv {
  width: 100%;
  float: left;
  padding: 40px 0
}

.none {
  display: none
}
.read-more{
  width: 100%;
  float: left;
  padding-top: 40px;
}
.read-more p {
  text-align: center
}

.readdiv .container {
  max-width: 1170px;
  width: 100%;
  margin: 0 auto
}

.read-more a {
  font-size: 20px;
  color: #337ab7;
  font-family: GT-Walsheim-Regular
}

.readdiv h2 {
  font-size: 30px;
  padding-bottom: 15px
}

.readdiv h3 {
  font-size: 25px;
  padding-bottom: 15px
}

.readdiv h4 {
  font-size: 20px;
  padding-bottom: 15px
}

.readdiv ul {
  padding-left: 40px
}

li,
p {
  font-size: 15px;
  padding-bottom: 15px;
  font-family: GT-Walsheim-Regular
}


@media (max-width:1600px) {
  .container {
    max-width: 1170px;
    width: 100%
  }
  .heroImage img {
    width: 555px
  }
  .tabCircle::before {
    width: 80px
  }
  h1 {
    font-size: 42px
  }
  .singleService {
    padding: 15px 5px;
  }
  .services {
    margin-top: 30px;
    grid-row-gap: 0px;
  }
  .whyTrust .serviceText p.small-text {
    padding-right: 20px;
    text-align: justify
  }
  .small-text {
    font-size: 20px
  }
  .reviews .inner {
    max-width: 1140px !important
  }
  .banner{
    padding-top: 50px;
    margin-top: 30px;
  }
}
@media (max-width:1300px) {
  .heroImage img {
    width: 400px;
    height: 600px
  }
  .whyTrust .services .singleService{
    max-height: 100%;
  }
  .girlSideImg {
    width: 278px
  }
  .girlSideImg img {
    width: inherit;
    position: absolute;
    right: -62px
  }
  .orderBtn {
    margin-top: 70px
  }
  .serviceText {
    margin-left: 10px
  }
  .orderBtn a {
    font-size: 30px
  }
  .orderEssayNow {
    max-width: 1065px
  }
  p.small-text {
    font-size: 15px !important
  }
  .orderEssayNow .services{
    margin-left: 23px;
  }

  .orderForm{
    max-width: 230px;
    width: 100%;
    padding: 30px;
  }
  .minus{
    bottom:91px;
    left: 31px;
  }
  .plus{
    right: 28px;
    bottom: 91px;
  }
  .banner{
    margin-top: 0px;
  }
}
@media (max-width:1200px) {
  .reviews .inner {
    max-width: 920px !important
  }
  .tab button {
    min-width: 275px;
    font-size: 15px
  }
  .swiper-slide .slide-text .slideInfo {
    min-height: 128px
  }
  .tab::before {
    left: -14px
  }
  .swiper-slide .slide-text p {
    font-size: 14px
  }
  .swiper-slide.swiper-slide-active .slide-text {
    margin: 0 auto
  }
  .orderEssayNow {
    max-width: 990px;
    width: 100%
  }

  .container {
    max-width: 930px;
    width: 100%
  }
  .paper .wrapper {
    max-width: 910px
  }
  .tabCircle::before {
    width: 38px
  }
  .girlSideImg img {
    right: -40px !important
  }
  .weOffer .container {
    max-width: 980px;
    padding: 0
  }
  .orderForm {
    padding: 20px
  }

  .minus {
    bottom: 81px;
    left: 29px

  }
  .plus {
    right: 21px;
bottom: 81px;
  }
  section.orderEssay {
    min-height: 660px
  }
  .paperText h2 {
    font-size: 32px
  }
  .orderBtn a {
    font-size: 27px;
    padding: 14px 20px
  }
  .step p {
    font-size: 24px
  }
  .subTitle {
    font-size: 24px;
    max-width: 725px;
    margin-top: 20px
  }
  .securePayment p {
    font-size: 24px
  }
  .heroImage img {
    width: 350px !important
  }
  h1 {
    font-size: 50px
  }
  .tab {
    padding-left: 18px
  }
  button.tablinks.active::before {
    width: 38px;
    left: -31px
  }
  .weOffer .wrapper {
    max-width: 980px
  }
  .readdiv .container {
    max-width: 920px;
    width: 100%;
    margin: 0 auto
  }
  .badges .container {
    max-width: 920px !important
  }
  .badges ul.wrapper {
    padding-left: 0
  }
  ul.navItems a{
    font-size: 16px;
  }
  .services{
    grid-column-gap: 16px;
  }
  .orderEssayNow .services{
    margin-left: 18px;
  }
}



@media (max-width:992px) {
  .container {
    max-width: 740px;
    width: 100%
  }
  .bannerText {
    max-width: 515px;
    width: 100%
  }
  .weOffer .container {
    max-width: 900px
  }
  .services {
    grid-column-gap: 35px;
    grid-row-gap: 35px
  }
  .whyTrust .services{
    grid-row-gap: 0px;
  }
  .whyTrust .serviceText{
    max-width: 100%;
  }
  .paperText{
    max-width: 500px;
    width: 100%;
  }
  section.orderEssay {
    padding-top: 40px;
  }
  .reviews .swiper-container {
    padding: 0
  }
  .swiper-slide.swiper-slide-active .slide-text {
    transform: none;
    margin: 0 auto
  }
  .swiper-button-next:after,
  .swiper-button-prev:after {
    font-size: 16px;
    font-weight: 700
  }
  .swiper-button-prev,
  .swiper-container-rtl .swiper-button-next {
    left: 0
  }
  .swiper-button-next,
  .swiper-container-rtl .swiper-button-prev {
    right: 0
  }
  .howItWorks .wrapper {
    max-width: 935px
  }
  .heroImage {
    max-width: 300px;
    margin-top: -10px
  }
  .heroImage img {
    width: 210px !important;
    height: 510px
  }
  .banner {
    padding-top: 65px
  }
  .banner .container {
    max-width: 720px
  }
  .step {
    padding: 19px 16px
  }
  .step p {
    font-size: 20px
  }
  .paper .wrapper {
    max-width: 745px
  }
  .girlImage img {
    max-width: 300px;
    width: 100%
  }
  .girlSideImg {
    width: 245px;
    margin-top: -33px
  }
  .girlSideImg img {
    right: 0 !important
  }
  .sprite6,
  .sprite7,
  .sprite8 {
    width: 160px !important
  }

  .accordionWrapper {
    max-width: 730px
  }
  h4.accordionItemHeading::before {
    left: 680px
  }
  .howItWorks,
  .paper,
  .reviews,
  .securePayment {
    padding-top: 30px
  }
  .badges {
    padding-top: 40px;
    padding-bottom: 20px
  }
  .badges .wrapper {
    max-width: 650px !important;
    display: flex;
    justify-content: center !important
  }
  .badges img {
    width: 76px;
    height: 93px
  }
  .securePayment p {
    font-size: 16px;
    line-height: 20px
  }
  .badges .wrapper li:first-child::before {
    background-image: url(images/sm-students.svg);
    top: 21px;
    left: 22px
  }
  .badges .wrapper li:nth-child(2)::before {
    background-image: url(images/sm-rate.svg);
    top: 19px;
    left: 15px
  }
  .badges .wrapper li:nth-child(3)::before {
    background-image: url(images/sm-moneyback.svg);
    top: 19px;
    left: 15px
  }
  .badges .wrapper li:last-child::before {
    background-image: url(images/sm-success.svg);
    top: 21px;
    left: 22px
  }
  .badges .wrapper {
    justify-content: center
  }
  .badges .wrapper li {
    padding-right: 50px
  }
  h1 {
    font-size: 40px
  }
  .h-two {
    font-size: 38px
  }
  .h-three {
    font-size: 23px
  }
  .tabcontent p {
    font-size: 16px
  }
  .weOffer .wrapper {
    max-width: 800px
  }
  .tab {
    max-width: 210px;
    padding-left: 45px
  }
  .tab button {
    max-width: 183px;
    font-size: 16px;
    min-width: 180px
  }
  .tabCircle {
    max-width: 197px;
    min-height: 198px;
    padding: 10px 11px
  }
  .tabCircle::before {
    display: none
  }
  .tabCircle p {
    padding: 55px 28px;
    font-size: 24px
  }
  .tabCircle::after {
    max-width: 159px;
    min-height: 161px;
    top: 0;
    left: -4px;
    padding: 15px 20px
  }
  button.tablinks.active::before {
    top: 15px;
    width: 38px
  }
  .tab::before {
    left: 7px;
    height: 95%
  }
  .hierarchy .step:first-child {
    padding: 19px 72px 19px 16px
  }
  p.small-text {
    font-size: 15px !important
  }
  .orderEssayNow {
    max-width: 842px;
    padding-top: 30px;
    flex-direction: column;
    align-items: center;
    padding-bottom: 30px
  }
  .orderForm {
    padding: 15px
  }
  .orderForm p.medium-text {
    margin-bottom: 15px
  }
  input,
  select {
    max-width: 210px;
    margin-bottom: 18px
  }
  select#pages {
    max-width: 166px;
    margin-left: 15px
  }
  .minus {
    left: 20px;
    bottom: 68px
  }
  .plus {
    right: 46px;
    bottom: 68px
  }
  input[type=submit] {
    font-size: 17px;
    max-width: 203px;
    margin-left: 7px
  }
  .small-text {
    font-size: 14px
  }
  .readdiv .container {
    max-width: 820px;
    box-sizing: border-box;
  }
  .reviews .inner {
    max-width: 690px !important
  }
  .weOffer .container {
    max-width: 720px
  }
  .badges .container {
    max-width: 820px !important
  }
  .badges .wrapper li:last-child {
    padding-right: 0
  }
  .whyTrust .services .spriteIcons {
    background-image: url(images/trust-mobile-sprite.png)
  }
  .sprite12 {
    max-width: 90px;
    height: 110px;
    background-position: -240px -146px
  }
  .sprite13 {
    max-width: 90px;
    height: 110px;
    background-position: -126px -146px
  }
  .sprite14 {
    max-width: 90px;
    height: 110px;
    background-position: -240px -12px
  }
  .sprite15 {
    max-width: 90px;
    height: 110px;
    background-position: -12px -146px
  }
  .sprite16 {
    max-width: 90px;
    height: 110px;
    background-position: -126px -12px
  }
  .sprite17 {
    max-width: 90px;
    height: 110px;
    background-position: -12px -12px
  }
  .subTitle{
    font-size: 22px;
  }
}

@media (max-width:767px) {
  .container {
    max-width: 550px
  }

  .banner .container {
    max-width: 550px
  }
  .bannerText {
    max-width: 370px;
    padding-left: 10px;
    min-height: 100vh;
  }
  .banner .bannerText .services{
    flex-direction: column;
  }
  .reviews .inner {
    max-width: 550px !important
  }
  .swiper-container {
    max-width: 400px
  }
  .tab::before {
    height: 95%
  }
  section.whyTrust .swiper-container {
    display: none
  }
  h1 {
    font-size: 28px;
    max-width: auto;
    width: 100%
  }
  .services {
    grid-template-columns: 220px 220px;
    display: flex;
    flex-direction: column
  }
  .orderEssayNow {
    flex-direction: row
  }
  .orderEssayNow .services {
    margin-top: 30px;
    padding: 25px;
  }
  .sprite6 {
    width: 275px
  }
  .sprite8 {
    width: 306px
  }
  .sprite7 {
    width: 230px
  }

  .orderEssay .singleService p {
    font-size: 14px
  }
  .spriteIcons {
    background-image: url(images/mobile-sprite1.png)
  }
  .sprite1 {
    max-width: 53px;
    height: 55px;
    background-position: -85px -8px;
    width: 100%
  }
  .sprite2 {
    max-width: 53px;
    height: 55px;
    background-position: -1060px -958px;
    width: 100%
  }
  .sprite3 {
    max-width: 53px;
    height: 55px;
    background-position: -685px -956px;
    width: 100%
  }
  .sprite4 {
    max-width: 53px;
    height: 55px;
    background-position: -1810px -721px;
    width: 100%
  }
  .orderEssayNow {
    flex-direction: column;
    align-items: center
  }
  .orderEssayNow .singleService {
    padding: 20px
  }
  .spriteOrder {
    background-image: url(images/mobile_sprite_order_essay.png)
  }
  .sprite5 {
    max-width: 80px;
    height: 100px;
    background-position: -20px -156px;
    width: 100%
  }
  .sprite6 {
    width: 90px !important;
    height: 110px;
    background-position: -18px -10px
  }
  .sprite7 {
    width: 90px !important;
    height: 110px;
    background-position: -138px -10px
  }
  .sprite8 {
    width: 108px !important;
    height: 110px;
    background-position: -258px -10px
  }
  .sprite-bg {
    width: 34px;
    height: 35px
  }
  h4.accordionItemHeading::before {
    left: 330px
  }
  .step {
    padding: 8px 12px
  }
  .step::after {
    background-image: url(images/mobile-arrow.svg);
    top: 48px;
    left: 105px
  }
  input[type=submit] {
    border: none;
    max-width: 170px;
    margin-left: 45px;
  }
  h5.accordionItemHeading::before {
    top: 29px;
    right: 24px
  }
  .paymentGateways {
    max-width: 300px;
    width: 100%;
    margin: 0 auto
  }
  .paymentGateways img {
    width: 300px;
    height: 40px;
  }
  .orderBtn a {
    font-size: 26px;
    padding: 10px 15px;
    margin: 0 auto;
    text-align: center;
    display: flex;
    justify-content: center
  }
  .paper .wrapper {
    max-width: 620px;
    justify-content: unset;
    margin-bottom: 30px;
    flex-direction: row
  }
  .banner {
    padding-top: 80px
  }
  .badges .wrapper {
    justify-content: center
  }
  .badges .wrapper li {
    padding-right: 20px
  }
  .heroImage {
    margin-top: 0
  }
  .heroImage img {
    width: 189px !important;
    height: 340px
  }
  .tabCircle::before {
    display: block;
    left: 105px;
    width: 10px;
    height: 38px;
    top: 227px
  }
  .paperText h2 {
    font-size: 24px
  }
  .paperText p strong {
    font-size: 20px
  }
  section.faqs {
    padding: 30px 0
  }
  .girlSideImg img {
    width: 300px
  }
  .orderForm {
    max-width: 240px
  }
  .subTitle {
    margin-top: 30px;
    max-width: 300px;
    font-size: 14px;
    line-height: 20px
  }
  section {
    padding-top: 30px;
    paddding-bottom: 30px
  }
  .plus {
    right: 46px
  }
  .girlSideImg {
    width: 229px;
    height: 184px;
    margin-top: -37px;
    position: relative
  }
  .paperText h2 {
    font-size: 20px;
    line-height: 24px;
    padding-bottom: 0
  }
  .paperText p strong {
    font-size: 14px;
    line-height: 18px
  }
  .paperText .orderBtn {
    margin-top: 5px;
    text-align: center
  }
  .paperText .orderBtn a {
    font-size: 12px;
    padding: 10px 15px;
    margin: 0 auto;
    text-align: center;
    display: inline-block
  }
  .paper {
    padding-bottom: 0
  }
  section.whyTrust {
    padding-bottom: 0
  }
  .accordionWrapper {
    padding-top: 30px;
    max-width: 364px
  }
  .accordionItemHeading {
    font-size: 14px
  }
  .reviewText {
    line-height: 20px;
    text-align: justify;
    min-height: auto;
    font-size: 14px
  }
  .reviewer {
    flex-direction: column
  }
  .reviewer p {
    font-size: 12px
  }
  .paperText {
    padding-top: 30px
  }
  .girlSideImg img {
    width: 131px;
    top: 36px;
    right: 0
  }
  .bannerText .services {
    display: flex;
    flex-direction: row;
    flex-wrap: wrap;
    margin-left: 0
  }
  .bannerContent .wrapper {
    justify-content: flex-start
  }


  .bannerText .orderBtn {
    display: none
  }
  .singleService {
    padding: 0
  }
  .singleService p {
    font-size: 17px;
    padding: 15px 0px;
  }
  .h-two {
    font-size: 20px
  }
  .medium-text {
    font-size: 16px
  }
  .orderEssayNow {
    flex-direction: column;
    align-items: center
  }

  .orderEssayNow .services .singleService {
    box-shadow: 0 1px 7px 2px rgb(0 0 0 / 25%);
    padding: 8px;
    margin-bottom: 20px;
  }

  section.orderEssay {
    padding-top: 30px;
    /* margin-top: -200px; */
  }
  .howItWorks .step .spriteIcons {
    background-image: url(images/how-mobile-sprite.png)
  }
  .sprite9 {
    max-width: 34px;
    height: 34px;
    background-position: -93px -6px
  }
  .sprite10 {
    max-width: 36px;
    height: 27px;
    background-position: -47px -7px
  }
  .sprite11 {
    max-width: 31px;
    height: 28px;
    background-position: -6px -9px
  }
  .hierarchy {
    max-width: 230px;
    width: 100%;
    margin: 0 auto;
    padding-bottom: 0;
    min-height: auto
  }
  .step {
    margin-bottom: 35px
  }

  .step p {
    font-size: 12px;
    line-height: 14px;
    margin-left: 54px
  }
  .howItWorks .wrapper {
    max-width: 400px;
    padding-top: 20px
  }
  .whyTrust .services {
    display: flex;
    flex-direction: column;
    margin-bottom: 25px;
    grid-row-gap: 25px
  }
  section.whyTrust {
    padding-top: 30px
  }
  .whyTrust .wrapper {
    display: block
  }
  .girlImage img {
    width: 123px;
    height: 221px
  }

  tag {
    max-width: 304px;
    font-size: 15px;
    padding: 11px 30px;
    margin-top: 20px
  }
  .weOffer .container {
    max-width: 550px;
    width: 100%;

  }
  .badges .wrapper {
    max-width: 600px
  }
  .badges ul.wrapper {
    padding-left: 0
  }
  .weOffer .wrapper {
    flex-direction: column
  }
  .tabcontent {
    padding-top: 30px
  }
  .tabcontent p {
    font-size: 18px
  }
  .tab button {
    margin-bottom: 20px
  }
  .h-three {
    font-size: 20px;
    line-height: 24px;
    text-align: center
  }
  .readdiv .container {
    max-width: 520px
  }
  .hierarchy .step:first-child {
    padding: 8px 12px
  }
}

@media(max-width:575px) {
  h1{
    text-align: center;
    padding-bottom: 0px;
  }

  .heroImage {
    display: none
  }
  .banner .bannerText .services {
    flex-direction: row;
    flex-wrap: wrap
  }
  .container {
    max-width: 375px
  }
  .banner .container,
  .reviews .inner,
  .weOffer .container {
    max-width: 375px !important
  }
  .open .accordionItemHeading::before {
    top: 26px
  }
  .readdiv h2 {
    font-size: 22px
  }
  section.whyTrust .swiper-container {
    display: block
  }
  .swiper-pagination {
    bottom: 14px;
    left: 114px
  }
  .swiper-pagination-bullet {
    width: 16px;
    height: 6px;
    border-radius: 40px;
    margin-right: 8px;
    background: #766262
  }
  .swiper-pagination-bullet-active {
    opacity: 1;
    background: #35414b;
    width: 32px;
    height: 6px;
    border-radius: 40px
  }
  .swiper-slide .slide-text p {
    text-align: left;
    padding-left: 20px;
    padding-bottom: 0
  }
  .whyTrust .services {
    display: none
  }
  .whyTrust .swiper-slide .singleService {
    padding-left: 0
  }
  .whyTrust .swiper-container .spriteIcons {
    background-image: url(images/trust-mobile-sprite.png)
  }
  .whyTrust .swiper-slide .slide-text {
    box-shadow: none
  }
  .whyTrust .slide-text .singleService:nth-child(2) {
    margin-top: 56px
  }
  .whyTrust .serviceText {
    margin-left: 0
  }
  .whyTrust .swiper-slide p strong {
    font-size: 16px
  }
  .whyTrust .swiper-slide .slide-text {
    padding: 34px 0
  }
  .whyTrust .swiper-slide .slide-text {
    max-width: 374px !important
  }
  .whyTrust .swiper-slide {
    padding-bottom: 55px
  }
  .swiper-container-rtl .swiper-button-next,
  .whyTrust .swiper-button-prev {
    left: 82px;
    top: 90%
  }
  .swiper-container-rtl .swiper-button-prev,
  .whyTrust .swiper-button-next {
    right: 95px;
    top: 90%
  }
  .whyTrust .wrapper {
    padding-top: 15px
  }
  .paperText h2 {
    font-size: 15px
  }
  .paper .wrapper {
    max-width: 520px;
    padding-bottom: 30px
  }
  .reviews {
    padding-bottom: 30px
  }
  h5.accordionItemHeading::before {
    top: 38%;
    right: 20px
  }

  .badges .wrapper {
    max-width: 315px !important
  }
  .badges .wrapper ul {
    padding-left: 0
  }
  .reviews .subTitle::after {
    width: 40px;
    height: 40px;
    top: 165px;
    right: -44px
  }
  .weOffer {
    padding-top: 30px
  }
  .tab {
    max-width: fit-content;
    margin-right: 0;
    border-left: none;
    padding-top: 41px;
    border-top: 5px solid #249dec;
    padding-left: 14px
  }
  .tab::before {
    display: none
  }
  .tab button {
    width: 45%;
    float: left;
    display: inline-block;
    min-width: 160px;
    padding: 5px 8px;
    font-size: 14px
  }
  .tabcontent p {
    line-height: 22px
  }
  .tab button:nth-child(2n) {
    margin-left: 20px
  }
  button.tablinks.active::before {
    display: none
  }
  .swiper-slide .slide-text {
    max-width: 320px !important;
    width: 100%
  }
  .swiper-slide.swiper-slide-prev .slide-text {
    margin-left: 0
  }
  .swiper-slide.swiper-slide-next .slide-text {
    margin-right: 0
  }
  .tabCircle {
    max-width: 101px;
    height: 101px;
    padding: 12px 9px;
    min-height: 101px;
    margin-bottom: 53px
  }
  .tabCircle::after {
    max-width: 99px;
    min-height: 99px;
    padding: 0;
    box-shadow: inset -4px 0 4px rgb(0 0 0 / 25%), inset 4px 0 4px rgb(0 0 0 / 25%), inset 0 -4px 4px rgb(0 0 0 / 25%), inset 0 4px 4px rgb(0 0 0 / 25%)
  }
  .tabCircle p {
    padding: 18px !important;
    font-size: 14px;
    line-height: 21px
  }
  .tabCircle::before {
    left: 57px;
    width: 10px;
    height: 56px;
    top: 138px
  }
  .weOffer .wrapper {
    max-width: 412px
  }
  .reviews .subTitle::after {
    display: none
  }
  .orderForm {
    display: flex;
    flex-direction: column;
    align-items: center;
    max-width: 330px
  }
  input,
  select {
    max-width: 250px
  }
  select#pages {
    max-width: 218px
  }
  .orderForm p.medium-text {
    margin-left: -130px
  }
  .plus {
    right: 56px
  }
  .minus {
    left: 56px
  }
  .sprite5 {
    max-width: 80px
  }
  .sprite6 {
    width: 146px !important
  }
  .sprite8 {
    width: 150px !important
  }
  .sprite7 {
    width: 118px !important
  }
  .serviceText p strong {
    font-size: 18px
  }
  .whyTrust .h-width {
    max-width: 304px
  }
  .subTitle {
    max-width: 360px
  }
  .swiper-slide .slide-text .slideInfo {
    font-size: 14px;
    line-height: 18px
  }
  .accordionItemHeading {
    padding: 16px 30px
  }
  h4.accordionItemHeading::before {
    top: 19px
  }
  .open .accordionItemContent {
    margin: 20px 0 10px 0;
    box-shadow: 0 2px 8px rgb(124 124 124 / 50%);
    border-radius: 8px
  }
  .loadMore {
    margin-top: 20px
  }
  .services {
    grid-row-gap: 20px
  }
  .spriteOrder {
    min-width: 85px
  }
  .badges ul {
    padding-left: 0
  }
  .bannerText .singleService {
    width: 100%;
    max-height: unset;
    flex-direction: column;
  }
  .readdiv .container {
    max-width: 350px
  }
  .hierarchy .step:first-child {
    padding: 8px 10px
  }
  .girlSideImg img {
    right: -26px !important
  }
  .orderEssayNow .services{
    margin-left: 0px;
  }
}

@media(max-width:400px) {
  .readdiv .container {
    max-width: 300px
  }
  .container {
    padding: 0
  }
  .tab button {
    width: 32%
  }
  .orderForm {
    max-width: 290px
  }
  select#pages {
    max-width: 190px;
    margin-left: 33px
  }
  .minus {
    left: 40px
  }
  .plus {
    right: 38px
  }
  .sprite6 {
    width: 157px !important
  }
  .sprite7 {
    width: 138px !important
  }
  .sprite8 {
    width: 178px !important
  }
  .step p {
    margin-left: 38px
  }
  .tab button {
    min-width: 163px
  }
  .paperText {
    padding-left: 10px;
    max-width: 300px;
    padding-left: 10px;
    padding-bottom: 20px
  }
  .paperText h2 {
    font-size: 16px
  }
  .girlSideImg img {
    right: 0 !important
  }
  .paper .wrapper {
    padding: 0
  }

}

@media(max-width:360px) {
  .badges .container {
    padding: 0 !important
  }
  .girlSideImg {
    display: none
  }
  .tabCircle {
    height: 99px;
    min-height: 99px
  }
  .tabCircle p {
    padding: 20px !important
  }
  .tabCircle::after {
    min-height: 95px
  }
  .tabCircle::before {
    top: 136px
  }
  .dsktp-none {
    max-width: 254px
  }
  tag {
    max-width: 250px;
    font-size: 12px;
    padding: 11px 10px;
    margin-top: 20px
  }
  .tabcontent {
    max-width: 270px;
    padding: 0;
    padding-top: 20px;
  }
  .orderEssayNow {
    max-width: 274px
  }
  .orderEssayNow .services {
    padding: 0
  }
  .orderEssayNow .singleService {
    padding: 0;
    margin-bottom: 7px
  }
  .tab {
    max-width: 295px;
    margin: 0 auto;
    width: 100%;
    border-left: 0;
    padding-left: 0
  }
  .tab button {
    min-width: 128px;
    font-size: 11px
  }
  .swiper-container {
    width: 100%;
    float: left
  }
  .swiper-container {
    width: 100%;
    float: left
  }
  .rev-inner {
    width: 100%;
    float: left
  }
  .container {
    max-width: 300px
  }
  .accordionItemHeading {
    padding: 15px 30px
  }
  h4.accordionItemHeading::before {
    left: 275px;
    top: 22px
  }
  .weOffer .wrapper {
    max-width: 290px
  }
  .swiper-slide .slide-text {
    max-width: 250px !important
  }
  .paperText {
    padding-left: 14px
  }
  .paperText h2 {
    font-size: 14px
  }
  .tabcontent p {
    font-size: 14px
  }
  .siteLogo img {
    width: 80px
  }
  .whyTrust .swiper-slide .slide-text {
    max-width: 300px !important
  }
  .step {
    padding: 8px 10px
  }
  .step::after {
    top: 48px;
    left: 92px
  }
  .subTitle {
    max-width: 275px
  }
  .tab button {
    font-size: 10px
  }
  .paper .wrapper {
    max-width: 330px;
    margin-bottom: 20px;
    padding: 20px 0
  }
  .badges .wrapper li {
    padding-right: 2px
  }
  .badges .container {
    max-width: 300px !important
  }
  .paperText {
    padding-top: 0
  }
  .read-more a {
    font-size: 15px
  }

  .singleService p{
    font-size: 10px;
  }
  .orderEssay .singleService p{
    padding: 0px;
  }
  .whyTrust .singleService{
    max-height: 100%;
  }
  .whyTrust .slide-text .singleService:nth-child(2){
    margin-top: 10px;
  }
  .whyTrust .swiper-slide .slide-text {
    padding: 5px 0px;
  }
}
</style>
</head>
<body>
  <section class="banner">
    <div class="container">
      <div class="bannerContent">
        <div class="wrapper">
          <div class="bannerText">
            <h1 class="">Essay Writing Service</h1>
            <div class="services">
              <div class="singleService ">
                <div class="spriteIcons sprite1"></div>
                <p>100+ Writing Experts</p>
              </div>
              <div class="singleService">
                <div class="spriteIcons sprite2"></div>
                <p>Money Back Guarantee</p>
              </div>
              <div class="singleService">
                <div class="spriteIcons sprite3"></div>
                <p>100+ Plagiarism-Free Content</p>
              </div>
              <div class="singleService">
                <div class="spriteIcons sprite4"></div>
                <p>Unique and Premium Quality Papers</p>
              </div>
            </div>
            <div class="orderBtn">
              <a href="javascript:void(0)">Calculate Now</a>
            </div>
          </div>
          <div class="heroImage">
            <picture>
              <source type="image/webp" srcset="" data-src="">
                <img src="images/hero-image.svg" alt="Student">
              </picture>
            </div>
          </div>
        </div>
      </div>
    </section>
    <section class="orderEssay">
      <div class="container">
        <h2 class="h-two">Order Your Essay Now</h2>
        <div class="orderEssayNow">
          <div class="orderForm">
            <p class="medium-text"><strong>I need Help with</strong></p>
            <form>
              <label for="work_type">Document Type</label> <br>
              <span class="sprite-formIcons docIcon"></span>
              <select name="work_type" id="">
                <option value="" disabled selected hidden>What can we do for you?</option>
                <option value="Admission Essay">Admission Essay</option>
                <option value="Analytical Essay">Analytical Essay</option>
                <option value="Annotated Bibliography">Annotated Bibliography</option>
                <option value="Application Letter">Application Letter</option>
                <option value="Argumentative Essay">Argumentative Essay</option>
                <option value="Assessment">Assessment</option>
                <option value="Assignment ">Assignment </option>
                <option value="Biography">Biography</option>
                <option value="Book Reports ">Book Reports </option>
                <option value="Book Reviews ">Book Reviews </option>
                <option value="Brief Overview">Brief Overview</option>
                <option value="Business Plan">Business Plan</option>
                <option value="Case Study ">Case Study </option>
                <option value="College Paper ">College Paper </option>
                <option value="Contrast Essay">Contrast Essay</option>
                <option value="Coursework ">Coursework </option>
                <option value="Cover Letter">Cover Letter</option>
                <option value="Creative Writing">Creative Writing</option>
                <option value="Critical analysis">Critical analysis</option>
                <option value="Critical Thinking">Critical Thinking</option>
                <option value="Dissertation">Dissertation</option>
                <option value="eBooks ">eBooks </option>
                <option value="Essay">Essay</option>
                <option value="Exposition Writing">Exposition Writing</option>
                <option value="Homework ">Homework </option>
                <option value="Lab Report ">Lab Report </option>
                <option value="Literature Review">Literature Review</option>
                <option value="Movie Review ">Movie Review </option>
                <option value="News Release ">News Release </option>
                <option value="Online assignment">Online assignment</option>
                <option value="Personal Statement">Personal Statement</option>
                <option value="Powerpoint Presentation (with speaker notes)">Powerpoint Presentation (with speaker notes)</option>
                <option value="Powerpoint Presentation (without speaker notes)">Powerpoint Presentation (without speaker notes)</option>
                <option value="Quiz">Quiz</option>
                <option value="Reflection paper">Reflection paper</option>
                <option value="Reflective Essay">Reflective Essay</option>
                <option value="Report">Report</option>
                <option value="Research Essay">Research Essay</option>
                <option value="Research Paper">Research Paper</option>
                <option value="Research proposal ">Research proposal </option>
                <option value="Response Essay">Response Essay</option>
                <option value="Response paper">Response paper</option>
                <option value="Scholarship Essay">Scholarship Essay</option>
                <option value="School Paper ">School Paper </option>
                <option value="Speech ">Speech </option>
                <option value="Term Paper">Term Paper</option>
                <option value="Thesis">Thesis</option>
                <option value="Thesis Proposal">Thesis Proposal</option>
                <option value="other">Other</option>
              </select> <br>
              <label for="academic-level">Academic Level</label> <br>
              <span class="sprite-formIcons levelIcon"></span>
              <select name="level" id="level" >
                <option value="" disabled selected hidden>You study at?</option>
                <option value="high_school">High school</option>
                <option value="college-undergraduate">College-undergraduate</option>
                <option value="master">Master</option>
                <option value="doctoral">Doctoral</option>
              </select> <br>
              <label for="deadline">Select Deadline</label> <br>
              <span class="sprite-formIcons calendarIcon"></span>
              <input type="date" name="deadline" value="" id="deadline"><span class="downIcon"></span> <br>
              <label for="pages">No. of Pages</label> <br>
              <span onclick="decrementPagesOption()" class="minus">-</span>
              <select name="pages" id="pages">
                <option value="1">1 Page ~ 300 Words</option>
                <option value="2">2 Pages ~ 600 Words</option>
                <option value="3">3 Pages ~ 900 Words</option>
                <option value="4">4 Pages ~ 1200 Words</option>
                <option value="5">5 Pages ~ 1500 Words</option>
                <option value="6">6 Pages ~ 1800 Words</option>
                <option value="7">7 Pages ~ 2100 Words</option>
                <option value="8">8 Pages ~ 2400 Words</option>
                <option value="9">9 Pages ~ 2700 Words</option>
                <option value="10">10 Pages ~ 3000 Words</option>
                <option value="11">11 Pages ~ 3300 Words</option>
                <option value="12">12 Pages ~ 3600 Words</option>
                <option value="13">13 Pages ~ 3900 Words</option>
                <option value="14">14 Pages ~ 4200 Words</option>
                <option value="15">15 Pages ~ 4500 Words</option>
                <option value="16">16 Pages ~ 4800 Words</option>
                <option value="17">17 Pages ~ 5100 Words</option>
                <option value="18">18 Pages ~ 5400 Words</option>
                <option value="19">19 Pages ~ 5700 Words</option>
                <option value="20">20 Pages ~ 6000 Words</option>
                <option value="21">21 Pages ~ 6300 Words</option>
                <option value="22">22 Pages ~ 6600 Words</option>
                <option value="23">23 Pages ~ 6900 Words</option>
                <option value="24">24 Pages ~ 7200 Words</option>
                <option value="25">25 Pages ~ 7500 Words</option>
                <option value="26">26 Pages ~ 7800 Words</option>
                <option value="27">27 Pages ~ 8100 Words</option>
                <option value="28">28 Pages ~ 8400 Words</option>
                <option value="29">29 Pages ~ 8700 Words</option>
                <option value="30">30 Pages ~ 9000 Words</option>
                <option value="31">31 Pages ~ 9300 Words</option>
                <option value="32">32 Pages ~ 9600 Words</option>
                <option value="33">33 Pages ~ 9900 Words</option>
                <option value="34">34 Pages ~ 10200 Words</option>
                <option value="35">35 Pages ~ 10500 Words</option>
                <option value="36">36 Pages ~ 10800 Words</option>
                <option value="37">37 Pages ~ 11100 Words</option>
                <option value="38">38 Pages ~ 11400 Words</option>
                <option value="39">39 Pages ~ 11700 Words</option>
                <option value="40">40 Pages ~ 12000 Words</option>
                <option value="41">41 Pages ~ 12300 Words</option>
                <option value="42">42 Pages ~ 12600 Words</option>
                <option value="43">43 Pages ~ 12900 Words</option>
                <option value="44">44 Pages ~ 13200 Words</option>
                <option value="45">45 Pages ~ 13500 Words</option>
                <option value="46">46 Pages ~ 13800 Words</option>
                <option value="47">47 Pages ~ 14100 Words</option>
                <option value="48">48 Pages ~ 14400 Words</option>
                <option value="49">49 Pages ~ 14700 Words</option>
                <option value="50">50 Pages ~ 15000 Words</option>
                <option value="51">51 Pages ~ 15300 Words</option>
                <option value="52">52 Pages ~ 15600 Words</option>
                <option value="53">53 Pages ~ 15900 Words</option>
                <option value="54">54 Pages ~ 16200 Words</option>
                <option value="55">55 Pages ~ 16500 Words</option>
                <option value="56">56 Pages ~ 16800 Words</option>
                <option value="57">57 Pages ~ 17100 Words</option>
                <option value="58">58 Pages ~ 17400 Words</option>
                <option value="59">59 Pages ~ 17700 Words</option>
                <option value="60">60 Pages ~ 18000 Words</option>
                <option value="61">61 Pages ~ 18300 Words</option>
                <option value="62">62 Pages ~ 18600 Words</option>
                <option value="63">63 Pages ~ 18900 Words</option>
                <option value="64">64 Pages ~ 19200 Words</option>
                <option value="65">65 Pages ~ 19500 Words</option>
                <option value="66">66 Pages ~ 19800 Words</option>
                <option value="67">67 Pages ~ 20100 Words</option>
                <option value="68">68 Pages ~ 20400 Words</option>
                <option value="69">69 Pages ~ 20700 Words</option>
                <option value="70">70 Pages ~ 21000 Words</option>
                <option value="71">71 Pages ~ 21300 Words</option>
                <option value="72">72 Pages ~ 21600 Words</option>
                <option value="73">73 Pages ~ 21900 Words</option>
                <option value="74">74 Pages ~ 22200 Words</option>
                <option value="75">75 Pages ~ 22500 Words</option>
                <option value="76">76 Pages ~ 22800 Words</option>
                <option value="77">77 Pages ~ 23100 Words</option>
                <option value="78">78 Pages ~ 23400 Words</option>
                <option value="79">79 Pages ~ 23700 Words</option>
                <option value="80">80 Pages ~ 24000 Words</option>
                <option value="81">81 Pages ~ 24300 Words</option>
                <option value="82">82 Pages ~ 24600 Words</option>
                <option value="83">83 Pages ~ 24900 Words</option>
                <option value="84">84 Pages ~ 25200 Words</option>
                <option value="85">85 Pages ~ 25500 Words</option>
                <option value="86">86 Pages ~ 25800 Words</option>
                <option value="87">87 Pages ~ 26100 Words</option>
                <option value="88">88 Pages ~ 26400 Words</option>
                <option value="89">89 Pages ~ 26700 Words</option>
                <option value="90">90 Pages ~ 27000 Words</option>
                <option value="91">91 Pages ~ 27300 Words</option>
                <option value="92">92 Pages ~ 27600 Words</option>
                <option value="93">93 Pages ~ 27900 Words</option>
                <option value="94">94 Pages ~ 28200 Words</option>
                <option value="95">95 Pages ~ 28500 Words</option>
                <option value="96">96 Pages ~ 28800 Words</option>
                <option value="97">97 Pages ~ 29100 Words</option>
                <option value="98">98 Pages ~ 29400 Words</option>
                <option value="99">99 Pages ~ 29700 Words</option>
                <option value="100">100 Pages ~ 30000 Words</option>
              </select>
              <span onclick="incrementPagesOption()" class="plus">+</span>
              <br>
              <input type="submit" value="Calculate now">
            </form>
          </div>
          <div class="services">
            <div class="singleService">
              <div class="spriteOrder sprite5"></div>
              <div class="serviceText">
                <p><strong>Native Writer</strong> </p>
                <p class="small-text">Graduated from reputed U.S. universities, subject specialists.</p>
              </div>
            </div>
            <div class="singleService">
              <div class="spriteOrder sprite6"></div>
              <div class="serviceText">
                <p><strong>24/7 Customer Support</strong> </p>
                <p class="small-text">Live chat, text, and call. Contact us any time of the day, and night.</p>
              </div>
            </div>
            <div class="singleService">
              <div class="spriteOrder sprite7"></div>
              <div class="serviceText">
                <p><strong>Direct Communication</strong> </p>
                <p class="small-text">Direct communication with your writer. Free and any time.</p>
              </div>
            </div>
            <div class="singleService">
              <div class="spriteOrder sprite8"></div>
              <div class="serviceText">
                <p><strong>100% Refund</strong> </p>
                <p class="small-text">Get your money back in case you do not like what you get. Nothing to lose.</p>
              </div>
            </div>
          </div>
        </div>
      </section>
      <section class="howItWorks" id="howItWorks">
        <h2 class="h-two">How Our Essay Writing Service Works?</h2>
        <div class="wrapper">
          <div class="girlImage">
            <picture>
              <source type="image/webp" srcset="" data-src="">
                <img src="images/how-it-works-opt.svg" alt="Girl Image">
              </picture>
            </div>
            <div class="hierarchy">
              <div class="step">
                <div class="sprite-bg">
                  <div class="spriteIcons sprite9"></div>
                </div>

                <p>Place your order by filling an order form</p>
              </div>
              <div class="step">
                <div class="sprite-bg">
                  <div class="spriteIcons sprite10"></div>
                </div>
                <p>Our U.S. - based writer gets busy on your project</p>
              </div>
              <div class="step">
                <div class="sprite-bg">
                  <div class="spriteIcons sprite11"></div>
                </div>
                <p>Your project beats the deadline and shows up IN YOUR INBOX</p>
              </div>
            </div>
          </div>
        </div>
      </section>
      <section class="whyTrust">
        <div class="container">
          <div class="h-width">
            <h2 class="h-two">Here is why students trust us with their papers</h2>
          </div>
          <div class="wrapper">
            <div class="services">
              <div class="singleService">
                <div class="spriteIcons sprite12"></div>
                <div class="serviceText">
                  <p><strong>Top Quality Assignments</strong> </p>
                  <p class="small-text">We work with professional writers that only deliver premium quality and 100% unique assignments. The team of subject experts is native English speakers who have expertise in all academic subjects.</p>
                </div>
              </div>
              <div class="singleService">
                <div class="spriteIcons sprite13"></div>
                <div class="serviceText">
                  <p><strong>Timely Delivery </strong> </p>
                  <p class="small-text">Short deadlines are not a problem for us. We pledge by delivering the work on or before the given deadline. Whether it’s a 6-8 hours deadline, we promise timely and best writing service.</p>
                </div>
              </div>
              <div class="singleService">
                <div class="spriteIcons sprite14"></div>
                <div class="serviceText">
                  <p><strong>Unlimited Free Revisions</strong> </p>
                  <p class="small-text">We provide unlimited free revisions and edits within your deadlines. No matter if it’s 2nd time or the 20th, we are always here to meet your requirements until you are satisfied.</p>
                </div>
              </div>
            </div>
            <div class="services">
              <div class="singleService">
                <div class="spriteIcons sprite15"></div>
                <div class="serviceText">
                  <p><strong>Guaranteed Privacy and Confidentiality </strong> </p>
                  <p class="small-text">We are committed to protecting your privacy. We will NEVER reveal your identity to any third party.</p>
                </div>
              </div>
              <div class="singleService">
                <div class="spriteIcons sprite16"></div>

                <div class="serviceText">
                  <p><strong>24/7 Customer Support Team</strong> </p>
                  <p class="small-text">We'll be available around the clock to help you out. If you have any queries, don't hesitate to reach us! We're always happy when we can make your day a little bit easier with our expertise and knowledge.</p>
                </div>
              </div>
              <div class="singleService">
                <div class="spriteIcons sprite17"></div>
                <div class="serviceText">
                  <p><strong>Online Order Tracking</strong> </p>
                  <p class="small-text">Your account on our website gives you complete access to your order details. You can directly communicate with the writer and track your work progress. Online chat is also available to answer your queries.</p>
                </div>
              </div>
            </div>
            <!-- Mobile Services Swiper -->
            <div class="swiper-container">
              <div class="swiper-wrapper">
                <div class="swiper-slide">
                  <div class="slide-text">
                    <div class="singleService">
                      <div class="spriteIcons sprite12"></div>
                      <div class="serviceText">
                        <p><strong>Top Quality Assignments</strong> </p>
                        <p class="small-text">We work with professional writers that only deliver premium quality and 100% unique assignments. The team of subject experts is native English speakers who have expertise in all academic subjects.</p>
                      </div>
                    </div>
                    <div class="singleService">
                      <div class="spriteIcons sprite13"></div>
                      <div class="serviceText">
                        <p><strong>Timely Delivery </strong> </p>
                        <p class="small-text">Short deadlines are not a problem for us. We pledge by delivering the work on or before the given deadline. Whether it’s a 6-8 hours deadline, we promise timely and best writing service.</p>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="swiper-slide">
                  <div class="slide-text">
                    <div class="singleService">
                      <div class="spriteIcons sprite14"></div>
                      <div class="serviceText">
                        <p><strong>Unlimited Free Revisions</strong> </p>
                        <p class="small-text">We provide unlimited free revisions and edits within your deadlines. No matter if it’s 2nd time or the 20th, we are always here to meet your requirements until you are satisfied. </p>
                      </div>
                    </div>
                    <div class="singleService">
                      <div class="spriteIcons sprite15"></div>
                      <div class="serviceText">
                        <p><strong>Guaranteed Privacy and Confidentiality</strong> </p>
                        <p class="small-text">We are committed to protecting your privacy. We will NEVER reveal your identity to any third party. </p>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="swiper-slide">
                  <div class="slide-text">
                    <div class="singleService">
                      <div class="spriteIcons sprite16"></div>
                      <div class="serviceText">
                        <p><strong>24/7 Customer Support Team</strong> </p>
                        <p class="small-text">We'll be available around the clock to help you out. If you have any queries, don't hesitate to reach us! We're always happy when we can make your day a little bit easier with our expertise and knowledge.</p>
                      </div>
                    </div>
                    <div class="singleService">
                      <div class="spriteIcons sprite17"></div>
                      <div class="serviceText">
                        <p><strong>Online Order Tracking</strong> </p>
                        <p class="small-text">Your account on our website gives you complete access to your order details. You can directly communicate with the writer and track your work progress. Online chat is also available to answer your queries.</p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <!--  navigation buttons -->
              <div class="nav-pagination">
                <div class="swiper-button-prev"></div>
                <div class="swiper-button-next"></div>
                <!-- <div class="swiper-pagination"></div> -->
              </div>
            </div>
          </div>
        </div>
      </section>
      <section class="reviews">
        <div class="container inner">
          <div class="rev-inner">
            <h2 class="h-two">Reviews</h2>
            <p class="subTitle">EssayHours.com is renowned as the global source for professional paper writing services at all academic levels. Our team is based in the U.S. We’re not an offshore “paper mill” grinding out questionable research and inferior writing. But don’t take our word for it.</p>
            <tag>Rated 4.7 / 5 based on 2079 student reviews.</tag>
          </div>
          <div class="swiper-container">
            <div class="swiper-wrapper">
              <!-- Slides -->
              <div class="swiper-slide">
                <div class="slide-text">
                  <h6>“Best Service to Hire”</h6>
                  <p class="slideInfo">“It was a relief to start my PhD program at Stanford, but the last six months were absolute hell. Juggling my first career internship while working on research in Psychology would have been way more difficult without the help I got from Essayhours. The fact that they helped me understand all of those instructions made it possible for me to do what I needed as an intern and researcher so easily!”</p>
                  <div class="author"><p>Leo Matthew</p>
                    <div class="star">
                      <p>5/5</p>
                      <picture>
                        <source type="image/webp" srcset="" data-src="">
                          <img src="images/rating-star.svg" alt="rating-star">
                        </picture>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="swiper-slide">
                  <div class="slide-text">
                    <h6>“Great Work!”</h6>
                    <p class="slideInfo">“ I was really excited when I finally earned my MBA in Economics. That is until the time came to put together a meaningful thesis for it! The professional team Essayhours had no problem coming up with research data and great writing, though, so that made things easy enough.”</p>
                    <div class="author"><p>Christian Robert</p>
                      <div class="star">
                        <p>5/5</p>
                        <picture>
                          <source type="image/webp" srcset="" data-src="">
                            <img src="images/rating-star.svg" alt="rating-star">
                          </picture>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="swiper-slide">
                    <div class="slide-text">
                      <h6>“Highly Satisfied”</h6>
                      <p class="slideInfo">“I was desperate for a paper that would be on time, and you delivered. The essay you wrote for me was amazing! I didn’t think it would be this good. It is so nice to know that there are still reliable people in the world, even when things seem impossible and the worst-case scenario seems inevitable. Thank you so much. I will make sure to use your services again in the future! ”</p>
                      <div class="author"><p>Xavier Brooks </p>
                        <div class="star">
                          <p>5/5</p>
                          <picture>
                            <source type="image/webp" srcset="" data-src="">
                              <img src="images/rating-star.svg" alt="rating-star">
                            </picture>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="swiper-slide">
                      <div class="slide-text">
                        <h6>“Great Experience ”</h6>
                        <p class="slideInfo">“I am so glad that my friend told me about this service. I had a lot of trouble with formatting and citations, but Essayshours helped me through it all in time for the deadline. My paper was formatted exactly the way it needed to be and cited properly. Will definitely come back again!”</p>
                        <div class="author"><p>George Bentley</p>
                          <div class="star">
                            <p>5/5</p>
                            <picture>
                              <source type="image/webp" srcset="" data-src="">
                                <img src="images/rating-star.svg" alt="rating-star">
                              </picture>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="swiper-slide">
                        <div class="slide-text">
                          <h6>“Best Academic Writing Service”</h6>
                          <p class="slideInfo">“On the day my roommate told me about them, I had no idea where to go and what next steps to take. After checking out their website online, they seemed legit, so I placed an order for a research paper on time management with a two weeks deadline. It was delivered before its due date! Great experience. ”</p>
                          <div class="author"><p>Edward Nicolas </p>
                            <div class="star">
                              <p>5/5</p>
                              <picture>
                                <source type="image/webp" srcset="" data-src="">
                                  <img src="images/rating-star.svg" alt="rating-star">
                                </picture>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <!--  navigation buttons -->
                      <div class="swiper-button-prev"></div>
                      <div class="swiper-button-next"></div>
                    </div>
                  </div>
                </section>
                <section class="weOffer">
                  <div class="container">
                    <div class="wrapper">
                      <div class="tabCircle">
                        <p><strong>Writing Services We Offer</strong> </p>
                      </div>
                      <div class="tab">
                        <button class="tablinks active" onclick="openTab(event, 'tab1')">College Essays</button>
                        <button class="tablinks" onclick="openTab(event, 'tab2')">Thesis Writing & Dissertations</button>
                        <button class="tablinks" onclick="openTab(event, 'tab3')">Research Papers</button>
                        <button class="tablinks" onclick="openTab(event, 'tab4')">Term Papers</button>
                        <button class="tablinks" onclick="openTab(event, 'tab5')">Custom Paper Writing</button>
                        <button class="tablinks" onclick="openTab(event, 'tab6')">Editing & Proofreading</button>
                      </div>
                      <div id="tab1" class="tabcontent active">
                        <h3 class="h-three">College Essays</h3>
                        <p>Get your college essay done in no time with the help of our writing service. We are known for providing high-quality papers that will impress any teacher. We also complete them within a short deadline without compromising quality, so you don't have to worry about not finishing on time!</p>
                      </div>
                      <div id="tab2" class="tabcontent">
                        <h3 class="h-three">Thesis Writing & Dissertations</h3>
                        <p>Eager to have a well-written thesis or dissertation that will put you ahead of your classmates in the race for success? We're here to help! With 100% original and unique writing from PhD writers, we'll make sure your paper is one of its kind.</p>
                      </div>
                      <div id="tab3" class="tabcontent">
                        <h3 class="h-three">Research Papers</h3>
                        <p>Our team of experts guarantees that your research papers will be formatted in the required style before the deadline. We use a variety of citation styles, including APA and MLA, to format your papers.</p>
                      </div>
                      <div id="tab4" class="tabcontent">
                        <h3 class="h-three">Term Papers</h3>
                        <p>If you're looking for a professional term paper writing service that will guarantee your success, then look no further. Our team of native and professional writers works tirelessly to provide impeccable term papers written specifically to suit the needs of our customers.</p>
                      </div>
                      <div id="tab5" class="tabcontent">
                        <h3 class="h-three">Custom Paper Writing</h3>
                        <p>Our team of creative writers specializes in crafting compelling content for blogs, whitepapers, business plans, and web content writing.</p>
                      </div>
                      <div id="tab6" class="tabcontent">
                        <h3 class="h-three">Editing and Proof Reading</h3>
                        <p>We offer expert editing and proofreading for all of your paper needs. This includes checking grammar, sentence structure, spelling, formatting style to get you the best grade possible on any assignment. We also offer proofreading services for essays, lab reports, case studies, book reviews, and papers.</p>
                      </div>
                    </div>
                  </div>
                </section>
                <section class="paper">
                  <div class="wrapper">
                    <div class="paperText">
                      <h2 class="h-two">Starting your Paper is one thing, Finishing it is another.</h2>
                      <p><strong>That's what we do, from start to finish.</strong> </p>
                      <p>Place your order now and calculate the price of your assignment. You will get the paper within the set deadline right into your INBOX.</p>
                      <div class="orderBtn">
                        <a href="javascript:void(0)">Calculate Now</a>
                      </div>
                    </div>
                    <div class="girlSideImg">
                      <img src="images/girl-portrait.png" alt="Girl with Phone portrait">
                    </div>
                  </div>
                </section>
                <div class="readdiv none">
                  <div class="container">
                    <h2>Get the Best ‘Write My Essay’ Help at Affordable Rates</h2>
                    <p>Do you need help with your essay? We have the right team for you. </p>
                    <p>Thousands of students face academic pressures as they are required to write different types of assignments. But with those expectations come unrealistic academic standards and a culture that values grades over learning. As it requires a lot of time to write such assignments, it leads them to look for professional help on essay writing.</p>
                    <p>With a variety of different writers from various fields, EssayHours.com can provide everything to suit your needs or requirements. If time is running out, you do not have good writing skills and are in need of assistance, our online service will be perfect for helping get those grades up!</p>
                    <p>Moreover, our paper writing service prides itself in providing the customers with professional writing services that they will be proud of and enjoy using. </p>
                    <p>Our expert writers are eager to help you when you come to us with your ‘I need someone to write my essay for me’ request. They know how important the work done by them on essays or any other assignment is.</p>
                    <h2>EssayHours.com - ‘Write My Paper’ is a Legit and Safe Option</h2>
                    <p>It is completely legitimate and safe to seek ‘write my paper’ help online. All you have to do is make sure you are working with a reliable and authentic essay writing service. </p>
                    <p>If you are struggling to complete your assignments in a timely manner, use our professional service and watch as we turn your stress into relief. We offer academic help from any field at an affordable price. </p>
                    <p>Remember, asking someone to help me write my essay is not illegal. We guarantee that getting our help does not violate college or university policies because it's completely legal.  </p>
                    <p>The paper you get from EssayHours.com is your property, and only you will have access to it. It is entirely up to you how to utilize it.</p>
                    <h2>Ways to Locate Websites That Offer Legit ‘Write My Essay’ Help</h2>
                    <p>There are many websites available online that claim to offer ‘write my essay for me free’ or ‘write my essay for me cheap’ help. They are not trustworthy, and many students become victims of such fraudulent companies. But why do they fail to recognize such sites? </p>
                    <p>We understand that it is difficult for you as a student to locate a reliable essay writing service. However, there are certain factors that will help guide your decision when choosing the right company.</p>
                    <ul>
                      <li>
                        <strong>Experience Level</strong>
                        <p>Make sure to choose a writing service that has an experienced team of writers. It is because they will work exactly the way you want.</p>
                      </li>
                      <li>
                        <strong>Customer Reviews and Testimonials</strong>
                        <p>An essay writing service will never post negative comments on its website. Therefore, always look for external comments and reviews on third-party websites. </p>
                      </li>
                      <li>
                        <strong>Check Out Their Terms and Conditions</strong>
                        <p>Fraudulent essay services are easy to spot if you pay attention to their terms and conditions. Usually, the signs for a fraudulent service can be found on the homepage or within the Terms of Services page. For example, an essay writer will charge less than $10 per page, which is way too cheap. </p>
                        <p>A reliable service like EssayHours.com offers high-quality essays within deadlines that match your needs at affordable prices. You can also have a look at our customer reviews and testimonials before making any decisions.</p>
                      </li>
                      <li>
                        <strong>Customer Support</strong>
                        <p>See if you can have access to the customer support team at any time of the day. Leave the website right away if you find out that it does not have any customer service. </p>
                      </li>
                    </ul>
                    <h2>EssayHours.com - Best ‘Write My Essay’ Website</h2>
                    <p>EssayHours.com is surely the best ‘write my essay’ website that you can trust for your academic needs. We provide expert and high-quality writing services to undergraduate and graduate students.</p>
                    <p>With our essay experts, you don't have to worry about the quality of work and deadline. We will make sure your work is delivered on time.</p>
                    <p>But what makes us the best site to write your paper? That is our dedication to providing the best possible service for everyone looking for a great paper. With us, you will get a quick response to your ‘help me write my essay’ request. </p>
                    <h2>Have Someone from EssayHours.com to Write My Essay </h2>
                    <p>You can have a college essay writer at EssayHours.com to write your essay. If you’re looking for quality services at an affordable price, then our service is exactly what you need.</p>
                    <p>We understand that there are many reasons when it is not just possible for students to complete assignments on time. In such situations, we have the perfect solution. We assign your task to a professional essay writer that will work according to your instructions.</p>
                    <p>With years of experience in the industry and thousands of satisfied customers from all around the world, trust us. We know how impressive an essay should be to help students get top grades. </p>
                    <h3>We Write Your Papers Without Plagiarism</h3>
                    <p>The professional essay writers at EssayHours.com can write a 100% unique paper for you written from scratch. </p>
                    <p>We have a strict policy against plagiarism. We do not sell pre-written content and always double-check the paper for originality. We also provide a plagiarism report to ensure that the assignment does not contain any copied content.</p>
                    <h2>Reasons Why Students Need Help from an Essay Writer Online</h2>
                    <p>Here are some reasons why students need help from an essay writer online. </p>
                    <ul>
                      <li>Lack of time </li>
                      <li>Difficult topic </li>
                      <li>Fear of poor grade</li>
                      <li>Other priorities</li>
                    </ul>
                    <p>If you are facing any of these issues, we can help you fix them. Whether it's a high school or college essay, our writers at EssayHours.com have got you covered!</p>
                    <h3>Hiring an Essay Writer is Not Illegal</h3>
                    <p>It is not illegal to hire an essay writer for writing your academic assignments. Instead, it is considered safe and legal to work with a professional expert. It is because they only aim to provide 100% original and non-plagiarized essays written from scratch. </p>
                    <p>If you hire an essay writer at EssayHours.com, have peace of mind that your paper is in safe hands. You can always count on us for unique, well-written, and well-formatted academic papers. We are just a click away.</p>
                    <h2>Steps to Hire a Professional Essay Writer at EssayHours.com </h2>
                    <p>When hiring a professional essay writer, it is important to be clear about what you need in your paper. Be sure that the requirements for this assignment are communicated and discussed with the assigned expert.</p>
                    <p>Follow the four simple steps for hiring a professional essay writer at EssayHours.com.</p>
                    <ul style="list-style:none">
                      <li>
                        <strong>1. Fill Out the Order Form</strong>
                        <p>We have created an order form on our website to make it easier for you. Here, provide all the information about your paper. If we need any more info from you, one of our custom essay writers will get back to you right away.</p>
                      </li>
                      <li>
                        <strong>2. Make the Payment</strong>
                        <p>After providing the information, the next step is to make the payment for your order so that we can assign a writer for you. Don't worry, your personal data is safe with us. In case we do not find an appropriate writer within 24 hours, we offer a money-back guarantee.</p>
                      </li>
                      <li>
                        <strong>3. Communicate With the Writer</strong>
                        <p>You can talk to your writer about the progress of your paper and make changes if you need them. They will gladly change anything that doesn't meet with satisfaction, even after they have started working on it.</p>
                      </li>
                      <li>
                        <strong>4. Receive the Paper</strong>
                        <p>Now just sit back and relax and let us deliver your paper within the given deadline. After we finish working on your assignment, you will receive a notification email. </p>
                      </li>
                    </ul>
                    <h3>Steps Our Writers Follow to Complete Your Assignments</h3>
                    <p>Here is how our essay writers work to complete your assignments.</p>
                    <ul>
                      <li>After you provide us your instructions through the order form, we will assign a suitable writer for you.</li>
                      <li>We start by composing an outline to guide the writer through the writing process.</li>
                      <li>Then our writers will locate relevant resources to craft unique arguments in proving your thesis statement. </li>
                      <li>Finally, the paper will be sent back for editing and proofreading to improve its quality before final submission. </li>
                      <li>You will receive a flawless paper within the set deadline. If you still want any revisions, we are here for you.</li>
                    </ul>
                    <h2>Benefits of Working With Our Essay Writers</h2>
                    <p>Here are the benefits of working with paper writers at EssayHours.com.</p>
                    <ul>
                      <li>
                        <strong>Highest Quality and Unique Papers</strong>
                        <p>By working with our professional writers, you will only get quality and 100% unique papers. We keep in mind all your instructions while working on your assignments.</p>
                      </li>
                      <li>
                        <strong>Affordable Rates</strong>
                        <p>We understand that it is not easy for students to earn or save money. Thus, our writers kept the prices low so that anyone can benefit from their services.</p>
                      </li>
                      <li>
                        <strong>Timely Delivery</strong>
                        <p>Meeting strict deadlines is the top priority of our writers. We make sure that all the papers are delivered within or before the deadline so that you get enough time to proofread them. </p>
                      </li>
                      <li>
                        <strong>Unlimited Free Revisions</strong>
                        <p>If you are not satisfied with the delivered work, you can ask for amendments. We offer free unlimited revisions until the customer is completely satisfied with the work’s quality.</p>
                      </li>
                    </ul>
                    <h3>Hire a Professional Essay Writer Service Now!</h3>
                    <p>Experienced and qualified writers in all high school and college disciplines are what make our essay writer service the best. With a fast writer's pace, we can provide your research papers or term papers on time! </p>
                    <p>You can ALWAYS count on us for papers that are 100% unique to you. No matter what field of study or academic level you are at, we're here to help. </p>
                    <p>With over a decade of experience in the field and helping thousands of students just like you with their essays, your success is our priority! We promise not to share your personal information with any third party. </p>
                    <p>Feel free and let one of our expert writers get started on creating an A+ paper for you from scratch. All it takes is to fill out the order form and request ‘write my paper’.</p>
                    <p>You are just a click away from hiring an expert today!</p>
                  </div>
                </div>
                <div class="read-more">
                  <p><a href="javascript:void(0)" class="readtext">Read More About EssayHours.com</a></p>
                </div>
                <section class="faqs">
                  <div class="container">
                    <h4 class="h-two">Frequently Asked Questions</h4>
                    <div class="wrapper">
                      <div class="accordionWrapper">
                        <div class="accordionItem close">
                          <h5 class="accordionItemHeading">How much should I pay someone to write my paper?</h5>
                          <div class="accordionItemContent">
                            <p>At EssayHours.com, you have to pay $15 to $30 for a high school essay. However, the following factors can affect our pricing for an academic paper.</p>
                            <ul>
                              <li>Number of pages</li>
                              <li>Academic level</li>
                              <li>Deadline </li>
                            </ul>
                          </div>
                        </div>
                        <div class="accordionItem close">
                          <h5 class="accordionItemHeading">Is it legal to pay someone to write a paper for you?</h5>
                          <div class="accordionItemContent">
                            <p>Yes, it is absolutely legal to pay a professional writer at EssayHours.com to write a paper for you. We have a team of experienced and qualified experts who work with the aim to produce high-quality and plagiarism-free papers.</p>
                          </div>
                        </div>
                        <div class="accordionItem close">
                          <h5 class="accordionItemHeading">Which website can help me with my essay?</h5>
                          <div class="accordionItemContent">
                            <p>EssayHours.com is the website that can help you write your essays or other academic assignments. If you are looking to get your essay done, simply place your order. We will compose a flawless essay for you by following your requirements.</p>
                          </div>
                        </div>
                        <div class="accordionItem close">
                          <h5 class="accordionItemHeading">Can you write essays for free?</h5>
                          <div class="accordionItemContent">
                            <p>No, we do not write essays for free. We know how tight on-budget students are. That's why we have made sure to keep our prices low. With this, you can get the best essay writing help, without having a huge price tag for it!</p>
                          </div>
                        </div>
                        <div class="accordionItem close">
                          <h5 class="accordionItemHeading">Should I use an online ‘write my essay’ service?</h5>
                          <div class="accordionItemContent">
                            <p>Yes, you should use an online ‘write my essay’ service if you are unable to write your papers. Professional services help students struggling with their academic assignments. So working with such a service can be of great writing help for you.</p>
                          </div>
                        </div>
                        <div class="accordionItem close">
                          <h5 class="accordionItemHeading">Can you trust essay writing sites? </h5>
                          <div class="accordionItemContent">
                            <p>Yes, you can trust reliable and legitimate essay writing services like EssayHours.com. Our main goal is to provide the best help to everyone coming to us with their ‘write my essay’ requests.</p>
                          </div>
                        </div>
                        <div class="accordionItem close">
                          <h5 class="accordionItemHeading">How fast can you write my college essay?</h5>
                          <div class="accordionItemContent">
                            <p>Professional writers at EssayHours.com can complete your assignments within a 6-12 hour deadline. However, short deadlines are not a problem for us though we recommend starting your college essays on time. </p>
                          </div>
                        </div>
                        <div class="accordionItem close">
                          <h5 class="accordionItemHeading">Can I pay someone to write my paper?</h5>
                          <div class="accordionItemContent">
                            <p>Yes, you can pay a professional essay writer to write an essay for you. Similarly, you can always buy an essay online when you are unable to complete it yourself due to any reason. </p>
                            <p>If you are feeling unmotivated or unable to finish your essay, make an order at EssayHours.com. We are a legit writing service that will write your paper from scratch. </p>
                            <p>We also offer a wide range of services that cater to all different needs - even if the deadline is tight. Our team of highly qualified, experienced, and fastest essay writers can write your essay within 6 hours. </p>
                          </div>
                        </div>
                        <div class="accordionItem close">
                          <h5 class="accordionItemHeading">Who can I pay to write my paper?</h5>
                          <div class="accordionItemContent">
                            <p>At EssayHours.com, you can pay a professional essay writer to write your paper. They are committed to helping students overcome their writing struggles. </p>
                            <p>If you need help on your paper and have no idea where to start, contact us today for custom paper writing services at affordable rates. With us, you don't have to concern yourself with deadlines or formatting any longer. We got your back!</p>
                            <p>Whether it’s a high school essay, college assignment, or a university thesis, just trust our essay writers.</p>
                          </div>
                        </div>
                        <div class="accordionItem close">
                          <h5 class="accordionItemHeading">Is it reliable to get help from a cheap essay writer?</h5>
                          <div class="accordionItemContent">
                            <p>No, it is not reliable to work with a free or cheap essay writer. We completely understand the most students are tight on the budget. But getting help from a ‘write my paper for cheap’ source is not an option. Keep in mind that top-quality and plagiarism-free papers will never come for cheap.</p>
                            <p>Instead, it is better to get reliable help from an affordable academic writing service. Legit writing services like EssayHours.com provide affordable paper writing services from experienced writers. </p>
                            <p>We DO NOT compromise on the quality and make sure everything comes to us get the best help at budget-friendly rates.</p>
                          </div>
                        </div>
                        <div class="accordionItem close">
                          <h5 class="accordionItemHeading">How long will it take for you to write my essay?</h5>
                          <div class="accordionItemContent">
                            <p>We focus on the given deadline. Thus, we write your paper as soon as possible to make sure that you get it back in time for submission. </p>
                            <p>The minimum time required to write a high school essay is 6 hours. But some papers like lab reports or case studies might take more work depending upon their topic and complexity.</p>
                            <p>Rest assured, EssayHours.com only hires qualified and capable writers who know how to produce original papers before the deadline. However, it is always better to place your order in advance so that we have enough time to polish your paper. </p>
                          </div>
                        </div>
                        <div class="loadMore">
                          <a href="javascript:;" id="next">View All frequently asked Questions <span><img src="images/arrow-right.svg" alt="Directional Arrow"> </span> </a>
                        </div>
                      </div>
                    </div>
                  </div>
                </section>
                <section class="securePayment">
                  <h2 class="h-two">Secure Payment</h2>
                  <p>We accept all major debit / credit cards</p>
                  <div class="wrapper">
                    <div class="paymentGateways">
                      <picture>
                        <source type="image/webp" srcset="" data-src="">
                          <img src="images/paymetMethods.svg" alt="Payment Methods">
                        </picture>
                      </div>
                    </div>
                  </section>
                  <section class="badges">
                    <div class="container">
                      <ul style="list-style:none;" class="wrapper">
                        <li><img src="images/badge-students.svg" alt="200+ Stisfied Students"></li>
                        <li><img src="images/badge-footer.svg" alt="95% Satisfaction Rate"></li>
                        <li><img src="images/badge-footer.svg" alt="30 Days Money Back guarantee"></li>
                        <li><img src="images/badge-footer.svg" alt="95% Success Rate"></li>
                      </ul>
                    </div>
                  </section>
