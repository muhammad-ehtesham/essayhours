<style media="screen">
.container{
  max-width: 1170px;
  width: 100%;
  margin: 0 auto;
  padding: 0 15px;
  border: none;
}
a{
  text-decoration: none;
}
.mobileNav{
  display: none;
}
/* .myscroll
{
  position: sticky;
  top: 0;
  left: 0;
  z-index: 9;
  background-color: #fff;
} */

.navBarSect{
  position: sticky;
  top: 0;
  left: 0;
  z-index: 9;
  background-color: #fff;
}
.af-overlay{
  display: none;
  position: fixed;
  top:0;
  bottom: 0;
  left: 0;
  right:0;
  width: 100%;
  height: 100%;
  background-color: rgba(0,0,0,0.4);
}
.af-overlay.show{
  display: block;
}
.mobileNav ul{
  list-style: none;
  padding-left: 0px;
}
.navbar {
  margin: 0 auto;
  padding: 9px 0px;
  width: 100%;
  /* justify-content: center; */
  box-shadow: 0px 4px 4px 0px #00000040;
}
.navbar a{
  color: #35414B;
  margin-right: 30px;
}
.navbar li{
  padding-bottom: 0px;
}
ul.ctaBtns a {
  font-size: 24px;
  font-weight: 500;
}
ul.ctaBtns li:first-child a {
  border: 3px solid #35414B;
  box-sizing: border-box;
  border-radius: 30px;
  padding: 10px 16px;
}
ul.ctaBtns li:last-child a {
  background: #249DEC;
  border: 3px solid #249DEC;
  box-sizing: border-box;
  border-radius: 30px;
  padding: 13px 29px;
  color: #fff !important;
  font-size: 24px;
}
ul.ctaBtns li:last-child a:hover{
  border: 3px solid #249DEC;
  background: transparent;
  color: #249DEC !important;
}
ul.navItems a {
  font-size: 20px;
  font-weight: 400;
}
.navbar ul{
  display:flex;
  list-style:none;
}
.navbar, .navItems, .ctaBtns{
  display: flex;
  align-items: center;
  justify-content: space-evenly;
}
@media (max-width:1200px){
  ul.ctaBtns a{
    font-size: 18px;
  }
  ul.ctaBtns li:first-child a{
    padding: 10px 13px;
  }
  ul.ctaBtns li:last-child a{
    font-size: 18px;
    padding: 9px 22px;
  }
  .navbar a{
    margin-right: 20px;
  }

}
@media (max-width:992px){
  .siteLogo img{
    width: 100px;
    height: 57px;
  }
  .navbar a {
    margin-right: 14px;
  }
  ul.navItems a{
    font-size: 15px;
  }
  ul.ctaBtns li:first-child a {
    padding: 6px;
    font-size: 16px;
  }
  ul.ctaBtns li:last-child a {
    font-size: 16px;
    padding: 7px 14px;
  }
}
@media (max-width:767px){
  .navbar{
    display: none;
  }
  section.navBarSect{
    padding-top: 10px;
  }

  .siteLogo img {
    width: 100px;
  }
  ul.ctaBtns a{
    font-size: 14px;
  }
  ul.ctaBtns li:first-child a{
    padding: 6px 7px;
  }
  ul.ctaBtns li:last-child a{
    padding: 6px 16px;
    font-size: 14px;
  }
  ul.ctaBtns li{
    padding-bottom: 0px;
  }
  .mobileNav{
    display: block;
    display: flex;
    align-items: center;
    padding: 0px 20px;
    box-shadow: 0px 4px 4px 0px #00000040;
  }
  ul.ctaBtns li {
    margin-left: 15px;
  }
  .bar1 {
    background-image: url(images/navToggler.svg);
    width: 25px;
    height: 20px;
    position: absolute;
    right: 19px;
    top: 32px;
  }
  .sidenav {
    height: 100%;
    width: 0;
    position: fixed;
    z-index: 1;
    top: 0;
    right: 0;
    background-color: #202024;
    overflow-x: hidden;
    transition: 0.5s;
    padding-top: 60px;
  }
  .sidenav a {
    padding: 8px 8px 8px 32px;
    text-decoration: none;
    font-size: 25px;
    color: #fff;
    display: block;
    transition: 0.3s;
  }
  .sidenav a:hover {
    color: #f1f1f1;
  }
  .sidenav .closebtn {
    position: absolute;
    top: 0;
    right: 25px;
    font-size: 36px;
    margin-left: 50px;
  }
}
@media(max-width:360px){
  .siteLogo img{
    width: 80px;
  }
  ul.ctaBtns li:first-child a, ul.ctaBtns li:first-child a{
    font-size: 12px;
  }
  ul.ctaBtns li{
    margin-left: 7px;
  }
}
</style>
<section class="navBarSect">
  <div class="af-overlay" onclick="closeNav()"></div>
  <div class="navbar">
    <div class="siteLogo">
      <a href="<?=$path?>"><img src="images/essayHoursLogo.svg" alt="Essay Hours Logo"> </a>
    </div>
    <div>
      <ul class="navItems">
        <li><a href="#howItWorks">How to Order</a> </li>
        <!-- <li><a href="<?=$path?>">Writers</a> </li> -->
        <!-- <li><a href="<?=$path?>">Samples</a> </li> -->
        <!-- <li><a href="<?=$path?>">Pricing</a> </li> -->
        <!-- <li><a href="<?=$path?>">FAQs</a> </li> -->
        <!-- <li><a href="<?=$path?>">Why Us</a> </li> -->
        <!-- <li><a href="<?=$path?>">Reviews</a> </li> -->
        <!-- <li><a href="<?=$path?>">Blogs</a> </li> -->
      </ul>
    </div>
    <div >
      <ul class="ctaBtns">
        <li><a href="<?=$path?>">Register</a> </li>
        <li><a href="<?=$path?>order">Order Now</a> </li>
      </ul>
    </div>
  </div>
  <div class="mobileNav">
    <div class="siteLogo">
      <a href="<?=$path?>"><img src="images/essayHoursLogo.svg" alt="Essay Hours Logo"> </a>
    </div>
    <div >
      <ul class="ctaBtns">
        <li><a href="<?=$path?>">Register</a> </li>
        <li><a href="<?=$path?>order">Order</a> </li>
      </ul>
    </div>
    <div id="mySidenav" class="sidenav">
      <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
      <ul>
      <li><a href="#howItWorks">How to Order</a> </li>
        <!-- <li><a href="<?=$path?>">Writers</a> </li>
        <li><a href="<?=$path?>">Samples</a> </li>
        <li><a href="<?=$path?>">Pricing</a> </li>
        <li><a href="<?=$path?>">FAQs</a> </li>
        <li><a href="<?=$path?>">Why Us</a> </li>
        <li><a href="<?=$path?>">Reviews</a> </li>
        <li><a href="<?=$path?>">Blogs</a> </li> -->
      </ul>
    </div>
    <span style="cursor:pointer" onclick="openNav()" class="bar1"></span>
  </div>
</section>
