<style media="screen">
*,body{
  box-sizing: border-box;
}
.container{
  max-width: 1170px;
  width: 100%;
  margin: 0 auto;
  padding: 0 15px;
}
section.makePayment {
  background-color: #E5E5E5;
  background-image: url('images/provide-details-bg.svg');
  background-repeat: no-repeat;
  background-position-x: 97%;
  background-position-y: 10%;
  min-height: 100vh;
  padding: 50px 0;
}
.orderWraper {
  width: 100%;
  display: flex;
  align-items: center;
}
.orderForm {
  max-width: 751px;
  width: 100%;
  background-color: #fff;
  padding: 30px 96px 40px 30px;
  position: relative;
  box-shadow: -11px 15px 60px rgb(183 183 183 / 25%);
}
p.form-head {
  background-color: #249DEC;
  padding: 10px;
  width: calc(100% + 35px );
  position: absolute;
  left: -17px;
  top: 30px;
  font-size: 39px;
  color: #fff;
  padding-left: 40px;
  font-family: 'Poppins';
}
p.form-head::before {
  position: absolute;
  content: '';
  width: 0;
  height: 0;
  border-top: 18px solid #9D9D9D;
  border-left: 18px solid transparent;
  left: 0;
  bottom: -18px;
}
p.form-head::after {
  position: absolute;
  content: '';
  width: 0;
  height: 0;
  border-top: 18px solid #9D9D9D;
  border-right: 18px solid transparent;
  right: 0;
  bottom: -18px;
}
form.sh-form {
  padding-top: 115px;
}
.paymentFields {
  display: flex;
  width: 100%;
  margin-bottom: 20px;
}
.paymentFields label {
  max-width: 180px;
  width: 100%;
  font-size: 18px;
  line-height: 21px;
  color: #35414B;
  font-family: 'GT-Walsheim-Regular';
}
.Payment .orderForm {
  padding: 30px 70px;
}
.instructions a {
  color: #35414B;
}
.priceBox {
  background: #FFFFFF;
  border: 2px solid #249DEC;
  box-sizing: border-box;
  box-shadow: -11px 15px 60px rgba(183, 183, 183, 0.25);
  max-width: 365px;
  width: 100%;
  text-align: center;
  margin-left: -10px;
  padding: 20px 0 25px;
  margin-top: 100px;
}
p.price {
  font-size: 28px;
  line-height: 32px;
  color: #35414B;
  font-family: 'GT-Walsheim-Regular';
  margin: 0;
}
.priceBox span {
  font-size: 17px;
  line-height: 19px;
  font-family: 'GT-Walsheim-Regular';
}
span.was {
  padding-right: 35px;
}
p.save {
  font-size: 28px;
  line-height: 32px;
  text-align: center;
  color: #1BA260;
  font-family: 'GT-Walsheim-Regular';
  padding-top: 20px;
}
p.offerEnds {
  font-size: 17px;
  line-height: 19px;
  text-align: center;
  color: #FF2A13;
  font-family: 'GT-Walsheim-Regular';
}
.sidecheckbox {
  display: flex;
  align-items: center;
  justify-content: center;
  margin-top: 22px;
  margin-bottom: 10px;
}
.sidecheckbox p {
  padding-left: 5px;
}
p.innerP {
  font-size: 17px;
  line-height: 19px;
  text-align: center;
  color: #35414B;
  font-family: 'GT-Walsheim-Regular';
}
p.totalPrice {
  margin-top: 25px;
  padding-top: 20px;
  border-top: 2px dashed #D5D2DC;
  color: #249DEC;
  font-family: 'GT-Walsheim-Regular';
  font-size: 32px;
  line-height: 37px;
}
p.agree {
  font-size: 14px;
  line-height: 16px;
  text-align: center;
  color: #35414B;
  font-family: 'GT-Walsheim-Regular';
  margin-top: 30px;
  padding: 0 20px;
}
.acceptMethods {
  width: 100%;
  float: left;
  margin-top: 15px;
}
.agree a {
  color: #249DEC;
  text-decoration: none;
}
.formButton a {
  border: 3px solid #FFCE4B;
  box-sizing: border-box;
  box-shadow: 0px 2px 4px rgba(0, 0, 0, 0.25);
  border-radius: 8px;
  background-color: #FFCE4B;
  color: #35414B;
  text-decoration: none;
  padding: 11px 40px;
  font-family: 'GT-Walsheim-Regular';
}
.formButton a:hover {
  border: 3px solid #FFCE4B;
  background-color: #fff;
}
.detailsPanel {
  display: flex;
  justify-content: center;
  margin-top: 20px;
}
.Payment .priceBox {
  margin-top: 120px;
}
.paymentFields p {
  font-size: 18px;
  line-height: 21px;
  color: #35414B;
  font-family: 'GT-Walsheim-Regular';
}
p.para-1 {
  font-size: 16px;
  line-height: 18px;
  color: #35414B;
  font-family: 'GT-Walsheim-Regular';
  padding-bottom: 25px;
}
.editOrder a {
  font-size: 11px;
  line-height: 13px;
  color: #35414B;
  font-family: 'GT-Walsheim-Regular';
  display: flex;
  align-items:center;
  padding-top: 5px;
}
.editOrder svg {
  margin-right: 5px;
}

@media (max-width:1200px) {
  .orderForm {
    padding: 30px 30px 40px 30px;
  }
}
@media (max-width:992px) {
  .container{
    max-width: 960px;
  }
  .orderForm {
    max-width: 710px;
  }
  .orderWraper {
    flex-direction: column;
  }
  .priceBox {
    margin-top: -10px;
  }
  .provideDetails .priceBox {
    margin-top: -10px;
  }
  p.form-head {
    width: calc(100% + 35px );
    left: -17px;
  }
  p.form-head::before {
    left: 1px;
    bottom: -18px;
  }
  .priceBox {
    max-width: 450px;
  }
  .Payment .priceBox {
    margin-top: -10px;
  }
}
@media (max-width:768px) {
  .container{
    max-width: 720px;
  }
  .paymentFields p {
    font-size: 16px;
  }
  .paymentFields label {
    font-size: 16px;
  }
}
@media (max-width:576px) {
  .container{
    max-width: 540px;
  }
  p.form-head {
    font-size: 30px;
  }
  .Payment .orderForm {
    padding: 30px;
  }
  form.sh-form {
    padding-top: 100px;
  }
  .paymentFields {
    flex-direction: column;
  }
  .paymentFields label {
    font-size: 20px;
    padding-bottom: 10px;
  }
  .paymentFields p {
    font-size: 19px;
  }
  .editOrder a {
    font-size: 15px;
  }
}
@media (max-width:480px) {
  .priceBox {
    max-width: 270px;
  }
}
</style>
</head>
<body>
  <section class="makePayment">
    <div class="container">
      <div class="orderWraper Payment">
        <div class="orderForm">
          <p class="form-head">Provide Details</p>
          <form class="sh-form">
            <p class="para-1">Your writer will start working on your order immediately after you make payment. All payments are securely processed via 2checkout. We accept all major cards.</p>
            <div class="paymentFields">
              <label>Name</label>
              <p>Ali</p>
            </div>
            <div class="paymentFields">
              <label>Email :</label>
              <p>ali.b.arshaddd@gmail.com</p>
            </div>
            <div class="paymentFields">
              <label>Phone no. :</label>
              <p>123456789</p>
            </div>
            <div class="paymentFields">
              <label>Document type :</label>
              <p>Admission Essay</p>
            </div>
            <div class="paymentFields">
              <label>Academic Level :</label>
              <p>College - Undergraduate</p>
            </div>
            <div class="paymentFields">
              <label>Due date & time :</label>
              <p>2021 - 07 - 16 04 : 00 </p>
            </div>
            <div class="paymentFields">
              <label>No. of pages :</label>
              <p>2 pages ~ 600 words</p>
            </div>
            <div class="paymentFields">
              <label>Subject :</label>
              <p>Biology and Life Sciences </p>
            </div>
            <div class="paymentFields">
              <label>Title :</label>
              <p>Hello World </p>
            </div>
            <div class="paymentFields">
              <label>Citation style :</label>
              <p>AMA </p>
            </div>
            <div class="paymentFields">
              <label>No. of Sources :</label>
              <p>3</p>
            </div>
            <div class="paymentFields">
              <label>Instructions :</label>
              <p>Surprise me </p>
            </div>
            <div class="paymentFields instructions">
              <label>Attachments :</label>
              <p><a href="javascript:;">Instructions.pdf</a> </p>
            </div>
            <p class="editOrder"><a href="#javascript:;"><svg width="15" height="15" viewBox="0 0 15 15" fill="none" xmlns="http://www.w3.org/2000/svg">
              <path d="M13.8444 9.41338C13.6381 9.41338 13.4708 9.58147 13.4708 9.78885V13.1226C13.4701 13.7445 12.9688 14.2484 12.3501 14.249H1.86789C1.24915 14.2484 0.747884 13.7445 0.747155 13.1226V3.33828C0.747884 2.71656 1.24915 2.21261 1.86789 2.21187H5.18484C5.39119 2.21187 5.55842 2.04379 5.55842 1.83641C5.55842 1.62916 5.39119 1.46094 5.18484 1.46094H1.86789C0.836755 1.46211 0.00116743 2.30193 0 3.33828V13.1227C0.00116743 14.1591 0.836755 14.9989 1.86789 15.0001H12.3501C13.3812 14.9989 14.2168 14.1591 14.218 13.1227V9.78885C14.218 9.58147 14.0507 9.41338 13.8444 9.41338Z" fill="#35414B"/>
              <path d="M14.0693 0.494892C13.4128 -0.164964 12.3484 -0.164964 11.6918 0.494892L5.02697 7.19348C4.98129 7.23939 4.94831 7.29629 4.9311 7.35877L4.05465 10.539C4.0186 10.6693 4.05523 10.809 4.15038 10.9047C4.24567 11.0004 4.38459 11.0372 4.51432 11.0011L7.67849 10.1201C7.74066 10.1028 7.79728 10.0696 7.84295 10.0237L14.5077 3.32498C15.1632 2.66469 15.1632 1.59578 14.5077 0.935481L14.0693 0.494892ZM5.84096 7.43753L11.2956 1.95511L13.0548 3.72319L7.59998 9.20561L5.84096 7.43753ZM5.48956 8.14623L6.895 9.55893L4.95094 10.1003L5.48956 8.14623ZM13.9794 2.79405L13.5832 3.19225L11.8239 1.42403L12.2202 1.02583C12.5849 0.659306 13.1762 0.659306 13.5409 1.02583L13.9794 1.46642C14.3435 1.83338 14.3435 2.42723 13.9794 2.79405Z" fill="#35414B"/>
            </svg>
            EDIT ORDER DETAILS</a> </p>
          </form>
        </div>
        <div class="priceBox">
          <p class="price">Your Price: $50</p>
          <p>
            <span class="was">Was: $50</span>
            <span>Now: $25</span>
          </p>
          <p class="save">You Save: $25</p>
          <p class="offerEnds">Offer ending in 12 : 17 :06</p>
          <div class="sidecheckbox">
            <input type="checkbox">
            <p>Prepay 50%</p>
          </div>
          <p class="innerP">Pay now only: $12.5</p>
          <p class="innerP">Pay on delivery: $12.5</p>
          <p class="totalPrice">Total Price: $25</p>
          <div class="formButton detailsPanel">
            <p><a href="javascript:;">Review order details</a> </p>
          </div>
          <p class="agree">I agree with <a href="#javascriptL:;">policies</a> and <a href="#javascript:;">terms and conditions</a> by clicking button above.</p>
          <div class="acceptMethods">
            <img src="images/paymet-methods.svg" alt="Payment Methods">
          </div>
        </div>
      </div>
    </div>
  </section>
