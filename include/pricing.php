<style media="screen">
*,body{
  box-sizing: border-box;
}
.container{
  max-width: 1170px;
  width: 100%;
  margin: 0 auto;
  padding: 0 15px;
}
section.pricingPage {
  padding: 50px 0;
}
.pricingWraper {
  width: 100%;
  display: flex;
  justify-content: space-between;
}
.pricingContent {
  max-width: 750px;
  width: 100%;
  float: left;
}
.pricingContent h1 {
  line-height: 46px;
  color: #35414B;
  font-size: 40px;
  font-family: 'GT-Walsheim-Regular';
  padding-bottom: 30px;
}
.pricingContent p{
  font-size: 16px;
  line-height: 18px;
  color: #35414B;
  padding-bottom: 40px;
  font-family: 'GT-Walsheim-Regular';
}
.pricingContent  a {
  color: #249dec;
  text-decoration: none;
}
.factorsprites{
  background-image: url('images/pricing-sprites.png');
}
.factor1 {
  width: 26px;
  height: 30px;
  background-position: -10px -59px;
}
.factor2 {
  width: 27px;
  height: 39px;
  background-position: -10px -10px;
}
.factor3 {
  width: 27px;
  height: 29px;
  background-position: -57px -10px;
}
.factors-wraper {
  width: 100%;
  display: flex;
  align-items: center;
  margin-bottom: 10px;
}
.factors-wraper p {
  padding-bottom: 0;
  padding-left: 20px;
}
.pricingRange {
  width: 100%;
  margin-top: 60px;
}
.pricingRange ul {
  width: 100%;
  display: flex;
  list-style: none;
  justify-content: space-between;
  padding-bottom: 25px;
}
.ranges li {
  font-size: 24px;
  line-height: 27px;
  color: #35414B;
  font-family: 'GT-Walsheim-Regular';
}
.pricingRange input[type=range]{
    -webkit-appearance: none !important;
    max-width: 738px;
    width: 100%;
}
.pricingRange input[type="range"]:focus {
    outline: none !important;
}
.pricingRange input[type="range"]::-webkit-slider-runnable-track{
    width: 100%;
    height: 12px;
    cursor: pointer;
    border-radius: 20px;
    border: 1px solid rgba(61, 66, 74, 0.1);
}
.pricingRange input[type="range"]::-webkit-slider-thumb{
    border: 5px solid rgba(36, 157, 236, 1);
    height: 30px;
    width: 30px;
    border-radius: 50%;
    background: #fff;
    cursor: pointer;
    -webkit-appearance: none;
    margin-top: -13.5px;
}
.academicLevel {
  width: 100%;
  margin: 50px 0;
  display: flex;
  justify-content: space-between;
}
.academicBox {
  box-shadow: 0px 2px 14px rgb(124 124 124 / 50%);
  border-radius: 12px;
  padding: 30px 15px 30px 20px;
  max-width: 170px;
  width: 100%;
}
p.level {
  color: #249DEC;
  font-size: 16px;
  line-height: 18px;
  padding-bottom: 20px;
  font-family: 'GT-Walsheim-Regular';
}
p.levelPrice {
  font-size: 22px;
  line-height: 25px;
  font-family: 'GT-Walsheim-Regular';
  color: #35414B;
  padding-bottom: 0;
  text-align: justify;
}
.levelPrice small{
  font-size: 14px;
  line-height: 16px;
}
.pricingContent h2 {
  font-size: 22px;
  line-height: 25px;
  color: #249DEC;
  padding-bottom: 30px;
  font-family: 'GT-Walsheim-Regular';
}
.promisesInner {
  width: 100%;
  display: flex;
  flex-wrap: wrap;
  justify-content: space-between;
}
.promises {
  width: 50%;
  margin-bottom: 20px;
  display: flex;
  align-items: center;
}
.promises label {
  padding-left: 30px;
  position: relative;
  font-size: 14px;
  line-height: 18px;
  text-align: justify;
  color: #35414B;
  font-family: 'GT-Walsheim-Regular';
}
.promises label::before {
  position: absolute;
  content: '';
  width: 20px;
  height: 20px;
  background-image: url('images/promise-check.svg');
  top: 0;
  left: 0px;
  background-repeat: no-repeat;
}
/* SideBar CSS */
.sideBar {
  max-width: 230px;
  width: 100%;
  margin-left: auto;
}
.benefitsWrap{
  filter: drop-shadow(0px 2px 14px rgba(216, 217, 255, 0.668003));
  margin-bottom: 50px;
}
.benefitsInner{
  background: #Fff;
  color: #222;
  clip-path: polygon(50% 0%, 100% 21%, 100% 100%, 0 100%, 0 21%);
  width: 100%;
  min-height: 400px;
  padding-top: 50px;
}
.benefitsInner h3 {
  font-size: 33px;
  line-height: 38px;
  color: #35414B;
  font-family: 'GT-Walsheim-Regular';
  text-align: center;
}
.benefitsInner ul {
  padding-top: 10px;
  list-style: none;
  padding-left: 15px;
}
.benefitsInner ul li{
  padding-bottom: 7px;
  font-size: 15px;
  line-height: 22px;
  text-align: justify;
  font-family: 'GT-Walsheim-Regular';
}
.benefitsInner ul li span{
  color: #249DEC;
}
.valueBox {
  width: 100%;
  min-height: 230px;
  background: #FFFFFF;
  box-shadow: 0px 2px 14px rgba(216, 217, 255, 0.668003);
  border-radius: 8px;
  margin-bottom: 50px;
  border: 3px solid #249DEC;
}
.valueInner {
  padding: 55px 0 0 0;
  text-align: center;
  position: relative;
}
.valueInner::before {
  position: absolute;
  content: '';
  width: 90px;
  height: 70px;
  background: #EBF5FF;
  border-radius: 0 0 150px 0;
  top: 0;
  left: 0;
}
.valueInner small {
  font-size: 19px;
  line-height: 22px;
  text-align: center;
  letter-spacing: 0.01em;
  color: #35414B;
  font-family: 'GT-Walsheim-Regular';
  position: relative;
}
.valueInner  .words {
  font-size: 34px;
  line-height: 39px;
  text-align: center;
  color: #35414B;
  font-family: 'GT-Walsheim-Regular';
  font-weight: 600;
}
.valueInner .discount {
  font-size: 22px;
  line-height: 27px;
  text-align: center;
  letter-spacing: 0.01em;
  color: #35414B;
  font-family: 'GT-Walsheim-Regular';
  padding-top: 10px;
  border-top: 2px dashed #D5D2DC;
  margin-top: 30px;
  position: relative;
}
.papersWrap{
  filter: drop-shadow(0px 2px 14px rgba(216, 217, 255, 0.668003));
  margin-bottom: 50px;
}
.papersInner{
  background: #Fff;
  color: #222;
  clip-path: polygon(100% 0, 100% 75%, 50% 100%, 0 75%, 0 0);
  width: 100%;
  min-height: 400px;
  padding-top: 70px;
  text-align: center;
}
p.paperMatters {
  font-size: 33px;
  line-height: 38px;
  text-align: center;
  color: #35414B;
  font-family: 'GT-Walsheim-Regular';
}
p.goodWords {
  font-size: 33px;
  line-height: 38px;
  text-align: center;
  color: #35414B;
  font-family: 'GT-Walsheim-Regular';
  font-weight: 600;
}
p.panelOrder {
  margin-top: 20px;
}
.papersInner a {
  background: #249DEC;
  border: 3px solid #249DEC;
  box-sizing: border-box;
  border-radius: 30px;
  color: #fff;
  text-decoration: none;
  padding: 10px;
  display: block;
  max-width: 160px;
  margin: 0 auto;
  width: 100%;
}
.roundShape::before {
  position: absolute;
  content: '';
  width: 32px;
  height: 32px;
  background: #fff;
  border-radius: 100%;
  top: -20px;
  left: -20px;
  border: 3px solid #249DEC;
}
.roundShape::after {
  content: "";
  position: absolute;
  content: '';
  width: 18px;
  height: 38px;
  background: #fff;
  top: -20px;
  left: -21px;
}
.roundShape.right::before {
  top: -20px;
  right: -20px;
  left: auto;
}
.roundShape.right::after {
  top: -20px;
  right: -21px;
  left: auto;
}
@media (max-width:1200px) {
  .container{
    max-width: 920px;
  }
  .pricingContent {
    max-width: 680px;
    padding-right: 50px;
  }
  .academicBox {
    max-width: 150px;
  }
  p.levelPrice {
    font-size: 20px;
    text-align: left;
  }
  .academicBox {
    padding: 30px 15px 30px 10px;
  }
  .ranges li {
    font-size: 20px;
  }
  .promises {
    width: 100%;
  }
  .academicLevel {
    flex-direction: column;
    align-items: center;
  }
  .academicBox {
    margin-bottom: 30px;
  }
  .academicBox {
    max-width: 250px;
    text-align: center;
  }
  p.levelPrice {
    text-align: center;
  }
  .ranges li {
    font-size: 16px;
  }
  .benefitsInner ul li {
    font-size: 13px;
  }
  .sideBar {
    max-width: 210px;
  }
  p.goodWords {
    font-size: 30px;
  }
  p.paperMatters {
    font-size: 25px;
  }
}
@media (max-width:991px) {
  .container{
    max-width: 820px;
  }
  .pricingContent {
    padding-right: 50px;
  }
}
@media (max-width:767px) {
  .container{
    max-width: 720px;
  }
  .pricingContent {
    max-width: unset;
    width: 100%;
  }
  .sideBar {
    display: none;
  }
  .pricingContent {
    padding-right: 0;
  }
}
@media (max-width:575px) {
  .container{
    max-width: 540px;
  }
}
</style>
<body>
  <section class="pricingPage">
    <div class="container">
      <div class="pricingWraper">
        <div class="pricingContent">
          <h1>Value Pricing</h1>
          <p>When you’re on a tight budget and your grades don't matter, then offshore writers are the way to go. </p>
          <p>But if it's all about getting good results it's worth the investment in a higher quality service like EssayHours.com.</p>
          <p>Our pricing is based on our guiding principles. We will not only deliver an excellent paper on time but also follow all of your instructions exactly as requested.</p>
          <p>The following three factors influence our pricing on your assignment. </p>
              <ul>
                <li>Deadline</li>
                <li>The number of pages required</li>
                <li>Academic level </li>
              </ul>
          <div class="pricingTable">
            <img src="<?=$path?>images/pricing-table.png" alt="Pricing Table">
          </div>
        <p>You'll find our writing to be of high quality, and delivered at an affordable rate. </p>
        <p>Our standard academic projects are 300 words per page with 12pt Times Roman font; however other formats may vary slightly in price.</p>

          <div class="factors-wraper">
            <div class="factorsprites factor1"></div>
            <p>Your deadline. We guarantee to meet it or beat it.</p>
          </div>
          <div class="factors-wraper">
            <div class="factorsprites factor2"></div>
            <p>The minimum number of pages required. We never come up short.</p>
          </div>
          <div class="factors-wraper">
            <div class="factorsprites factor3"></div>
            <p>Your academic level. We don’t “under-write” or “over-write.”</p>
          </div>
          <div class="pricingRange">
            <ul class="ranges">
              <li>8+ Days</li>
              <li>6-7 Days</li>
              <li>4-5 Days</li>
              <li>3 Days</li>
              <li>2 Days</li>
              <li>24 Hours</li>
              <li>12 Hours</li>
            </ul>
            <input id="myRange" type="range" min="0" max="350000" value="0" oninput="sliderColors()">
          </div>
          <div class="academicLevel">
            <div class="academicBox">
              <p class="level">High School</p>
              <p class="levelPrice">$15.50/<small>per page 1 page ~ 300 words.</small></p>
            </div>
            <div class="academicBox">
              <p class="level">College/University</p>
              <p class="levelPrice">$18.50/<small>per page 1 page ~ 300 words.</small></p>
            </div>
            <div class="academicBox">
              <p class="level">Masters/MBA</p>
              <p class="levelPrice">$21.50/<small>per page 1 page ~ 300 words.</small></p>
            </div>
            <div class="academicBox">
              <p class="level">PHD</p>
              <p class="levelPrice">$33.00/<small>per page 1 page ~ 300 words.</small></p>
            </div>
          </div>
          <p>For standard academic projects, we deliver approximately 300 words per page, double-spaced, Times Roman 12pt font.</p>
          <p>Other formats (scripts, speeches, PowerPoint, mathematical calculations), per page charges may vary slightly.</p>
          <h2>Our Promise to you:</h2>
          <div class="promisesInner">
            <div class="promises">
              <label>No hidden charges or any “upsell”</label>
            </div>
            <div class="promises">
              <label>100% originality (plagiarism screening provided)</label>
            </div>
            <div class="promises">
              <label>Flexible rates depending upon deadline</label>
            </div>
            <div class="promises">
              <label>You own the copyright upon final payment</label>
            </div>
            <div class="promises">
              <label>100% confidentiality</label>
            </div>
            <div class="promises">
              <label>Direct communication with your writer</label>
            </div>
            <div class="promises">
              <label>100% satisfaction guaranteed</label>
            </div>
            <div class="promises">
              <label>Your project delivered to your account, ready to submit</label>
            </div>
          </div>
        </div>
        <div class="sideBar">
          <div class="sidebarWraper">
            <div class="benefitsWrap">
              <div class="benefitsInner">
                <h3>Benefits</h3>
                <ul>
                  <li><span>Free</span> Proofreading</li>
                  <li><span>Free</span> Unlimited Revisions</li>
                  <li><span>Free</span> Formatting</li>
                  <li><span>Free</span> Title Page</li>
                  <li><span>Free</span> Bibliography</li>
                  <li><span>US Qualified</span> Writers</li>
                  <li><span>No Plagiarism</span> Guaranteed</li>
                  <li><span>Direct contact</span> with Writer</li>
                  <li><span>100%</span> Secure & Confidential</li>
                  <li><span>Deadline</span> Driven</li>
                </ul>
              </div>
            </div>
            <div class="valueBox">
              <div class="valueInner">
                <small>Our average page is</small>
                <p class="words">300 WORDS</p>

                <p class="discount">so you get upto 20% more value
                  <span class="roundShape left"></span>
                  <span class="roundShape right"></span>
                </p>
              </div>
            </div>
            <div class="papersWrap">
              <div class="papersInner">
                <p class="paperMatters">If your paper matters,</p>
                <p class="goodWords">we own all the good words.</p>
                <p class="panelOrder"><a href="#javascript:;">Order Now</a> </p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
