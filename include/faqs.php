<style media="screen">
*,body{
  box-sizing: border-box;
}
.container{
  max-width: 1170px;
  width: 100%;
  margin: 0 auto;
  padding: 0 15px;
}
section.faqsPage {
  padding: 50px 0;
}
.faqsWraper {
  width: 100%;
  display: flex;
  justify-content: space-between;
}
.faqsContent {
  max-width: 750px;
  width: 100%;
  float: left;
}
h1 {
  line-height: 46px;
  color: #35414B;
  font-size: 40px;
  padding-bottom: 40px;
}
.faqsContent p{
  font-size: 16px;
  line-height: 18px;
  color: #35414B;
  font-family: 'GT-Walsheim-Regular';
  border-left: 2px solid #249DEC;
  padding-left: 20px;
}
.faqsContent  a {
  color: #249dec;
  text-decoration: none;
}
.faqsContent h2 {
  font-size: 22px;
  line-height: 25px;
  color: #249DEC;
  padding-bottom: 30px;
  font-family: 'GT-Walsheim-Regular';
}
.accordionWrapper{
  background:#fff;float:left; max-width: 915px;width:100%;box-sizing:border-box;
}
.accordionItem{
  float:left;
  display:block;
  width:100%;
  box-sizing: border-box;
  font-family:'Open-sans',Arial,sans-serif;
}
.accordionItemHeading{
  cursor: pointer;
  margin: 0px 0px 22px 0px;
  padding: 25px 60px;
  background: #fff;
  color: #2B3543;
  width: 100%;
  border-bottom: 2px solid #EBF5FF;
  box-sizing: border-box;
  position: relative;
  font-size: 18px;
  font-weight: 400;
}
.close .accordionItemContent{
  height:0px;
  transition:height 1s ease-out;
  -webkit-transform: scaleY(0);
  -o-transform: scaleY(0);
  -ms-transform: scaleY(0);
  transform: scaleY(0);
  float:left;
  display:block;
}
.open .accordionItemHeading::before{
  background-image: url('images/blue-minus.svg');
}
.open.accordionItem{
  border-bottom: 2px solid #EBF5FF;
}
.open .accordionItemContent{
  padding: 20px;
  background-color: #fff;
  width: 100%;
  margin: 0px 0px 10px 0px;
  display:block;
  -webkit-transform: scaleY(1);
  -o-transform: scaleY(1);
  -ms-transform: scaleY(1);
  transform: scaleY(1);
  max-width: 590px;
  margin: 0 auto;
  width: 100%;
  text-align: justify;
  -webkit-transition: -webkit-transform 0.4s ease-out;
  transition: transform 0.4s ease;
  box-sizing: border-box;
}

.open .accordionItemHeading{
  margin:0px;
  border-bottom-right-radius: 0px;
  border-bottom-left-radius: 0px;
  border-bottom: none;
}

h4.accordionItemHeading::before {
  content: '';
  position: absolute;
  background-image: url('images/blue-plus.svg');
  top: 28px;
  left: 13px;
  background-repeat: no-repeat;
  pointer-events: none;
  width: 16px;
  height: 16px;
}

/* SideBar CSS */
.sideBar {
  max-width: 230px;
  width: 100%;
  margin-left: auto;
}
.benefitsWrap{
  filter: drop-shadow(0px 2px 14px rgba(216, 217, 255, 0.668003));
  margin-bottom: 50px;
}
.benefitsInner{
  background: #Fff;
  color: #222;
  clip-path: polygon(50% 0%, 100% 21%, 100% 100%, 0 100%, 0 21%);
  width: 100%;
  min-height: 400px;
  padding-top: 50px;
}
.benefitsInner h3 {
  font-size: 33px;
  line-height: 38px;
  color: #35414B;
  font-family: 'GT-Walsheim-Regular';
  text-align: center;
}
.benefitsInner ul {
  padding-top: 10px;
  list-style: none;
  padding-left: 15px;
}
.benefitsInner ul li{
  padding-bottom: 7px;
  font-size: 15px;
  line-height: 22px;
  text-align: justify;
  font-family: 'GT-Walsheim-Regular';
}
.benefitsInner ul li span{
  color: #249DEC;
}
.valueBox {
  width: 100%;
  min-height: 230px;
  background: #FFFFFF;
  box-shadow: 0px 2px 14px rgba(216, 217, 255, 0.668003);
  border-radius: 8px;
  margin-bottom: 50px;
  border: 3px solid #249DEC;
}
.valueInner {
  padding: 55px 0 0 0;
  text-align: center;
  position: relative;
}
.valueInner::before {
  position: absolute;
  content: '';
  width: 90px;
  height: 70px;
  background: #EBF5FF;
  border-radius: 0 0 150px 0;
  top: 0;
  left: 0;
}
small {
  font-size: 19px;
  line-height: 22px;
  text-align: center;
  letter-spacing: 0.01em;
  color: #35414B;
  font-family: 'GT-Walsheim-Regular';
  position: relative;
}
.valueInner  .words {
  font-size: 34px;
  line-height: 39px;
  text-align: center;
  color: #35414B;
  font-family: 'GT-Walsheim-Regular';
  font-weight: 600;
}
.valueInner .discount {
  font-size: 22px;
  line-height: 27px;
  text-align: center;
  letter-spacing: 0.01em;
  color: #35414B;
  font-family: 'GT-Walsheim-Regular';
  padding-top: 10px;
  border-top: 2px dashed #D5D2DC;
  margin-top: 30px;
  position: relative;
}
.papersWrap{
  filter: drop-shadow(0px 2px 14px rgba(216, 217, 255, 0.668003));
  margin-bottom: 50px;
}
.papersInner{
  background: #Fff;
  color: #222;
  clip-path: polygon(100% 0, 100% 75%, 50% 100%, 0 75%, 0 0);
  width: 100%;
  min-height: 400px;
  padding-top: 70px;
  text-align: center;
}
p.paperMatters {
  font-size: 33px;
  line-height: 38px;
  text-align: center;
  color: #35414B;
  font-family: 'GT-Walsheim-Regular';
}
p.goodWords {
  font-size: 33px;
  line-height: 38px;
  text-align: center;
  color: #35414B;
  font-family: 'GT-Walsheim-Regular';
  font-weight: 600;
}
p.panelOrder {
  margin-top: 20px;
}
.papersInner a {
  background: #249DEC;
  border: 3px solid #249DEC;
  box-sizing: border-box;
  border-radius: 30px;
  color: #fff;
  text-decoration: none;
  padding: 10px;
  display: block;
  max-width: 160px;
  margin: 0 auto;
  width: 100%;
}
.roundShape::before {
  position: absolute;
  content: '';
  width: 32px;
  min-height: 32px;
  background: #fff;
  border-radius: 100%;
  top: -20px;
  left: -20px;
  border: 3px solid #249DEC;
}
.roundShape::after {
  content: "";
  position: absolute;
  content: '';
  width: 18px;
  height: 38px;
  background: #fff;
  top: -20px;
  left: -21px;
}
.roundShape.right::before {
  top: -20px;
  right: -20px;
  left: auto;
}
.roundShape.right::after {
  top: -20px;
  right: -21px;
  left: auto;
}
/* End of SideBar CSS */
@media (max-width:1200px) {
  .faqsContent {
    max-width: 580px;
    padding-right: 10px;
  }
  .benefitsInner ul li {
    font-size: 13px;
  }
  .sideBar {
    max-width: 210px;
  }
  .container{
    max-width: 920px;
  }
}
@media (max-width:992px) {
  .container{
    max-width: 820px;
  }
}
@media (max-width:768px) {
  .container{
    max-width: 720px;
  }
  .faqsContent {
    max-width: unset;
    width: 100%;
  }
  .sideBar {
    display: none;
  }
  .accordionWrapper{
    padding-top: 0px;
  }
}
@media (max-width:576px) {
  .container{
    max-width: 540px;
  }
  .accordionItemHeading{
    font-size: 17px;
    padding: 25px 38px;
    padding-right: 0px;
  }
  .faqsContent h1 {
    line-height: 25px;
    font-size: 25px;
  }

}
</style>
<body>
  <section class="faqsPage">
    <div class="container">
      <div class="faqsWraper">
        <div class="faqsContent">
          <h1>Frequently asked questions</h1>
          <div class="wrapper">
            <div class="accordionWrapper">
              <div class="accordionItem close">
                <h4 class="accordionItemHeading">There are a lot of cheap writing services on the internet. How do I know EssayHours is for real?</h4>
                <div class="accordionItemContent">
                  <p>Because we’re simply better. Our competitors employ off-shore “writers” for whom English is a 2nd or 3rd language, and who will work for less than a subsistence wage. Much of what other websites deliver comes from “paper mill” digital content that is plagiarized from random websites. You may receive a completed document, but handing it in to your professor will likely create more problems than you have ever imagined. Our writers are professionals with serious academic backgrounds. We value their skills and their commitment to our clients’ success, and we compensate them accordingly. As in much of life, you get what you pay for; paying what appears to be a “bargain” rate is always more expensive in the long run.</p>
                </div>
              </div>

              <div class="accordionItem close">
                <h4 class="accordionItemHeading">Will i get an “A” on my assignmnet?</h4>
                <div class="accordionItemContent">
                  <p>Because we’re simply better. Our competitors employ off-shore “writers” for whom English is a 2nd or 3rd language, and who will work for less than a subsistence wage. Much of what other websites deliver comes from “paper mill” digital content that is plagiarized from random websites. You may receive a completed document, but handing it in to your professor will likely create more problems than you have ever imagined. Our writers are professionals with serious academic backgrounds. We value their skills and their commitment to our clients’ success, and we compensate them accordingly. As in much of life, you get what you pay for; paying what appears to be a “bargain” rate is always more expensive in the long run.</p>
                </div>
              </div>

              <div class="accordionItem close">
                <h4 class="accordionItemHeading">Where are your writers located?</h4>
                <div class="accordionItemContent">
                  <p>Because we’re simply better. Our competitors employ off-shore “writers” for whom English is a 2nd or 3rd language, and who will work for less than a subsistence wage. Much of what other websites deliver comes from “paper mill” digital content that is plagiarized from random websites. You may receive a completed document, but handing it in to your professor will likely create more problems than you have ever imagined. Our writers are professionals with serious academic backgrounds. We value their skills and their commitment to our clients’ success, and we compensate them accordingly. As in much of life, you get what you pay for; paying what appears to be a “bargain” rate is always more expensive in the long run.</p>
                </div>
              </div>
              <div class="accordionItem close">
                <h4 class="accordionItemHeading">Your charge more than a lot of other websites. Why?</h4>
                <div class="accordionItemContent">
                  <p>Because we’re simply better. Our competitors employ off-shore “writers” for whom English is a 2nd or 3rd language, and who will work for less than a subsistence wage. Much of what other websites deliver comes from “paper mill” digital content that is plagiarized from random websites. You may receive a completed document, but handing it in to your professor will likely create more problems than you have ever imagined. Our writers are professionals with serious academic backgrounds. We value their skills and their commitment to our clients’ success, and we compensate them accordingly. As in much of life, you get what you pay for; paying what appears to be a “bargain” rate is always more expensive in the long run.</p>
                </div>
              </div>
              <div class="accordionItem close">
                <h4 class="accordionItemHeading">What if someone finds out i am not writing my paper myself?</h4>
                <div class="accordionItemContent">
                  <p>Because we’re simply better. Our competitors employ off-shore “writers” for whom English is a 2nd or 3rd language, and who will work for less than a subsistence wage. Much of what other websites deliver comes from “paper mill” digital content that is plagiarized from random websites. You may receive a completed document, but handing it in to your professor will likely create more problems than you have ever imagined. Our writers are professionals with serious academic backgrounds. We value their skills and their commitment to our clients’ success, and we compensate them accordingly. As in much of life, you get what you pay for; paying what appears to be a “bargain” rate is always more expensive in the long run.</p>

                </div>
              </div>
              <div class="accordionItem close">
                <h4 class="accordionItemHeading">How does your “Value Pricing” work?</h4>
                <div class="accordionItemContent">
                  <p>Because we’re simply better. Our competitors employ off-shore “writers” for whom English is a 2nd or 3rd language, and who will work for less than a subsistence wage. Much of what other websites deliver comes from “paper mill” digital content that is plagiarized from random websites. You may receive a completed document, but handing it in to your professor will likely create more problems than you have ever imagined. Our writers are professionals with serious academic backgrounds. We value their skills and their commitment to our clients’ success, and we compensate them accordingly. As in much of life, you get what you pay for; paying what appears to be a “bargain” rate is always more expensive in the long run.</p>

                </div>
              </div>
              <div class="accordionItem close">
                <h4 class="accordionItemHeading">Do you guarantee your writing quality?</h4>
                <div class="accordionItemContent">
                  <p>Because we’re simply better. Our competitors employ off-shore “writers” for whom English is a 2nd or 3rd language, and who will work for less than a subsistence wage. Much of what other websites deliver comes from “paper mill” digital content that is plagiarized from random websites. You may receive a completed document, but handing it in to your professor will likely create more problems than you have ever imagined. Our writers are professionals with serious academic backgrounds. We value their skills and their commitment to our clients’ success, and we compensate them accordingly. As in much of life, you get what you pay for; paying what appears to be a “bargain” rate is always more expensive in the long run.</p>

                </div>
              </div>
              <div class="accordionItem close">
                <h4 class="accordionItemHeading">Do you guarantee your writing quality?</h4>
                <div class="accordionItemContent">
                  <p>Because we’re simply better. Our competitors employ off-shore “writers” for whom English is a 2nd or 3rd language, and who will work for less than a subsistence wage. Much of what other websites deliver comes from “paper mill” digital content that is plagiarized from random websites. You may receive a completed document, but handing it in to your professor will likely create more problems than you have ever imagined. Our writers are professionals with serious academic backgrounds. We value their skills and their commitment to our clients’ success, and we compensate them accordingly. As in much of life, you get what you pay for; paying what appears to be a “bargain” rate is always more expensive in the long run.</p>

                </div>
              </div>
              <div class="accordionItem close">
                <h4 class="accordionItemHeading">Do you guarantee your writing quality?</h4>
                <div class="accordionItemContent">
                  <p>Because we’re simply better. Our competitors employ off-shore “writers” for whom English is a 2nd or 3rd language, and who will work for less than a subsistence wage. Much of what other websites deliver comes from “paper mill” digital content that is plagiarized from random websites. You may receive a completed document, but handing it in to your professor will likely create more problems than you have ever imagined. Our writers are professionals with serious academic backgrounds. We value their skills and their commitment to our clients’ success, and we compensate them accordingly. As in much of life, you get what you pay for; paying what appears to be a “bargain” rate is always more expensive in the long run.</p>

                </div>
              </div>

            </div>
          </div>
        </div>
        <div class="sideBar">
          <div class="sidebarWraper">
            <div class="benefitsWrap">
              <div class="benefitsInner">
                <h3>Benefits</h3>
                <ul>
                  <li><span>Free</span> Proofreading</li>
                  <li><span>Free</span> Unlimited Revisions</li>
                  <li><span>Free</span> Formatting</li>
                  <li><span>Free</span> Title Page</li>
                  <li><span>Free</span> Bibliography</li>
                  <li><span>US Qualified</span> Writers</li>
                  <li><span>No Plagiarism</span> Guaranteed</li>
                  <li><span>Direct contact</span> with Writer</li>
                  <li><span>100%</span> Secure & Confidential</li>
                  <li><span>Deadline</span> Driven</li>
                </ul>
              </div>
            </div>
            <div class="valueBox">
              <div class="valueInner">
                <small>Our average page is</small>
                <p class="words">300 WORDS</p>

                <p class="discount">so you get upto 20% more value
                  <span class="roundShape left"></span>
                  <span class="roundShape right"></span>
                </p>
              </div>
            </div>
            <div class="papersWrap">
              <div class="papersInner">
                <p class="paperMatters">If your paper matters,</p>
                <p class="goodWords">we own all the good words.</p>
                <p class="panelOrder"><a href="#javascript:;">Order Now</a> </p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
