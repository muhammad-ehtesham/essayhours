  <style media="screen">
*,body{
  box-sizing: border-box;
}
  .container{
    max-width: 1170px;
    width: 100%;
    margin: 0 auto;
    padding: 0 15px;
  }
  section.pricingPage {
    padding: 50px 0;
  }
  .pricingWraper {
    width: 100%;
    display: flex;
    justify-content: space-between;
  }
  .pricingContent {
    max-width: 750px;
    width: 100%;
    float: left;
  }
  .pricingContent h1 {
    line-height: 46px;
    color: #35414B;
    font-size: 40px;
    font-family: 'GT-Walsheim-Regular';
    padding-bottom: 30px;
  }
  .pricingContent p{
    font-size: 16px;
    line-height: 18px;
    color: #35414B;
    padding-bottom: 40px;
    font-family: 'GT-Walsheim-Regular';
  }
  .pricingContent  a {
    color: #249dec;
    text-decoration: none;
  }
  .reviewWraper {
    width: 100%;
    background: #FFFFFF;
    box-shadow: 0px 2px 5px rgba(124, 124, 124, 0.5);
    border-radius: 12px;
    padding: 20px 30px;
    position: relative;
    margin-bottom: 50px;
  }
  .reviewBox1 ul {
    list-style: none;
    display: flex;
    justify-content: space-between;
  }
  .reviewBox1 p {
    padding-bottom: 0;
    padding-top: 10px;
    font-size: 22px;
    line-height: 28px;
  }
  .reviewBox1 .custName {
    text-align: right;
    font-size: 16px;
    line-height: 18px;
  }
  .reviewBox1 ul  li {
    font-size: 30px;
    line-height: 30px;
    text-align: center;
    color: #35414B;
    font-family: 'GT-Walsheim-Regular';
  }

  /* SideBar CSS */
  .sideBar {
    max-width: 230px;
    width: 100%;
    margin-left: auto;
  }
  .benefitsWrap{
    filter: drop-shadow(0px 2px 14px rgba(216, 217, 255, 0.668003));
    margin-bottom: 50px;
  }
  .benefitsInner{
    background: #Fff;
    color: #222;
    clip-path: polygon(50% 0%, 100% 21%, 100% 100%, 0 100%, 0 21%);
    width: 100%;
    min-height: 400px;
    padding-top: 50px;
  }
  .benefitsInner h3 {
    font-size: 33px;
    line-height: 38px;
    color: #35414B;
    font-family: 'GT-Walsheim-Regular';
    text-align: center;
  }
  .benefitsInner ul {
    padding-top: 10px;
    list-style: none;
    padding-left: 15px;
  }
  .benefitsInner ul li{
    padding-bottom: 7px;
    font-size: 15px;
    line-height: 22px;
    text-align: justify;
    font-family: 'GT-Walsheim-Regular';
  }
  .benefitsInner ul li span{
    color: #249DEC;
  }
  .valueBox {
    width: 100%;
    min-height: 230px;
    background: #FFFFFF;
    box-shadow: 0px 2px 14px rgba(216, 217, 255, 0.668003);
    border-radius: 8px;
    margin-bottom: 50px;
    border: 3px solid #249DEC;
  }
  .valueInner {
    padding: 55px 0 0 0;
    text-align: center;
    position: relative;
  }
  .valueInner::before {
    position: absolute;
    content: '';
    width: 90px;
    height: 70px;
    background: #EBF5FF;
    border-radius: 0 0 150px 0;
    top: 0;
    left: 0;
  }
  .valueInner small {
    font-size: 19px;
    line-height: 22px;
    text-align: center;
    letter-spacing: 0.01em;
    color: #35414B;
    font-family: 'GT-Walsheim-Regular';
    position: relative;
  }
  .valueInner  .words {
    font-size: 34px;
    line-height: 39px;
    text-align: center;
    color: #35414B;
    font-family: 'GT-Walsheim-Regular';
    font-weight: 600;
  }
  .valueInner .discount {
    font-size: 22px;
    line-height: 27px;
    text-align: center;
    letter-spacing: 0.01em;
    color: #35414B;
    font-family: 'GT-Walsheim-Regular';
    padding-top: 10px;
    border-top: 2px dashed #D5D2DC;
    margin-top: 30px;
    position: relative;
  }
  .papersWrap{
    filter: drop-shadow(0px 2px 14px rgba(216, 217, 255, 0.668003));
    margin-bottom: 50px;
  }
  .papersInner{
    background: #Fff;
    color: #222;
    clip-path: polygon(100% 0, 100% 75%, 50% 100%, 0 75%, 0 0);
    width: 100%;
    min-height: 400px;
    padding-top: 70px;
    text-align: center;
  }
  p.paperMatters {
    font-size: 33px;
    line-height: 38px;
    text-align: center;
    color: #35414B;
    font-family: 'GT-Walsheim-Regular';
  }
  p.goodWords {
    font-size: 33px;
    line-height: 38px;
    text-align: center;
    color: #35414B;
    font-family: 'GT-Walsheim-Regular';
    font-weight: 600;
  }
  p.panelOrder {
    margin-top: 20px;
  }
  .papersInner a {
    background: #249DEC;
    border: 3px solid #249DEC;
    box-sizing: border-box;
    border-radius: 30px;
    color: #fff;
    text-decoration: none;
    padding: 10px;
    display: block;
    max-width: 160px;
    margin: 0 auto;
    width: 100%;
  }
  .roundShape::before {
    position: absolute;
    content: '';
    width: 32px;
    height: 32px;
    background: #fff;
    border-radius: 100%;
    top: -20px;
    left: -20px;
    border: 3px solid #249DEC;
  }
  .roundShape::after {
    content: "";
    position: absolute;
    content: '';
    width: 18px;
    height: 38px;
    background: #fff;
    top: -20px;
    left: -21px;
  }
  .roundShape.right::before {
    top: -20px;
    right: -20px;
    left: auto;
  }
  .roundShape.right::after {
    top: -20px;
    right: -21px;
    left: auto;
  }
  @media (max-width:1200px) {
    .container{
      max-width: 950px;
    }
    .pricingContent {
      max-width: 680px;
      padding-right: 50px;
    }
    .benefitsInner ul li {
      font-size: 13px;
    }
    .sideBar {
      max-width: 210px;
    }
    p.goodWords {
      font-size: 30px;
    }
    p.paperMatters {
      font-size: 25px;
    }
  }
  @media (max-width:991px) {
    .container{
      max-width: 820px;
    }
    .reviewBox1 ul li {
      font-size: 22px;
    }
    .reviewBox1 p {
      font-size: 16px;
    }
    .pricingContent {
      padding-right: 50px;
    }
  }
  @media (max-width:767px) {
    .container{
      max-width: 720px;
    }
    .pricingContent {
      max-width: unset;
      width: 100%;
    }
    .sideBar {
      display: none;
    }
    .reviewWraper {
      width: 100%;
    }
    .reviewBox1 p {
      font-size: 17px;
    }
    .pricingContent {
      padding-right: 0;
    }

  }
  @media (max-width:575px) {
    .container{
      max-width: 540px;
    }
    .reviewBox1 p {
      font-size: 13px;
    }
    .reviewBox1 ul li {
      font-size: 23px;
    }
    .reviewBox1 ul {
      flex-direction: column;
    }
  }
  </style>
</head>
<body>
  <section class="pricingPage reviews">
    <div class="container">
      <div class="pricingWraper">
        <div class="pricingContent">
          <h1>Reviews</h1>
          <div class="reviewWraper">
            <div class="reviewBox1">
              <ul>
                <li>5/5</li>
                <li>“Best Service to Hire”</li>
                <li><img src="images/review-stars.svg" alt="Review Stars"> </li>
              </ul>
              <p>I have always struggled to find the best research paper writing service and never felt like I found any that were really going to help me get good grades in my classes. It was so frustrating because sometimes a site would sound promising, but then they gave me an F on one of my assignments for some reason which is why I ended up at EssayHours.com who helped turn everything around! Now I'm getting A's thanks to them!</p>
              <p class="custName">James, Edinburgh</p>
            </div>
          </div>
          <div class="reviewWraper">
            <div class="reviewBox1">
              <ul>
                <li>5/5</li>
                <li>“Best Service to Hire”</li>
                <li><img src="images/review-stars.svg" alt="Review Stars"> </li>
              </ul>
              <p>EssayHours.com is the website I wish I found sooner because it's been so helpful to me. It provides such detailed insight and research that has helped my study for Economics without any problems.</p>
              <p class="custName">Oliver D., NY</p>
            </div>
          </div>

          <div class="reviewWraper">
            <div class="reviewBox1">
              <ul>
                <li>5/5</li>
                <li>“Best Service to Hire”</li>
                <li><img src="images/review-stars.svg" alt="Review Stars"> </li>
              </ul>
              <p>I thought I would never find a web-based company to help me with my research. Second-year is the most difficult one, but they did actually help me so much! They helped improve my knowledge by teaching from their own findings which I think has taught me more than any textbook.</p>
              <p class="custName">Noah, Singapore</p>
            </div>
          </div>
          <div class="reviewWraper">
            <div class="reviewBox1">
              <ul>
                <li>5/5</li>
                <li>“Best Service to Hire”</li>
                <li><img src="images/review-stars.svg" alt="Review Stars"> </li>
              </ul>
              <p>I am so grateful for your help with this paper. The extra time and creativeness you put into it mean a lot to me; I can't wait until we start working together more! Thank you again, keep up the good work :)</p>
              <p class="custName">Samuel, Chicago</p>
            </div>
          </div>

          <div class="reviewWraper">
            <div class="reviewBox1">
              <ul>
                <li>5/5</li>
                <li>“Best Service to Hire”</li>
                <li><img src="images/review-stars.svg" alt="Review Stars"> </li>
              </ul>
              <p>I've been coming to this site for a while now, and I can't tell you how amazing it is. The Professionals are great at what they do - from answering questions quickly (usually within minutes) or making sure the assignments are delivered on time. You never have trouble with anything when ordering here--it's all top-notch!</p>
              <p class="custName">David, Los Angeles</p>
            </div>
          </div>
          <div class="reviewWraper">
            <div class="reviewBox1">
              <ul>
                <li>5/5</li>
                <li>“Best Service to Hire”</li>
                <li><img src="images/review-stars.svg" alt="Review Stars"> </li>
              </ul>
              <p>The company is the best! I used to be so stressed out before when writing my essays. But now, it's all good with this service. You can ask for changes if you need them and get to know your writer really well too since they're available 24/7!</p>
              <p class="custName">Andrew, Texas</p>
            </div>
          </div>

          <div class="reviewWraper">
            <div class="reviewBox1">
              <ul>
                <li>5/5</li>
                <li>“Best Service to Hire”</li>
                <li><img src="images/review-stars.svg" alt="Review Stars"> </li>
              </ul>
              <p>I struggled to write my essay in time for the deadline, but luckily I found a great service that allowed me to get it done. High;y recommended!</p>
              <p class="custName">Luca, Oregon</p>
            </div>
          </div>
          <div class="reviewWraper">
            <div class="reviewBox1">
              <ul>
                <li>5/5</li>
                <li>“Best Service to Hire”</li>
                <li><img src="images/review-stars.svg" alt="Review Stars"> </li>
              </ul>
              <p>I was hesitant to pay for someone to write my nursing graduate thesis, but I just couldn't keep up with the workload. My classmates told me about your website because they knew what a lifesaver you were going to be when it came down to meeting an insane deadline like mine! Thanks so much!</p>
              <p class="custName">Cole B., Maryland</p>
            </div>
          </div>

          <div class="reviewWraper">
            <div class="reviewBox1">
              <ul>
                <li>5/5</li>
                <li>“Best Service to Hire”</li>
                <li><img src="images/review-stars.svg" alt="Review Stars"> </li>
              </ul>
              <p>I could not ask for better! I want to thank you so much for the outstanding job that my paper was done. The professionalism and passion shown in this work are clearly evident, from start to finish!</p>
              <p class="custName">Patrick, New Mexico</p>
            </div>
          </div>
          <div class="reviewWraper">
            <div class="reviewBox1">
              <ul>
                <li>5/5</li>
                <li>“Best Service to Hire”</li>
                <li><img src="images/review-stars.svg" alt="Review Stars"> </li>
              </ul>
              <p>Your writers were beyond helpful. I was so nervous to place my order, but the minute that I did things started happening. Your website Dashboard is amazing and made me feel relieved about getting my paper done on time. Thank you for your quick responses as well!</p>
              <p class="custName">Elizabeth, California</p>
            </div>
          </div>

          <div class="reviewWraper">
            <div class="reviewBox1">
              <ul>
                <li>5/5</li>
                <li>“Best Service to Hire”</li>
                <li><img src="images/review-stars.svg" alt="Review Stars"> </li>
              </ul>
              <p>Thanks for the paper. I really appreciate it and think you'll do a great job with what's to come!</p>
              <p>I am so excited about your work that I want you to be able to help me out on all my papers, as long as they are of equal or better quality than this one was. </p>
              <p class="custName">Nora S., Georgia</p>
            </div>
          </div>
          <div class="reviewWraper">
            <div class="reviewBox1">
              <ul>
                <li>5/5</li>
                <li>“Best Service to Hire”</li>
                <li><img src="images/review-stars.svg" alt="Review Stars"> </li>
              </ul>
              <p>I could not believe it when I saw my paper! It was just what you said, and at such an affordable price too. You can be sure that from now on, my papers will always come to the best service ever: 24-hour essay writing services</p>
              <p class="custName">Hunter H., New South Wales</p>
            </div>
          </div>

          <div class="reviewWraper">
            <div class="reviewBox1">
              <ul>
                <li>5/5</li>
                <li>“Best Service to Hire”</li>
                <li><img src="images/review-stars.svg" alt="Review Stars"> </li>
              </ul>
              <p>I was constantly disappointed with the other writing services I tried before coming to "EssayHours.com" The papers from another site were so disappointing that one of them got a failing grade. But then my personal writer here pulled me grades way up and made it easy for me, she really is the best!</p>
              <p class="custName">Victoria Lily, Louisiana</p>
            </div>
          </div>
          <div class="reviewWraper">
            <div class="reviewBox1">
              <ul>
                <li>5/5</li>
                <li>“Best Service to Hire”</li>
                <li><img src="images/review-stars.svg" alt="Review Stars"> </li>
              </ul>
              <p>I was so pleased with the work that I received from your writer. It's clear they have a deep understanding of Environmental Sciences and all my fellow students were jealous when mine showed up in class because it was better than theirs!</p>
              <p>The service provided by you is amazing; anyone who asks for help should know to come here first.</p>
              <p class="custName">Kennedy, Virginia</p>
            </div>
          </div>

          <div class="reviewWraper">
            <div class="reviewBox1">
              <ul>
                <li>5/5</li>
                <li>“Best Service to Hire”</li>
                <li><img src="images/review-stars.svg" alt="Review Stars"> </li>
              </ul>
              <p>I've never seen a paper so well written. It was really insightful and funny, too. I am so impressed with the quality of your writers and their work! I can't believe you got me a perfect grade. And what's even better is that it was really easy to order my paper from your site, too. Thanks for making this whole process less stressful.</p>
              <p class="custName">Julia, Ohio</p>
            </div>
          </div>
          <div class="reviewWraper">
            <div class="reviewBox1">
              <ul>
                <li>5/5</li>
                <li>“Best Service to Hire”</li>
                <li><img src="images/review-stars.svg" alt="Review Stars"> </li>
              </ul>
              <p>It's rare to find a company that provides such thorough and thoughtful essays. I've been satisfied with every paper you have done for me, which is why I will be coming back next semester without hesitation! Great Work.</p>
              <p class="custName">Gerry S., Stanford</p>
            </div>
          </div>

          <div class="reviewWraper">
            <div class="reviewBox1">
              <ul>
                <li>5/5</li>
                <li>“Best Service to Hire”</li>
                <li><img src="images/review-stars.svg" alt="Review Stars"> </li>
              </ul>
              <p>I'm a good writer, but there was just no time to pull together a complete history of Contract Law. The lawyer you assigned me happened to be an expert in that field and they wrote the book like it was their own! I'll always owe you one now too for doing this favor for me.</p>
              <p class="custName">Isabel, Florida</p>
            </div>
          </div>
          <div class="reviewWraper">
            <div class="reviewBox1">
              <ul>
                <li>5/5</li>
                <li>“Best Service to Hire”</li>
                <li><img src="images/review-stars.svg" alt="Review Stars"> </li>
              </ul>
              <p>Thanks for delivering on time and covering all the bases. I had been worried about my sociology paper for weeks, but after reading what you guys wrote up in less than a week? It was so easy to read and understand that it made me feel confident enough to take an upper 10% grade!</p>
              <p class="custName">Catalina, Alaska</p>
            </div>
          </div>

          <div class="reviewWraper">
            <div class="reviewBox1">
              <ul>
                <li>5/5</li>
                <li>“Best Service to Hire”</li>
                <li><img src="images/review-stars.svg" alt="Review Stars"> </li>
              </ul>
              <p>Your writing service is the best! I can't believe how quickly you delivered my research paper and got it to me on time. Your customer service was awesome too because they answered all of my questions before purchasing with detailed answers that made me feel confident in ordering from your website for a first-time buyer. </p>
              <p>Thank you so much for getting back at such short notice, and staying true to our agreement. </p>
              <p class="custName">Tony B., Texas </p>
            </div>
          </div>
          <div class="reviewWraper">
            <div class="reviewBox1">
              <ul>
                <li>5/5</li>
                <li>“Best Service to Hire”</li>
                <li><img src="images/review-stars.svg" alt="Review Stars"> </li>
              </ul>
              <p>Wow, you guys are really on it! I ordered my paper 24 hours ago and now I'm finally able to relax. Nice work. I am so glad I found you guys. You saved my life!</p>
              <p class="custName">Darren S., Washington</p>
            </div>
          </div>


        </div>
        <div class="sideBar">
          <div class="sidebarWraper">
            <div class="benefitsWrap">
              <div class="benefitsInner">
                <h3>Benefits</h3>
                <ul>
                  <li><span>Free</span> Proofreading</li>
                  <li><span>Free</span> Unlimited Revisions</li>
                  <li><span>Free</span> Formatting</li>
                  <li><span>Free</span> Title Page</li>
                  <li><span>Free</span> Bibliography</li>
                  <li><span>US Qualified</span> Writers</li>
                  <li><span>No Plagiarism</span> Guaranteed</li>
                  <li><span>Direct contact</span> with Writer</li>
                  <li><span>100%</span> Secure & Confidential</li>
                  <li><span>Deadline</span> Driven</li>
                </ul>
              </div>
            </div>
            <div class="valueBox">
              <div class="valueInner">
                <small>Our average page is</small>
                <p class="words">300 WORDS</p>

                <p class="discount">so you get upto 20% more value
                  <span class="roundShape left"></span>
                  <span class="roundShape right"></span>
                </p>
              </div>
            </div>
            <div class="papersWrap">
              <div class="papersInner">
                <p class="paperMatters">If your paper matters,</p>
                <p class="goodWords">we own all the good words.</p>
                <p class="panelOrder"><a href="#javascript:;">Order Now</a> </p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
