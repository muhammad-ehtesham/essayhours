<style media="screen">
*,body{
  box-sizing: border-box;
}
.container{
  max-width: 1170px;
  width: 100%;
  margin: 0 auto;
  padding: 0 15px;
}
section.orderpage {
  background-color: #f8f8f8;
  background-image: url('images/order-bg.svg');
  background-repeat: no-repeat;
  background-position-x: 100%;
  background-position-y: 10%;
  padding: 50px 0;
}
.topFeatures {
  display: flex;
  justify-content: space-between;
  align-items: center;
}
.featuresInner {
  max-width: 60px;
  width: 100%;
  background-color: #249DEC;
  border-radius: 344px;
  height: 80px;
  position: relative;
}
.featuresWraper {
  width: 20%;
  min-height: 124px;
  display: flex;
  align-items: center;
  flex-direction: column;
}
.spriteImages {
  background-image: url('images/features.png');
  position: absolute;
  right: -10px;
  bottom: 0;
}
.sprite1{
  width: 54px; height: 53px;
  background-position:-167px -10px;
}
.sprite2{
  width: 54px; height: 53px;
  background-position: -84px -92px;
}
.sprite3{
  width: 63px; height: 62px;
  background-position: -10px -10px;
}
.sprite4{
  width: 54px; height: 53px;
  background-position: -10px -92px;
}
.sprite5{
  width: 54px; height: 53px;
  background-position: -93px -10px;
}
.featuresWraper p {
  font-size: 18px;
  line-height: 22px;
  text-align: center;
  font-family: 'GT-Walsheim-Regular';
}
.orderTabs {
  margin-top: 55px;
}
.tabsLeft {
  width: 100%;
}
ul.tabsWraper {
  list-style: none;
  display: flex;
  flex-direction: row;
  justify-content: space-between;
}
.leftInner {
  display: flex;
  flex-direction: column;
  align-items: center;
}
.leftInner a {
  width: 70px;
  height: 70px;
  background-color: #CED5DC;
  border-radius: 50%;
  display: flex;
  align-items: center;
  justify-content: center;
  color: #fff;
  text-decoration: none;
  font-family: 'Poppins';
  font-weight: 600;
  font-size: 26px;
  border: 10px solid #f8f8f8;
  z-index: 10;
}
.leftInner p{
  font-size: 20px;
  line-height: 117.9%;
  font-family: 'GT-Walsheim-Regular';
  padding-top: 10px;
}
.tabsWraper li {
  position: relative;
  width: 33%;
}
.tabsWraper li:first-child::before {
  position: absolute;
  content: '';
  width: 100%;
  transform: translate(55%);
  top: 30%;
  height: 2px;
  background-color: #CED5DC;
}
.tabsWraper li:nth-child(2)::before {
  position: absolute;
  content: '';
  width: 100%;
  transform: translate(55%);
  top: 30%;
  height: 2px;
  background-color: #CED5DC;
}
.orderForm {
  max-width: 751px;
  width: 100%;
  margin-top: 55px;
  background-color: #fff;
  padding: 30px 96px 40px 30px;
  position: relative;
  box-shadow: -11px 15px 60px rgb(183 183 183 / 25%);
}
.fieldsWraper {
  max-width: 419px;
  width: 100%;
  height: 46px;
}
.fieldsWraper input, .fieldsWraper select {
  width: 100%;
  height: 100%;
  background: #FFFFFF;
  border: 1px solid #35414B;
  box-sizing: border-box;
  border-radius: 8px;
  padding-left: 43px;
  outline: none;
}
.formFields input::placeholder {
  font-size: 17px;
  line-height: 19px;
  font-family: 'GT-Walsheim-Regular';
  color: #35414B;
}
.formFields {
  width: 100%;
  display: flex;
  align-items: center;
  justify-content: space-between;
  margin-bottom: 20px;
  position: relative;
}
.formFields label {
  font-family: 'GT-Walsheim-Regular';
  font-size: 21px;
  line-height: 24px;
}
p.form-head {
  background-color: #249DEC;
  padding: 10px;
  width: calc(100% + 32px );
  position: absolute;
  left: -15px;
  top: 30px;
  font-size: 39px;
  color: #fff;
  padding-left: 40px;
  font-family: 'Poppins';
}
form.sh-form {
  padding-top: 120px;
}
p.form-head::before {
  position: absolute;
  content: '';
  width: 0;
  height: 0;
  border-top: 18px solid #9D9D9D;
  border-left: 18px solid transparent;
  left: 0;
  bottom: -18px;
}
p.form-head::after {
  position: absolute;
  content: '';
  width: 0;
  height: 0;
  border-top: 18px solid #9D9D9D;
  border-right: 18px solid transparent;
  right: 0;
  bottom: -18px;
}
.pages{
  position: relative;
}
span.subtrct {
  background-color: #FFCE4B;
  width: 40px;
  height: 46px;
  display: flex;
  align-items: center;
  justify-content: center;
  position: absolute;
  left: 0;
  top: 24px;
  border-radius: 8px;
  border: 1px solid #35414B;
  box-sizing: border-box;
  cursor: pointer;
}
span.add {
  background-color: #FFCE4B;
  width: 40px;
  height: 46px;
  display: flex;
  align-items: center;
  justify-content: center;
  position: absolute;
  right: 0;
  top: 24px;
  border-radius: 8px;
  border: 1px solid #35414B;
  box-sizing: border-box;
  cursor: pointer;
}
.formButton {
  margin-top: 45px;
  width: 100%;
  text-align: right;
}
.formButton input {
  border: 3px solid #FFCE4B;
  box-sizing: border-box;
  box-shadow: 0px 2px 4px rgba(0, 0, 0, 0.25);
  border-radius: 8px;
  background-color: #FFCE4B;
  color: #35414B;
  text-decoration: none;
  padding: 11px 40px;
  font-family: 'GT-Walsheim-Regular';
  cursor: pointer;
}
.formButton input:hover {
  border: 3px solid #FFCE4B;
  background-color: #fff;
}
.orderWraper {
  width: 100%;
  display: flex;
  align-items: center;
  justify-content: center;
}
.priceBox {
  background: #FFFFFF;
  border: 2px solid #249DEC;
  box-sizing: border-box;
  box-shadow: -11px 15px 60px rgba(183, 183, 183, 0.25);
  max-width: 365px;
  width: 100%;
  text-align: center;
  margin-left: -10px;
  padding: 20px 0 25px;
  margin-top: 100px;
}
p.price {
  font-size: 28px;
  line-height: 32px;
  color: #35414B;
  font-family: 'GT-Walsheim-Regular';
  margin: 0;
}
.priceBox span {
  font-size: 17px;
  line-height: 19px;
  font-family: 'GT-Walsheim-Regular';
}
span.was {
  padding-right: 35px;
}
p.save {
  font-size: 28px;
  line-height: 32px;
  text-align: center;
  color: #1BA260;
  font-family: 'GT-Walsheim-Regular';
  padding-top: 20px;
}
p.offerEnds {
  font-size: 17px;
  line-height: 19px;
  text-align: center;
  color: #FF2A13;
  font-family: 'GT-Walsheim-Regular';
}
.sidecheckbox {
  display: flex;
  align-items: center;
  justify-content: center;
  margin-top: 22px;
  margin-bottom: 10px;
}
.sidecheckbox p {
  padding-left: 5px;
}
p.innerP {
  font-size: 17px;
  line-height: 19px;
  text-align: center;
  color: #35414B;
  font-family: 'GT-Walsheim-Regular';
}
p.totalPrice {
  margin-top: 25px;
  padding-top: 20px;
  border-top: 2px dashed #D5D2DC;
  color: #249DEC;
  font-family: 'GT-Walsheim-Regular';
  font-size: 32px;
  line-height: 37px;
}
.provideDetails .formButton {
  width: 100%;
  display: flex;
  justify-content: space-between;
  align-items: center;
}
a.backbtn {
  background: #fff;
  border: none;
  box-shadow: none;
  padding: 0;
}
.textfield {
  resize: none;
  max-width: 419px;
  width: 100%;
  height: 86px;
  box-sizing: border-box;
  border-radius: 8px;
  outline: none;
  padding-left: 13px;
  padding-top: 12px;
}
.provideDetails .detailsPanel {
  display: flex;
  justify-content: center;
  margin-top: 20px;
}
p.agree {
  font-size: 14px;
  line-height: 16px;
  text-align: center;
  color: #35414B;
  font-family: 'GT-Walsheim-Regular';
  margin-top: 30px;
  padding: 0 20px;
}
.acceptMethods {
  width: 100%;
  float: left;
  margin-top: 15px;
}
.agree a {
  color: #249DEC;
  text-decoration: none;
}
p.payLater {
  margin-top: 25px;
  float: right;
  padding-right: 60px;
}
.payLater a {
  color: #249DEC;
  font-size: 11px;
  line-height: 13px;
  font-family: 'GT-Walsheim-Regular';
}
.provideDetails .priceBox {
  margin-top: 170px;
}
.formsprites{
  position: relative;
}
.formsprites::before{
  position: absolute;
  content: '';
  background-image: url('images/form-sprites.png');
}
.input-bg::before {
  width: 20px;
  height: 20px;
  background-position: -50px -10px;
  top: 13px;
  left: 13px;
}
.select-bg::before {
  width: 20px;
  height: 20px;
  background-position: -10px -10px;
  top: 13px;
  left: 13px;
}
.provideDetails .formFields input, .provideDetails .formFields select {
  padding-left: 13px;
}
.show{
  display: block;
}
.error {
    border-color: #f00 !important;
}
.valid {
    border-color: #1BA260 !important;
}
.af-none{
  display: none;
}
@media (max-width:1200px) {
  .orderForm {
    padding: 30px 30px 40px 30px;
  }
  .fieldsWraper {
    max-width: 380px;
  }
  .textfield {
    max-width: 380px;
  }
}
@media (max-width:992px) {
  .container{
    max-width: 960px;
  }
  .orderForm {
    max-width: 710px;
  }
  .orderWraper {
    flex-direction: column;
  }

  .priceBox {
    margin-top: -10px;
  }
  .provideDetails .priceBox {
    margin-top: -10px;
  }
  p.form-head {
    width: calc(100% + 35px );
    left: -17px;
  }
  p.form-head::before {
    left: 1px;
    bottom: -18px;
  }
  .priceBox {
    max-width: 450px;
  }
  .featuresWraper p {
    font-size: 15px;
    line-height: 20px;
    padding: 0 5px;
  }
  .featuresWraper {
    min-height: 140px;
  }
}
@media (max-width:768px) {
  .container{
    max-width: 720px;
  }
  .formFields label {
    font-family: 'GT-Walsheim-Regular';
    font-size: 18px;
  }
  .fieldsWraper {
    max-width: 340px;
  }
  .textfield {
    max-width: 340px;
  }
  p.form-head {
    width: calc(100% + 30px );
    left: -17px;
  }
}
@media (max-width:576px) {
  .container{
    max-width: 540px;
  }
  p.form-head {
    font-size: 30px;
    width: calc(100% + 30px );
  }
  .formFields {
    align-items: flex-start;
    flex-direction: column;
  }
  .formFields label {
    padding-bottom: 10px;
  }
  .priceBox {
    max-width: 360px;
  }
  .leftInner p {
    font-size: 12px;
  }
  .formButton input {
    padding: 11px 20px;
  }
  .formButton {
    text-align: left;
  }
  .topFeatures {
    display: none;
  }
  .orderTabs {
    margin-top: 0px;
  }
}
@media (max-width:480px) {
  .priceBox {
    max-width: 270px;
  }
  .formButton input {
    padding: 10px;
    font-size: 11px;
  }
  p.payLater {
    padding-right: 10px;
  }
  .tabsWraper li:first-child::before {
    top: 40%;
  }
  .tabsWraper li:nth-child(2)::before {
    top: 40%;
  }
  .formFields input::placeholder {
    font-size: 13px;
  }
  .formButton {
    text-align: center;
  }
}
</style>
<body>
  <section class="orderpage">
    <div class="container">
      <div class="topFeatures">
        <div class="featuresWraper">
          <div class="featuresInner">
            <div class="spriteImages sprite1"></div>
          </div>
          <p>Deadline driven</p>
        </div>
        <div class="featuresWraper">
          <div class="featuresInner">
            <div class="spriteImages sprite2"></div>
          </div>
          <p>100% money back guarantee</p>
        </div>
        <div class="featuresWraper">
          <div class="featuresInner">
            <div class="spriteImages sprite3"></div>
          </div>
          <p>Prepay 50% upfront - half after we deliver</p>
        </div>
        <div class="featuresWraper">
          <div class="featuresInner">
            <div class="spriteImages sprite4"></div>
          </div>
          <p>No plagiarism guaranteed (reports provided)</p>
        </div>
        <div class="featuresWraper">
          <div class="featuresInner">
            <div class="spriteImages sprite5"></div>
          </div>
          <p>100% private and confidential</p>
        </div>
      </div>
      <div class="orderTabs">
        <div class="tabsLeft">
          <ul class="tabsWraper">
            <li class="active">
              <div class="leftInner">
                <a href="javascript:;">1</a>
                <p>Calculate price</p>
              </div>
            </li>
            <li>
              <div class="leftInner">
                <a href="javascript:;">2</a>
                <p>Provide Details</p>
              </div>
            </li>
            <li>
              <div class="leftInner">
                <a href="javascript:;">3</a>
                <p>Make Payment</p>
              </div>
            </li>
          </ul>
        </div>
        <div class="orderWraper">
          <div class="orderForm">
            <p class="form-head">Calculate Price</p>
            <form class="sh-form" id="step1">
              <div class="formFields">
                <label>Your Email</label>
                <div class="fieldsWraper formsprites input-bg">
                  <input class="form1" type="email" id="f-email" placeholder="Enter your email address" >
                </div>
              </div>
              <div class="formFields">
                <label>Your Phone</label>
                <div class="formsprites fieldsWraper input-bg">
                  <input class="form1" type="number" id="fphoneNumber" placeholder="Enter your phone number" min="10" >
                </div>
              </div>
              <div class="formFields">
                <label>Document Type</label>
                <div class="formsprites fieldsWraper input-bg">
                  <select class="form1" name="" id="documentType"  >
                    <option value="" selected>What can we do for you?</option>
                    <option value="Admission Essay">Admission Essay</option>
                    <option value="Analytical Essay">Analytical Essay</option>
                    <option value="Annotated Bibliography">Annotated Bibliography</option>
                    <option value="Application Letter">Application Letter</option>
                    <option value="Argumentative Essay">Argumentative Essay</option>
                    <option value="Assessment">Assessment</option>
                    <option value="Assignment ">Assignment </option>
                    <option value="Biography">Biography</option>
                    <option value="Book Reports ">Book Reports </option>
                    <option value="Book Reviews ">Book Reviews </option>
                    <option value="Brief Overview">Brief Overview</option>
                    <option value="Business Plan">Business Plan</option>
                    <option value="Case Study ">Case Study </option>
                    <option value="College Paper ">College Paper </option>
                    <option value="Contrast Essay">Contrast Essay</option>
                    <option value="Coursework ">Coursework </option>
                    <option value="Cover Letter">Cover Letter</option>
                    <option value="Creative Writing">Creative Writing</option>
                    <option value="Critical analysis">Critical analysis</option>
                    <option value="Critical Thinking">Critical Thinking</option>
                    <option value="Dissertation">Dissertation</option>
                    <option value="eBooks ">eBooks </option>
                    <option value="Essay">Essay</option>
                    <option value="Exposition Writing">Exposition Writing</option>
                    <option value="Homework ">Homework </option>
                    <option value="Lab Report ">Lab Report </option>
                    <option value="Literature Review">Literature Review</option>
                    <option value="Movie Review ">Movie Review </option>
                    <option value="News Release ">News Release </option>
                    <option value="Online assignment">Online assignment</option>
                    <option value="Personal Statement">Personal Statement</option>
                    <option value="Powerpoint Presentation (with speaker notes)">Powerpoint Presentation (with speaker notes)</option>
                    <option value="Powerpoint Presentation (without speaker notes)">Powerpoint Presentation (without speaker notes)</option>
                    <option value="Quiz">Quiz</option>
                    <option value="Reflection paper">Reflection paper</option>
                    <option value="Reflective Essay">Reflective Essay</option>
                    <option value="Report">Report</option>
                    <option value="Research Essay">Research Essay</option>
                    <option value="Research Paper">Research Paper</option>
                    <option value="Research proposal ">Research proposal </option>
                    <option value="Response Essay">Response Essay</option>
                    <option value="Response paper">Response paper</option>
                    <option value="Scholarship Essay">Scholarship Essay</option>
                    <option value="School Paper ">School Paper </option>
                    <option value="Speech ">Speech </option>
                    <option value="Term Paper">Term Paper</option>
                    <option value="Thesis">Thesis</option>
                    <option value="Thesis Proposal">Thesis Proposal</option>
                    <option value="other">Other</option>
                  </select>
                </div>
              </div>
              <div class="formFields">
                <label>Academic Level</label>
                <div class="formsprites fieldsWraper select-bg">
                  <select class="form1" name="" id="academicLevel" >
                    <option value="" selected>You study at?</option>
                    <option>High school</option>
                    <option>College-undergraduate</option>
                    <option>Master</option>
                    <option>Doctoral</option>
                  </select>
                </div>
              </div>
              <div class="formFields">
                <label>Deadline</label>
                <div class="fieldsWraper">
                  <input class="form1" type="date" id="deadline" >
                </div>
              </div>
              <div class="formFields">
                <label>No. of pages</label>
                <div class="pages fieldsWraper">
                  <!-- <input type="text" id="pages">
                  <span onclick="decrementPagesOption()" class="subtrct">-</span>
                  <span onclick="incrementPagesOption()" class="add">+</span> -->
                  <label for="pages">No. of Pages</label> <br>
                  <span onclick="decrementPagesOption()" class="subtrct">-</span>
                  <select class="form1" name="pages" id="pages" >
                    <option value="1">1 Page ~ 300 Words</option>
                    <option value="2">2 Pages ~ 600 Words</option>
                    <option value="3">3 Pages ~ 900 Words</option>
                    <option value="4">4 Pages ~ 1200 Words</option>
                    <option value="5">5 Pages ~ 1500 Words</option>
                    <option value="6">6 Pages ~ 1800 Words</option>
                    <option value="7">7 Pages ~ 2100 Words</option>
                    <option value="8">8 Pages ~ 2400 Words</option>
                    <option value="9">9 Pages ~ 2700 Words</option>
                    <option value="10">10 Pages ~ 3000 Words</option>
                    <option value="11">11 Pages ~ 3300 Words</option>
                    <option value="12">12 Pages ~ 3600 Words</option>
                    <option value="13">13 Pages ~ 3900 Words</option>
                    <option value="14">14 Pages ~ 4200 Words</option>
                    <option value="15">15 Pages ~ 4500 Words</option>
                    <option value="16">16 Pages ~ 4800 Words</option>
                    <option value="17">17 Pages ~ 5100 Words</option>
                    <option value="18">18 Pages ~ 5400 Words</option>
                    <option value="19">19 Pages ~ 5700 Words</option>
                    <option value="20">20 Pages ~ 6000 Words</option>
                    <option value="21">21 Pages ~ 6300 Words</option>
                    <option value="22">22 Pages ~ 6600 Words</option>
                    <option value="23">23 Pages ~ 6900 Words</option>
                    <option value="24">24 Pages ~ 7200 Words</option>
                    <option value="25">25 Pages ~ 7500 Words</option>
                    <option value="26">26 Pages ~ 7800 Words</option>
                    <option value="27">27 Pages ~ 8100 Words</option>
                    <option value="28">28 Pages ~ 8400 Words</option>
                    <option value="29">29 Pages ~ 8700 Words</option>
                    <option value="30">30 Pages ~ 9000 Words</option>
                    <option value="31">31 Pages ~ 9300 Words</option>
                    <option value="32">32 Pages ~ 9600 Words</option>
                    <option value="33">33 Pages ~ 9900 Words</option>
                    <option value="34">34 Pages ~ 10200 Words</option>
                    <option value="35">35 Pages ~ 10500 Words</option>
                    <option value="36">36 Pages ~ 10800 Words</option>
                    <option value="37">37 Pages ~ 11100 Words</option>
                    <option value="38">38 Pages ~ 11400 Words</option>
                    <option value="39">39 Pages ~ 11700 Words</option>
                    <option value="40">40 Pages ~ 12000 Words</option>
                    <option value="41">41 Pages ~ 12300 Words</option>
                    <option value="42">42 Pages ~ 12600 Words</option>
                    <option value="43">43 Pages ~ 12900 Words</option>
                    <option value="44">44 Pages ~ 13200 Words</option>
                    <option value="45">45 Pages ~ 13500 Words</option>
                    <option value="46">46 Pages ~ 13800 Words</option>
                    <option value="47">47 Pages ~ 14100 Words</option>
                    <option value="48">48 Pages ~ 14400 Words</option>
                    <option value="49">49 Pages ~ 14700 Words</option>
                    <option value="50">50 Pages ~ 15000 Words</option>
                    <option value="51">51 Pages ~ 15300 Words</option>
                    <option value="52">52 Pages ~ 15600 Words</option>
                    <option value="53">53 Pages ~ 15900 Words</option>
                    <option value="54">54 Pages ~ 16200 Words</option>
                    <option value="55">55 Pages ~ 16500 Words</option>
                    <option value="56">56 Pages ~ 16800 Words</option>
                    <option value="57">57 Pages ~ 17100 Words</option>
                    <option value="58">58 Pages ~ 17400 Words</option>
                    <option value="59">59 Pages ~ 17700 Words</option>
                    <option value="60">60 Pages ~ 18000 Words</option>
                    <option value="61">61 Pages ~ 18300 Words</option>
                    <option value="62">62 Pages ~ 18600 Words</option>
                    <option value="63">63 Pages ~ 18900 Words</option>
                    <option value="64">64 Pages ~ 19200 Words</option>
                    <option value="65">65 Pages ~ 19500 Words</option>
                    <option value="66">66 Pages ~ 19800 Words</option>
                    <option value="67">67 Pages ~ 20100 Words</option>
                    <option value="68">68 Pages ~ 20400 Words</option>
                    <option value="69">69 Pages ~ 20700 Words</option>
                    <option value="70">70 Pages ~ 21000 Words</option>
                    <option value="71">71 Pages ~ 21300 Words</option>
                    <option value="72">72 Pages ~ 21600 Words</option>
                    <option value="73">73 Pages ~ 21900 Words</option>
                    <option value="74">74 Pages ~ 22200 Words</option>
                    <option value="75">75 Pages ~ 22500 Words</option>
                    <option value="76">76 Pages ~ 22800 Words</option>
                    <option value="77">77 Pages ~ 23100 Words</option>
                    <option value="78">78 Pages ~ 23400 Words</option>
                    <option value="79">79 Pages ~ 23700 Words</option>
                    <option value="80">80 Pages ~ 24000 Words</option>
                    <option value="81">81 Pages ~ 24300 Words</option>
                    <option value="82">82 Pages ~ 24600 Words</option>
                    <option value="83">83 Pages ~ 24900 Words</option>
                    <option value="84">84 Pages ~ 25200 Words</option>
                    <option value="85">85 Pages ~ 25500 Words</option>
                    <option value="86">86 Pages ~ 25800 Words</option>
                    <option value="87">87 Pages ~ 26100 Words</option>
                    <option value="88">88 Pages ~ 26400 Words</option>
                    <option value="89">89 Pages ~ 26700 Words</option>
                    <option value="90">90 Pages ~ 27000 Words</option>
                    <option value="91">91 Pages ~ 27300 Words</option>
                    <option value="92">92 Pages ~ 27600 Words</option>
                    <option value="93">93 Pages ~ 27900 Words</option>
                    <option value="94">94 Pages ~ 28200 Words</option>
                    <option value="95">95 Pages ~ 28500 Words</option>
                    <option value="96">96 Pages ~ 28800 Words</option>
                    <option value="97">97 Pages ~ 29100 Words</option>
                    <option value="98">98 Pages ~ 29400 Words</option>
                    <option value="99">99 Pages ~ 29700 Words</option>
                    <option value="100">100 Pages ~ 30000 Words</option>
                  </select>
                  <span onclick="incrementPagesOption()" class="add">+</span>

                </div>
              </div>
              <div class="formButton">
                <input type="submit"  value="Continue to order details">
              </div>
            </form>
          </div>
          <div class="priceBox">
            <p class="price">Your Price: $50</p>
            <p>
              <span class="was">Was: $50</span>
              <span>Now: $25</span>
            </p>
            <p class="save">You Save: $25</p>
            <p class="offerEnds">Offer ending in 12 : 17 :06</p>
            <div class="sidecheckbox">
              <input type="checkbox">
              <p>Prepay 50%</p>
            </div>
            <p class="innerP">Pay now only: $12.5</p>
            <p class="innerP">Pay on delivery: $12.5</p>
            <p class="totalPrice">Total Price: $25</p>
          </div>
        </div>
        <div class="orderWraper provideDetails af-none">
          <div class="orderForm">
            <p class="form-head">Provide Details</p>
            <form class="sh-form" id="step2">
              <div class="formFields">
                <label>Title*</label>
                <div class="fieldsWraper">
                  <input class="form1" type="text" id="title"  placeholder="Type assignmnet title">
                </div>
              </div>
              <div class="formFields">
                <label>Subject*</label>
                <div class="fieldsWraper">
                  <select class="form1" id="subject" >
                    <option selected>Select your course name</option>
                    <option>Biology and Life Sciences</option>
                    <option>Business and Management</option>
                    <option>Chemistry</option>
                    <option>Culture</option>
                    <option>Economics</option>
                    <option>Education</option>
                    <option>English</option>
                    <option>Environmental Science</option>
                    <option>Finance, Accounting and Banking</option>
                    <option>Geography</option>
                    <option>Project Management</option>
                    <option>Operational Plan</option>
                    <option>Critical review</option>
                    <option>Critical Appraisal</option>
                    <option>Capstone</option>
                    <option>Reflective Thinking</option>
                    <option>Healthcare and Nursing</option>
                    <option>History and Anthropology</option>
                    <option>HRM</option>
                    <option>International Relations</option>
                    <option>IT</option>
                    <option>Law and International Law</option>
                    <option>Linguistics</option>
                    <option>Literature</option>
                    <option>Marketing and PR</option>
                    <option>Maths</option>
                    <option>Philosophy</option>
                    <option>Physics</option>
                    <option>Political Science</option>
                    <option>Psychology</option>
                    <option>Sociology</option>
                    <option>Statistics</option>
                    <option>Other</option>
                  </select>
                </div>
              </div>
              <div class="formFields">
                <label>Citation Style*</label>
                <div class="fieldsWraper">
                  <select class="form1" id="citationStyle"  >
                    <option value="" selected>Select a citation style</option>
                    <option>AMA</option>
                    <option>APA</option>
                    <option>AMS</option>
                    <option>Chicago</option>
                    <option>MLA</option>
                    <option>Turbian</option>
                    <option>Other</option>
                  </select>
                </div>
              </div>
              <div class="formFields">
                <label>No. of Sources*</label>
                <div class="fieldsWraper">
                  <input class="form1" id="sources" placeholder="e.g 3 sources (1, Literary, 2 Peer Reviewd Academics)" type="text">

                </div>
              </div>
              <div class="formFields">
                <label>Instructions</label>
                <textarea  name="name" class="textfield" placeholder="Describe your task in detail or attach original file with teacher’s instructions."></textarea>
              </div>
              <div class="formFields">
                <label>Attachments</label>
                <div class="pages fieldsWraper">
                  <input type="file">
                </div>
              </div>
              <div class="formButton">
                <p><a class="backbtn" href="javascript:;"><svg width="20" height="15" viewBox="0 0 20 15" fill="none" xmlns="http://www.w3.org/2000/svg">
                  <path d="M6.45892 9.09721C6.30724 9.16813 6.14406 9.20222 5.98232 9.20222C5.74941 9.20222 5.51722 9.13062 5.32456 8.98948L0.420532 5.41112C0.155273 5.21816 0 4.91815 0 4.60108C0 4.28402 0.155273 3.98401 0.420532 3.79105L5.32456 0.212689C5.65092 -0.0259585 6.08943 -0.0668696 6.45892 0.105639C6.82769 0.277465 7.0606 0.633392 7.0606 1.02273V3.40853H16.7652C18.5486 3.40853 20 4.80632 20 6.52391V11.6473C20 13.2326 18.6716 15 16.7652 15H8.49831C7.9031 15 7.42003 14.5418 7.42003 13.9772C7.42003 13.4127 7.9031 12.9544 8.49831 12.9544H16.7652C17.2791 12.9544 17.8434 12.2705 17.8434 11.6473V6.52459C17.8434 5.95456 17.3395 5.45476 16.7652 5.45476H7.0606V8.18012C7.0606 8.56946 6.82769 8.92539 6.45892 9.09721ZM4.90403 3.09965L2.84667 4.60108L4.90403 6.1032V3.09965Z" fill="#35414B"/>
                </svg>Previous Step</a> </p>
                <input type="submit" name="" value="Review order details">
              </div>
              <p class="payLater"><a href="#javascript:;">SAVE AND PAY LATER</a> </p>
            </form>
          </div>
          <div class="priceBox">
            <p class="price">Your Price: $50</p>
            <p>
              <span class="was">Was: $50</span>
              <span>Now: $25</span>
            </p>
            <p class="save">You Save: $25</p>
            <p class="offerEnds">Offer ending in 12 : 17 :06</p>
            <div class="sidecheckbox">
              <input type="checkbox">
              <p>Prepay 50%</p>
            </div>
            <p class="innerP">Pay now only: $12.5</p>
            <p class="innerP">Pay on delivery: $12.5</p>
            <p class="totalPrice">Total Price: $25</p>
            <div class="formButton detailsPanel">
              <input type="submit" name="" value="Review order details">
            </div>
            <p class="agree">I agree with <a href="#javascriptL:;">policies</a> and <a href="#javascript:;">terms and conditions</a> by clicking button above.</p>
            <div class="acceptMethods">
              <img src="images/paymet-methods.svg" alt="Payment Methods">
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
