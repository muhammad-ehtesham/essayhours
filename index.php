<?php
$actual_link = $_SERVER['REQUEST_URI'];
$actual_link = substr($actual_link, 1);
$actual_link = strtok($actual_link, '?');
if($actual_link == "" || $actual_link == 'index.php' || $actual_link == 'home')
  $actual_link = "landingpage";
$json_data   = file_get_contents("include/page_config.json");
$pageInfo    = json_decode($json_data, true);
$protocol    = isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off' ? 'https://' : 'http://';
$path        = $protocol . $_SERVER['SERVER_NAME']."/";
$ClientInfo = $_SERVER['HTTP_USER_AGENT'];
if($pageInfo[$actual_link])
{
  if($pageInfo[$actual_link]['redirect'])
  {
    // http_redirect($pageInfo[$actual_link]['redirectUrl']);
    header("Location: ".$pageInfo[$actual_link]['redirectUrl']."");
    die();
  }
  else
  {
    $title       = $pageInfo[$actual_link]['metaTitle'];
    $description = $pageInfo[$actual_link]['metaDescription'];
    include('include/header.php');
    if($pageInfo[$actual_link]['isBlog'])
      include('include/LearnMore_bar.php');
    else
      include('include/navbar.php');
    include($pageInfo[$actual_link]['path']);
    include('include/footer.php');
    include('include/foot.php');
  }
}
else{
  // 404 Page
  $title       = "";
  $description = "";
  include('include/header.php');
  include('include/navbar.php');
  include('include/404.php');
  include('include/footer.php');
  include('include/foot.php');

}
?>
