var swiper = new Swiper(".swiper-container", {
  spaceBetween: 50,
  slidesPerView: 3,
  centeredSlides: true,
  loop: true,
  slideToClickedSlide: true,
  allowTouchMove: true,
  breakpoints: {
    1024: {
      slidesPerView: 3,
    },
    992: {
      slidesPerView: 1,
    },
    640: {
      slidesPerView: 1,
    },
    320: {
      slidesPerView: 1,
    }
  },
  pagination: {
    el: ".swiper-pagination",
    dynamicBullets: true,
    clickable: true,
  },
  navigation: {
    nextEl: ".swiper-button-next",
    prevEl: ".swiper-button-prev",
  },
});
// FAQs Accordion Start
let accItem = document.getElementsByClassName('accordionItem');
let accHD = document.getElementsByClassName('accordionItemHeading');
for (i = 0; i < accHD.length; i++) {
  accHD[i].addEventListener('click', toggleItem);
}
function toggleItem() {
  let itemClass = this.parentNode.className;
  console.log(this.parentNode);
  for (i = 0; i < accItem.length; i++) {
    accItem[i].className = 'accordionItem close';
  }
  if (itemClass == 'accordionItem close') {
    this.parentNode.className = 'accordionItem open';
  }
}
// Custom Tabs Script
function openTab(evt, tabName) {
  let i, tabcontent, tablinks;
  tabcontent = document.getElementsByClassName("tabcontent");
  for (i = 0; i < tabcontent.length; i++) {
    tabcontent[i].style.display = "none";
  }
  tablinks = document.getElementsByClassName("tablinks");
  for (i = 0; i < tablinks.length; i++) {
    tablinks[i].className = tablinks[i].className.replace(" active", "");
  }
  document.getElementById(tabName).style.display = "block";
  evt.currentTarget.className += " active";
}
// Get the element with id="defaultOpen" and click on it
// $(document).ready(function() {
//   document.getElementById("defaultOpen").click();
// });
// FAQs Accordion END

// *************** pages dropdown Code START *********
function incrementPagesOption() {
  var select = document.getElementById('pages');
  if(select.selectedIndex < select.options.length -1)
  select.selectedIndex++;
}
function decrementPagesOption() {
  var select = document.getElementById('pages');
  if(select.selectedIndex > 0)
  select.selectedIndex--;
}
// *************** pages dropdown Code END ***********

// Mobile Sibe Navbar Script
function openNav() {
  document.getElementById("mySidenav").style.width = "250px";
  $(".af-overlay").addClass("show");
}
function closeNav() {
  document.getElementById("mySidenav").style.width = "0";
  $(".af-overlay").removeClass("show");
}

$(document).ready(function() {
  var e = $(".accordionWrapper .accordionItem"),
  t = $("#next"),
  o = e.length,
  m=':visible';
  e.hide(), o > 3 && t.show(), e.slice(0, 3).show(), t.click(function() {
    var r = e.filter(m).length;
    e.slice(r - 1, r + 3).fadeIn(), e.filter(m).length >= o && t.hide()
  })
});
// Calender Dates disabled previous of today
$(document).ready(function() {
  var today = new Date().toISOString().split('T')[0];
  // document.getElementsByName("deadline")[0].setAttribute('min', today);
  $("#deadline").attr("min",today);
  // Input Range
  function sliderColors(){
    var rangeVal = $("#myRange").val();
    var rangeValMin = $("#myRange").attr("min");
    var rangeValMax = $("#myRange").attr("max");
    var val1 = (rangeVal - rangeValMin) / (rangeValMax - rangeValMin);
    $("#myRange").css("background-image","-webkit-gradient(linear, 0% 0%, 100% 0%, color-stop("+val1 +", rgba(36, 157, 236, 1)), color-stop("+val1 +", rgba(61, 66, 74, 0.1)))")
  }
});
// Read More link on landing page
$(".readtext").click(function(){$(".readdiv").hasClass("none")?($(".readdiv").removeClass("none"),$(".readtext").text("Show less...")):($(".readdiv").addClass("none"),$(".readtext").text("Read more about MyPerfectWords.com"))});
// Sticky Navbar
$(document).ready(function(){
  $(window).scrollTop()>100?$(".navBarSect").addClass("myscroll"):$(".navBarSect").removeClass("myscroll")
});
